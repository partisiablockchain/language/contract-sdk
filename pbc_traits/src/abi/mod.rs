//! Module containing ABI traits and glue code.

// Modules
mod enum_variant;
mod named_entity;
mod types;

// Types
pub use enum_variant::EnumVariant;
pub use named_entity::NamedEntityAbi;
pub use types::{KindInfo, NamedTypeSpec};
