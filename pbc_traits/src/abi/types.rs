use crate::abi::enum_variant::EnumVariant;
use crate::abi::NamedEntityAbi;

/// A struct representing the ABI for a Rust type.
///
/// Serialized with the ABI format.
pub struct NamedTypeSpec {
    /// The name of the type.
    pub name: String,
    /// The specific kind information, either struct or enum.
    pub kind_information: KindInfo,
}

/// An enum holding the specific kind information for the different named type specifications.
pub enum KindInfo {
    /// The list of the fields that are associated with the struct.
    Struct {
        /// The fields of the struct.
        fields: Vec<NamedEntityAbi>,
    },
    /// The list of variants that are associated with the enum.
    Enum {
        /// The variants of the enum.
        variants: Vec<EnumVariant>,
    },
}

impl NamedTypeSpec {
    /// Construct a new `StructTypeSpec` instance with the specified name
    pub fn new_struct(name: String) -> Self {
        NamedTypeSpec {
            name,
            kind_information: KindInfo::Struct { fields: Vec::new() },
        }
    }

    /// Construct a new `EnumTypeSpec` instance with the specified name
    pub fn new_enum(name: String) -> Self {
        NamedTypeSpec {
            name,
            kind_information: KindInfo::Enum {
                variants: Vec::new(),
            },
        }
    }

    /// Add a field to this `TypeAbi` instance.
    pub fn add_field(&mut self, field: NamedEntityAbi) {
        if let KindInfo::Struct { ref mut fields } = self.kind_information {
            fields.push(field);
        }
    }

    /// Add a variant to this `TypeAbi` instance.
    pub fn add_variant(&mut self, variant: EnumVariant) {
        if let KindInfo::Enum { ref mut variants } = self.kind_information {
            variants.push(variant);
        }
    }
}
