use crate::{
    create_type_spec::{NamedTypeLookup, NamedTypeSpecs},
    CreateTypeSpec,
};

/// A struct representing any (name, type) entity.
/// In this case it is function arguments and struct fields.
///
/// Serialized with the ABI format.
pub struct NamedEntityAbi {
    /// The name of the field or argument.
    pub name: String,
    /// The raw type spec for the type of the argument.
    pub type_spec: Vec<u8>,
}

impl NamedEntityAbi {
    /// Instantiate a `NamedEntityAbi` with  the specified name.
    ///
    /// * `name` - the name of the type.
    /// * `type_spec` - the type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn new<T: CreateTypeSpec>(
        name: String,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Self {
        let type_spec = T::__ty_generate_spec(named_types, named_type_specs);
        NamedEntityAbi { name, type_spec }
    }
}
