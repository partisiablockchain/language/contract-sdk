/// A struct representing an enum variant.
///
/// Serialized with the ABI format.
pub struct EnumVariant {
    /// The discriminant of the variant.
    pub discriminant: u8,
    /// The raw type spec for the variant, should always be a struct.
    pub type_spec: Vec<u8>,
}

impl EnumVariant {
    /// Instantiate an `EnumVariant` with  the specified discriminant.
    ///
    /// * `discriminant` - the discriminant of the variant.
    /// * `type_spec` - the type_spec for the variant.
    pub fn new(discriminant: u8, type_spec: Vec<u8>) -> Self {
        EnumVariant {
            discriminant,
            type_spec,
        }
    }
}
