//! Serialization for Partisia Blockchain SDK
//!
//! Exposes [the three serialization formats](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html) used in contracts:
//!
//! - [`ReadWriteState`] for State serialization.
//! - [`ReadRPC`]/[`WriteRPC`] for RPC serialization.
//! - [`create_type_spec::CreateTypeSpec`] for ABI serialization, and type witnesses.

pub use create_type_spec::CreateTypeSpec;
pub use read_int::ReadInt;
pub use readwrite_rpc::ReadRPC;
pub use readwrite_rpc::WriteRPC;
pub use readwrite_state::ReadWriteState;
pub use write_int::WriteInt;

pub mod abi;
pub mod create_type_spec;

mod read_int;
mod readwrite_rpc;
mod readwrite_state;
mod write_int;
