//! Serialization for Partisia Blockchain SDK
//!
//! Exposes [the three serialization formats](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html) used in contracts:
//!
//! - [`ReadWriteState`] for State serialization.
//! - [`ReadRPC`]/[`WriteRPC`] for RPC serialization.

pub use read_write_int::ReadInt;
pub use read_write_int::WriteInt;
pub use readwrite_rpc::ReadRPC;
pub use readwrite_rpc::WriteRPC;
pub use readwrite_state::ReadWriteState;

mod readwrite_rpc;
mod readwrite_state;
