//! Module for the [`CreateTypeSpec`] trait and associated types.

use sorted_vec_map::{SortedVec, SortedVecMap, SortedVecSet};
use std::collections::{BTreeSet, VecDeque};
use std::marker::PhantomData;

use crate::abi::NamedTypeSpec;

/// Type definition of the lookup table for the type index assignments made during ABI generation.
pub type NamedTypeLookup = SortedVecMap<String, u8>;
/// Type definition of the lookup table for the types visited during ABI generation.
pub type NamedTypeSpecs = SortedVecMap<u8, NamedTypeSpec>;

/// This trait adds the runtime type information needed to generate the [contract PBC ABI files](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
///
/// * The [`__ty_name`](Self::__ty_name) method returns ordinary Rust names.
/// * The [`__ty_identifier`](Self::__ty_identifier) method returns a unique identifier for the type.
/// * The [`__ty_generate_spec`](Self::__ty_generate_spec) method recursively creates the PBC ABI byte-serialized type specification.
///
/// Custom implementations should be rare, and thoroughly tested, as a malformed ABI might seriously
/// affect intercontract communication, and might even prevent initialization. Ensure validity with
/// respect to the [ABI specification](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
///
/// [`CreateTypeSpec`] is also utilized for creating type witnesses. These are used to validate
/// states for upgradable contracts.
pub trait CreateTypeSpec: Sized {
    /// Produce the name of the implementing type.
    fn __ty_name() -> String;

    /// A unique identifier for this type. Generated at compile time for structs and enums.
    fn __ty_identifier() -> String;

    /// Generate and return the type specification for the implementing type.
    /// If the implementing type is a named type (struct or enum) its implementation must conform to the following.
    ///
    /// If the type's identifier (see [`__ty_identifier`](Self::__ty_identifier)) does not exist in
    /// the `named_type_index_lookup` map: it should
    /// * Add its own identifier to the index map using the next unused index (i.e. `named_type_index_lookup.len()`),
    /// * then build its NamedTypeSpec and add it to the `named_type_specs` map, using its new index as key,
    /// * finally it the same must be done for all the types fields or variants recursively.
    ///
    /// If the type exists in the map, as it should if generated as described above, it should return the NamedTypeRef.
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8>;
}

/// Implement the [`CreateTypeSpec`] trait for a 'simple' type given a type name and a type ordinal.
///
/// The input is n pairs of (type, literal).
///
/// The output is n implementations of [`CreateTypeSpec`] that simply write the type as a string
/// and fill the ordinal in the [`CreateTypeSpec::__ty_spec_write`] vector output.
macro_rules! impl_for_type {
    ($($type:ty, $name:literal, $val:literal)*) => {
        $(
            #[doc = "Implementation of the [`CreateTypeSpec`] trait for [`"]
            #[doc = stringify!($type)]
            #[doc = "`]."]
            impl CreateTypeSpec for $type {

                #[doc = concat!("Constant string `", stringify!($type), "`.")]
                fn __ty_name() -> String {
                    $name.to_string()
                }

                #[doc = concat!("Ordinal is `", stringify!($val), "`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).")]
                fn __ty_identifier() -> String {
                    Self::__ty_name()
                }

                fn __ty_generate_spec(
                    _named_type_index_lookup: &mut NamedTypeLookup,
                    _named_type_specs: &mut NamedTypeSpecs,)
                -> Vec<u8> {
                    vec![$val]
                }
            }
        )*
    }
}

// Implement the [`CreateTypeSpec`] trait for simple types.
impl_for_type!(
    u8,     "U8",     0x01
    u16,    "U16",    0x02
    u32,    "U32",    0x03
    u64,    "U64",    0x04
    u128,   "U128",   0x05
    i8,     "I8",     0x06
    i16,    "I16",    0x07
    i32,    "I32",    0x08
    i64,    "I64",    0x09
    i128,   "I128",   0x0a
    String, "String", 0x0b
    bool,   "Bool",   0x0c
);

/// Implementation of the [`CreateTypeSpec`] trait for [`Vec<T>`] for any `T` that implements
/// [`CreateTypeSpec`].
impl<T: CreateTypeSpec> CreateTypeSpec for Vec<T> {
    /// Type name is constant string `Vec<T>`.
    fn __ty_name() -> String {
        format!("Vec{}", T::__ty_name())
    }

    fn __ty_identifier() -> String {
        format!("Vec<{}>", T::__ty_identifier())
    }

    /// Ordinal is `0x0e` followed by ordinal of `T`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        // Vector is 0x0e followed by the spec for the parameter type
        let mut type_spec = vec![0x0e];
        let mut t_type_spec = T::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut t_type_spec);
        type_spec
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`VecDeque<T>`] for any `T` that implements
/// [`CreateTypeSpec`].
impl<T: CreateTypeSpec> CreateTypeSpec for VecDeque<T> {
    /// Type name is constant string `VecDeque<T>`.
    fn __ty_name() -> String {
        format!("VecDeque{}", T::__ty_name())
    }

    fn __ty_identifier() -> String {
        format!("VecDeque<{}>", T::__ty_identifier())
    }

    /// Ordinal is `0x0e` followed by ordinal of `T`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        // Identical to Vec impl
        Vec::<T>::__ty_generate_spec(named_type_index_lookup, named_type_specs)
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`BTreeSet<T>`]
/// for any `T` that implements [`CreateTypeSpec`]
impl<V: CreateTypeSpec> CreateTypeSpec for BTreeSet<V> {
    /// Type name is `BTreeSet<T>`
    fn __ty_name() -> String {
        format!("BTreeSet{}", V::__ty_name())
    }

    /// Ordinal is `0x10` followed by ordinal of `T`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_identifier() -> String {
        format!("BTreeSet<{}>", V::__ty_identifier())
    }

    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        // BTreeSet is 0x10 followed by the spec for the parameter type
        let mut type_spec = vec![0x10];
        let mut v_type_spec = V::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut v_type_spec);
        type_spec
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`Option<T>`]
/// for any `T` that implements [`CreateTypeSpec`]
impl<T: CreateTypeSpec> CreateTypeSpec for Option<T> {
    /// Type name is `Option<T>`.
    fn __ty_name() -> String {
        format!("Option{}", T::__ty_name())
    }

    /// Ordinal is `0x12` followed by ordinal of `T`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_identifier() -> String {
        format!("Option<{}>", T::__ty_identifier())
    }

    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        // Option is 0x12 followed by the spec for the parameter type
        let mut type_spec = vec![0x12];
        let mut t_type_spec = T::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut t_type_spec);
        type_spec
    }
}

/// Implement [`CreateTypeSpec`] for an [`[T;n]`] array type.
///
/// The ordinal is `0x1A` followed by the length of the array
impl<const LEN: usize, T: CreateTypeSpec> CreateTypeSpec for [T; LEN] {
    /// Type name is `[u8; LEN]`.
    fn __ty_name() -> String {
        format!("{}{}", T::__ty_name(), LEN)
    }

    fn __ty_identifier() -> String {
        Self::__ty_name()
    }

    /// Ordinal is `0x1A` followed by byte repr of length `LEN`, as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        let mut type_spec = vec![0x1A];
        let length = u8::try_from(LEN)
            .ok()
            .filter(|&x| x <= 0x7F)
            .expect("ABI does not support byte arrays of sizes larger than 127.");
        let mut t_type_spec = T::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut t_type_spec);
        type_spec.push(length);
        type_spec
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`PhantomData<T>`]
/// for any `T` that implements [`CreateTypeSpec`]
///
/// Emitted type spec will always be an zero-length type, though which zero-length type is not
/// defined. It is not guarenteed that [`PhantomData`] is present in the emitted ABI.
impl<T: CreateTypeSpec> CreateTypeSpec for PhantomData<T> {
    /// Type name is `PhantomData<T>`.
    fn __ty_name() -> String {
        format!("PhantomData{}", T::__ty_name())
    }

    /// Type identifier is `PhantomData<T>`.
    fn __ty_identifier() -> String {
        format!("PhantomData<{}>", T::__ty_identifier())
    }

    fn __ty_generate_spec(
        _named_type_index_lookup: &mut NamedTypeLookup,
        _named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        vec![0x1A, 0x01, 0x00]
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`SortedVecMap<K, V>`]
/// for any key and value type `K`, `V` that implement [`CreateTypeSpec`].
impl<K: CreateTypeSpec, V: CreateTypeSpec> CreateTypeSpec for SortedVecMap<K, V> {
    /// Type name is `VecMap<K, V>`.
    fn __ty_name() -> String {
        format!("SortedVecMap{}{}", K::__ty_name(), V::__ty_name())
    }

    fn __ty_identifier() -> String {
        format!(
            "SortedVecMap<{}, {}>",
            K::__ty_identifier(),
            V::__ty_identifier()
        )
    }

    /// Ordinal is `0x0f` followed by ordinals of `K` and `V`,
    /// as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        let mut type_spec = vec![0x0f];
        let mut k_type_spec = K::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        let mut v_type_spec = V::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut k_type_spec);
        type_spec.append(&mut v_type_spec);
        type_spec
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`SortedVec<T>`]
/// for any element and element type `T`, `T` that implement [`CreateTypeSpec`].
impl<T: CreateTypeSpec> CreateTypeSpec for SortedVec<T> {
    /// Type name is `SortedVec<T>`.
    fn __ty_name() -> String {
        format!("SortedVec{}", T::__ty_name())
    }

    fn __ty_identifier() -> String {
        format!("SortedVec<{}>", T::__ty_identifier(),)
    }

    /// Ordinal is `0x0e` followed by ordinal of `T`.
    /// as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        // Identical to Vec impl
        Vec::<T>::__ty_generate_spec(named_type_index_lookup, named_type_specs)
    }
}

/// Implementation of the [`CreateTypeSpec`] trait for [`SortedVecSet<T>`]
/// for any element and element type `T`, `T` that implement [`CreateTypeSpec`].
impl<T: CreateTypeSpec> CreateTypeSpec for SortedVecSet<T> {
    /// Type name is `SortedVecSet<T>`.
    fn __ty_name() -> String {
        format!("SortedVecSet{}", T::__ty_name())
    }

    fn __ty_identifier() -> String {
        format!("SortedVecSet<{}>", T::__ty_identifier(),)
    }

    /// Ordinal is `0x10` followed by ordinal of `T`.
    /// as defined in [ABI Spec](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).
    fn __ty_generate_spec(
        named_type_index_lookup: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Vec<u8> {
        let mut type_spec = vec![0x10]; // Set
        let mut t_type_spec = T::__ty_generate_spec(named_type_index_lookup, named_type_specs);
        type_spec.append(&mut t_type_spec);
        type_spec
    }
}
