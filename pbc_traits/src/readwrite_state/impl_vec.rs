use super::ReadWriteState;
use crate::ReadInt;
use crate::WriteInt;
use sorted_vec_map::{entry::Entry, SortedVec, SortedVecMap, SortedVecSet};
use std::collections::VecDeque;
use std::io::{Read, Write};
use std::mem::MaybeUninit;

/// Implementation of the [`ReadWriteState`] trait for [`Vec<T>`] for any `T` that implements [`ReadWriteState`].
impl<T: ReadWriteState> ReadWriteState for Vec<T> {
    /// The vector buffer is stored behind a [pointer](https://doc.rust-lang.org/reference/types/pointer.html), so must be `false`.
    const SERIALIZABLE_BY_COPY: bool = false;

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        match T::SERIALIZABLE_BY_COPY {
            true => static_sized_content_read_from(reader),
            false => dynamic_sized_content_read_from(reader),
        }
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        match T::SERIALIZABLE_BY_COPY {
            true => static_sized_content_write_to([self], writer),
            false => dynamic_sized_content_write_to([self], writer),
        }
    }
}

/// Implementation of the [`ReadWriteState`] trait for [`VecDeque<T>`] for any `T` that implements [`ReadWriteState`].
impl<T: ReadWriteState> ReadWriteState for VecDeque<T> {
    /// The vector buffer is stored behind a [pointer](https://doc.rust-lang.org/reference/types/pointer.html), so must be `false`.
    const SERIALIZABLE_BY_COPY: bool = false;

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        // Implementation reads as vec before converting to vecdeque
        let as_vec = Vec::<T>::state_read_from(reader);
        VecDeque::from(as_vec)
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        let (slice_front, slice_back) = self.as_slices();
        let slices = [slice_front, slice_back];
        match T::SERIALIZABLE_BY_COPY {
            true => static_sized_content_write_to(slices, writer),
            false => dynamic_sized_content_write_to(slices, writer),
        }
    }
}

impl<K: ReadWriteState, V: ReadWriteState> ReadWriteState for Entry<K, V> {
    /// Determined based on the layout of the underlying types.
    const SERIALIZABLE_BY_COPY: bool = K::SERIALIZABLE_BY_COPY
        & V::SERIALIZABLE_BY_COPY
        & (std::mem::size_of::<Self>().wrapping_rem(std::mem::align_of::<Self>()) == 0)
        & (std::mem::size_of::<K>() + std::mem::size_of::<V>() == std::mem::size_of::<Self>());

    /// Implementation reads as internal list, before converting to [`SortedVecMap`].
    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let key = K::state_read_from(reader);
        let value = V::state_read_from(reader);
        Entry::new(key, value)
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.key.state_write_to(writer)?;
        self.value.state_write_to(writer)
    }
}

impl<K: ReadWriteState + Ord, V: ReadWriteState> ReadWriteState for SortedVecMap<K, V> {
    /// Determined based on the layout of the underlying type.
    const SERIALIZABLE_BY_COPY: bool = Vec::<Entry<K, V>>::SERIALIZABLE_BY_COPY;

    /// Implementation reads as internal list, before converting to [`SortedVecMap`].
    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let entries = Vec::<Entry<K, V>>::state_read_from(reader);
        SortedVecMap::from(entries)
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.entries().state_write_to(writer)
    }
}

impl<T: ReadWriteState + Ord> ReadWriteState for SortedVecSet<T> {
    /// Determined based on the layout of the underlying type.
    const SERIALIZABLE_BY_COPY: bool = Vec::<T>::SERIALIZABLE_BY_COPY;

    /// Implementation reads as internal list, before converting to [`SortedVec`].
    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let entries = Vec::<T>::state_read_from(reader);
        SortedVecSet::from(entries)
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.entries().state_write_to(writer)
    }
}

impl<T: ReadWriteState + Ord> ReadWriteState for SortedVec<T> {
    /// Determined based on the layout of the underlying type.
    const SERIALIZABLE_BY_COPY: bool = SortedVecSet::<T>::SERIALIZABLE_BY_COPY;

    /// Implementation reads as internal list, before converting to [`SortedVec`].
    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let entries = SortedVecSet::<T>::state_read_from(reader);
        SortedVec::from(entries)
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.as_set().state_write_to(writer)
    }
}

const fn length_of_slices<T, const N: usize>(slices: [&[T]; N]) -> usize {
    let mut idx = 0;
    let mut summed_length = 0;
    while idx < slices.len() {
        summed_length += slices[idx].len();
        idx += 1;
    }
    summed_length
}

/// Handles deserialization for vecs with dynamic-sized contents.
fn dynamic_sized_content_read_from<R: Read, T: ReadWriteState>(reader: &mut R) -> Vec<T> {
    let len = reader.read_u32_le() as usize;
    let mut result = Vec::with_capacity(len);
    for _ in 0..len {
        result.push(T::state_read_from(reader))
    }
    result
}

/// Handles serialization for vecs with dynamic-sized contents.
fn dynamic_sized_content_write_to<W: Write, T: ReadWriteState, const N: usize>(
    slices: [&[T]; N],
    writer: &mut W,
) -> std::io::Result<()> {
    writer
        .write_u32_le(length_of_slices(slices) as u32)
        .unwrap();
    for slice in slices {
        for item in slice {
            item.state_write_to(writer).unwrap();
        }
    }

    Ok(())
}

/// Handles deserialization for vecs with static-sized contents.
fn static_sized_content_read_from<R: Read, T: ReadWriteState>(reader: &mut R) -> Vec<T> {
    assert!(T::SERIALIZABLE_BY_COPY);

    let count = reader.read_u32_le() as usize;
    let mut result: Vec<MaybeUninit<T>> = Vec::with_capacity(count);
    unsafe {
        result.set_len(count);
        if std::mem::size_of::<T>() > 0 {
            let (prefix, middle, suffix) = result.align_to_mut::<u8>();
            assert!(prefix.is_empty());
            assert!(suffix.is_empty());
            reader.read_exact(middle).unwrap();
        }

        std::mem::transmute::<_, Vec<T>>(result)
    }
}

/// Handles serialization for vecs with static-sized contents.
fn static_sized_content_write_to<W: Write, T: ReadWriteState, const N: usize>(
    slices: [&[T]; N],
    writer: &mut W,
) -> std::io::Result<()> {
    assert!(T::SERIALIZABLE_BY_COPY);

    writer
        .write_u32_le(length_of_slices(slices) as u32)
        .unwrap();
    if std::mem::size_of::<T>() > 0 {
        for slice in slices {
            unsafe {
                let (prefix, middle, suffix) = slice.align_to::<u8>();
                assert!(prefix.is_empty());
                assert!(suffix.is_empty());
                writer.write_all(middle)?;
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::static_sized_content_write_to;
    use std::io::Write;
    use test_utility::DummyWriter;
    #[test]
    pub fn buffer_too_small() {
        let buf = vec![1u8, 2, 3, 4, 5];
        let mut writ = DummyWriter::new(4);
        let res = static_sized_content_write_to([&buf], &mut writ);
        writ.flush().unwrap();

        assert!(res.is_err())
    }
}
