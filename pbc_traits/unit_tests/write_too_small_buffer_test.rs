use crate::readwrite_state::impl_vec::static_sized_content_write_to;
use std::io::Write;
use test_utility::DummyWriter;

#[test]
pub fn buffer_too_small() {
    let buf = vec![1u8, 2, 3, 4, 5];
    let mut writ = DummyWriter::new(4);
    let res = static_sized_content_write_to([&buf], &mut writ);
    writ.flush().unwrap();

    assert!(res.is_err())
}
