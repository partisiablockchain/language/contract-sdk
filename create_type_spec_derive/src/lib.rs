//! Internal Partisia Blockchain SDK crate with derive logic for `CreateTypeSpec` trait.
//!
//! *This module is mainly used during `"ABI"` construction.*

extern crate derive_commons;
extern crate proc_macro;
#[macro_use]
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;

use proc_macro2::Literal;
use quote::ToTokens;
use syn::__private::TokenStream2;
use syn::{Data, DataEnum, DataStruct, Fields, Ident, Path};
use uuid::Uuid;

use derive_commons::{extract_enum_variant_data, has_unique_elements};

/// Derive the `CreateTypeSpec` trait for structs and enums.
#[proc_macro_derive(CreateTypeSpec, attributes(discriminant))]
pub fn create_type_spec(input: TokenStream) -> TokenStream {
    // Parse the AST
    let ast = syn::parse(input).unwrap();

    // Build the impl
    let gen = impl_create_type_spec(ast, syn::parse_quote! { pbc_contract_common::abi });

    // Return the generated impl
    gen.into()
}

/// Derive the `CreateTypeSpec` trait for structs and enum, with an internal view; for use in
/// `pbc_data_types`.
#[doc(hidden)]
#[proc_macro_derive(CreateTypeSpecInternal)]
pub fn create_type_spec_internal(input: TokenStream) -> TokenStream {
    // Parse the AST
    let ast = syn::parse(input).unwrap();

    // Build the impl
    let gen = impl_create_type_spec(ast, syn::parse_quote! { pbc_abi });

    // Return the generated impl
    gen.into()
}

fn impl_create_type_spec(
    ast: syn::DeriveInput,
    abi_module_prefix: syn::Path,
) -> proc_macro2::TokenStream {
    let type_name = &ast.ident;
    let trait_name: syn::Path = syn::parse_quote! { #abi_module_prefix::CreateTypeSpec };

    // Generics
    let mut generics = ast.generics;
    derive_commons::extend_generic_bounds_with_trait(&mut generics, &trait_name);

    let uuid = Uuid::new_v4();
    let type_id: String = uuid.hyphenated().to_string();

    let create_ty_generate_spec_fn = match ast.data {
        Data::Struct(ref data_struct) => {
            let (field_names, field_types) = data_to_field_types(data_struct);
            let field_names_ts = field_names.iter().map(|x| x.to_token_stream()).collect();
            create_ty_generate_spec_for_struct(&abi_module_prefix, &field_names_ts, &field_types)
        }
        Data::Enum(ref data_enum) => {
            create_ty_generate_spec_for_enum(&abi_module_prefix, data_enum)
        }
        _ => panic!(
            "CreateTypeSpec derive does not support Union, currently only structs with named \
        fields and explicitly discriminated enums consisting of struct variants"
        ),
    };
    let create_type_spec_impl = create_type_spec_impl(
        type_name,
        type_id,
        &trait_name,
        &generics,
        create_ty_generate_spec_fn,
    );

    quote! {
        #create_type_spec_impl
    }
}

struct VariantInfo<'a> {
    discriminant: Literal,
    identifier: &'a Ident,
    fields: Vec<VariantFieldInfo>,
}

struct VariantFieldInfo {
    field_name: TokenStream2,
    field_type: TokenStream2,
}

fn data_to_variants(data: &DataEnum) -> Vec<VariantInfo> {
    let mut result: Vec<VariantInfo> = Vec::new();
    let mut discriminants: Vec<Literal> = vec![];

    for variant in &data.variants {
        match variant.fields {
            Fields::Named(ref fields) => {
                let identifier = &variant.ident;
                let (discriminant, fields_types, fields_names) =
                    extract_enum_variant_data(fields, &variant.attrs);
                let fields : Vec<VariantFieldInfo> = fields_names
                    .into_iter()
                    .zip(fields_types.into_iter())
                    .map(|(field_name, field_type)| VariantFieldInfo {field_name, field_type})
                    .collect();
                discriminants.push(discriminant.clone());
                let variant_info = VariantInfo {
                    discriminant,
                    identifier,
                    fields,
                };
                result.push(variant_info);
            }
            _ => panic!("Derive CreateTypeSpec only supports explicitly discriminated enums consisting of struct variants"),
        }
    }
    if has_unique_elements(discriminants.iter().map(|d| d.to_string())) {
        result
    } else {
        panic!("Duplicate discriminant values")
    }
}

/// Produces Rust code for creating an identifier string for the given type, recursively accounting for the
/// type's generics.
fn create_identifier_expression(
    type_id: &str,
    called_method: syn::Ident,
    generics: &syn::Generics,
) -> proc_macro2::TokenStream {
    let type_params: Vec<syn::Ident> = generics.type_params().map(|x| x.ident.clone()).collect();
    let generics_str = if type_params.is_empty() {
        quote! {}
    } else {
        quote! { + #( &#type_params::#called_method())+* }
    };
    quote! {
        #type_id.to_owned() #generics_str
    }
}

fn create_ty_generate_spec_for_struct(
    abi_module_prefix: &syn::Path,
    field_names: &Vec<TokenStream2>,
    field_types: &Vec<TokenStream2>,
) -> proc_macro2::TokenStream {
    let fields_string_names: Vec<String> = field_names
        .iter()
        .map(|name| -> String { name.to_string().to_lowercase() })
        .collect();

    quote! {
        fn __ty_generate_spec(
            named_types: &mut #abi_module_prefix::create_type_spec::NamedTypeLookup,
            named_type_specs: &mut #abi_module_prefix::create_type_spec::NamedTypeSpecs,
        ) -> #abi_module_prefix::abi_model::TypeSpec {
            let type_id: String = Self::__ty_identifier();
            let type_index: u8 = if let Some(t) = named_types.get(&type_id) {
                *t
            } else {
                let type_index = named_types.len() as u8;
                named_types.insert(type_id.clone(), type_index);

                let mut type_abi = #abi_module_prefix::abi_model::NamedTypeSpec::new_struct(
                    Self::__ty_name(),
                );
                #(
                    let #field_names = #abi_module_prefix::abi_model::NamedEntityAbi::new::<#field_types>(
                        #fields_string_names.to_string(),
                        named_types,
                        named_type_specs,
                    );
                    type_abi.add_field(#field_names);
                )*
                named_type_specs.insert(type_index, type_abi);
                type_index
            };


            #abi_module_prefix::abi_model::TypeSpec::NamedTypeRef{ index: type_index }
        }
    }
}

fn create_ty_generate_spec_for_enum(
    abi_module_prefix: &Path,
    data_enum: &DataEnum,
) -> proc_macro2::TokenStream {
    let variants = data_to_variants(data_enum);

    let generate_variants_type_specs: Vec<TokenStream2> = variants
        .iter()
        .map(|variant| generate_type_spec_for_enum_variant(abi_module_prefix, variant))
        .collect();
    quote! {
        fn __ty_generate_spec(
            named_types: &mut #abi_module_prefix::create_type_spec::NamedTypeLookup,
            named_type_specs: &mut #abi_module_prefix::create_type_spec::NamedTypeSpecs,
        ) -> #abi_module_prefix::abi_model::TypeSpec {
            let type_id: String = Self::__ty_identifier();
            let type_index: u8 = if let Some(t) = named_types.get(&type_id) {
                *t
            } else {
                // 1. Add enum to named_types_index
                let type_index = named_types.len() as u8;
                named_types.insert(type_id.clone(), type_index);

                let mut enum_type_spec = #abi_module_prefix::abi_model::NamedTypeSpec::new_enum(
                    Self::__ty_name(),
                );

                // 2. For each variant, index the variant, call __ty_generate_spec on its fields,
                // and create its type spec
                #(
                    #generate_variants_type_specs
                )*

                // 3. Add enum to named_type_specs
                named_type_specs.insert(type_index, enum_type_spec);
                type_index
            };

            #abi_module_prefix::abi_model::TypeSpec::NamedTypeRef{ index: type_index }
        }
    }
}

fn generate_type_spec_for_enum_variant(
    abi_module_prefix: &Path,
    variant: &VariantInfo,
) -> TokenStream2 {
    let variant_name = &variant.identifier.to_string();
    let variant_name_lowercase = variant_name.to_lowercase();
    let discriminant = &variant.discriminant;

    let variant_tmp_identifier = Uuid::new_v4().hyphenated().to_string();

    let type_id_variable = format_ident!("type_id_{}", variant_name_lowercase);
    let type_next_index_variable = format_ident!("type_next_index_{}", variant_name_lowercase);
    let type_index_variable = format_ident!("type_index_{}", variant_name_lowercase);
    let type_spec_variable = format_ident!("type_spec_{}", variant_name_lowercase);
    let variant_variable = format_ident!("variant_{}", variant_name_lowercase);
    let variant_struct_variable = format_ident!("variant_struct_{}", variant_name_lowercase);

    let variant_field_names: Vec<TokenStream2> = variant
        .fields
        .iter()
        .map(|field| field.field_name.clone())
        .collect();
    let variant_field_names_as_strings: Vec<String> = variant_field_names
        .iter()
        .map(|name| name.to_string().to_lowercase())
        .collect();
    let variant_field_types: Vec<TokenStream2> = variant
        .fields
        .iter()
        .map(|field| field.field_type.clone())
        .collect();
    quote! {
        // Add temp identifier to get variant structs type index.
        let #type_id_variable = format!("{}{}", &type_id, #variant_tmp_identifier);
        let #type_next_index_variable = named_types.len() as u8;
        let #type_index_variable = named_types.entry(#type_id_variable.clone()).or_insert(#type_next_index_variable).clone();

        // Add enum variant to enum type spec
        let #type_spec_variable = #abi_module_prefix::abi_model::TypeSpec::NamedTypeRef { index: #type_index_variable.to_owned() };
        let #variant_variable = #abi_module_prefix::abi_model::EnumVariant::new(
            #discriminant,
            #type_spec_variable,
        );
        enum_type_spec.add_variant(#variant_variable);

        // Create variant struct
        let mut #variant_struct_variable = #abi_module_prefix::abi_model::NamedTypeSpec::new_struct(
          #variant_name.to_string()
        );

        // Generate type spec for variant structs' fields
        #(
            let #variant_field_names = #abi_module_prefix::abi_model::NamedEntityAbi::new::<#variant_field_types>(
              #variant_field_names_as_strings.to_string(),
                named_types,
                named_type_specs,
            );

            #variant_struct_variable.add_field(#variant_field_names);
        )*

        // Generate type spec for variant struct.
        named_type_specs.entry(#type_index_variable.to_owned()).or_insert(#variant_struct_variable);
    }
}

fn create_type_spec_impl(
    type_name: &Ident,
    type_id: String,
    trait_name: &syn::Path,
    generics: &syn::Generics,
    ty_generate_spec_fn: proc_macro2::TokenStream,
) -> proc_macro2::TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let name_creator =
        create_identifier_expression(&type_name.to_string(), format_ident!("__ty_name"), generics);
    let identifier_creator =
        create_identifier_expression(&type_id, format_ident!("__ty_identifier"), generics);

    let doc = "Automatically derived [`CreateTypeSpec`].";

    quote!(
        #[doc=#doc]
        #[automatically_derived]
        impl #impl_generics #trait_name for #type_name #ty_generics #where_clause {
            fn __ty_name() -> String {
                #name_creator
            }

            fn __ty_identifier() -> String {
                #identifier_creator
            }

            #ty_generate_spec_fn
        }
    )
}

fn data_to_field_types(data: &DataStruct) -> (Vec<Ident>, Vec<TokenStream2>) {
    match data.fields {
        Fields::Named(ref fields) => {
            let names: Vec<Ident> = fields
                .named
                .iter()
                .map(derive_commons::field_to_name)
                .collect();
            let types: Vec<TokenStream2> = fields
                .named
                .iter()
                .map(derive_commons::field_to_type)
                .collect();
            (names, types)
        }
        _ => {
            panic!("Derive CreateTypeSpec only supports named fields for structs")
        }
    }
}
