
# Test utility library for testing Zero-Knowledge computations

**NOTICE: This library cannot perform MPC; it can only mock MPC.**

This library tries to mimic the type system and semantics of the Zero-Knowledge compiler and
stack, in order allow contracts to test their contract computations without deploying on the
test net. This library is the first testing step for a contract, followed by

1. Integration testing between public contract and computation
2. Full integration testing on test-net.

This library also acts as documentation of the various builtin functions available in the
Zk-compiler.

## Example Computation of variable summation

Simple example of a computation summing all input variables:

```rust
use pbc_zk::{Sbi32, load_sbi, secret_variable_ids, zk_compute};

#[zk_compute(shortname = 0x01)]
pub fn sum_all_variables() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for variable_id in secret_variable_ids() {
        sum = sum + load_sbi::<Sbi32>(variable_id);
    }
    sum
}
```

## Example Computation of loading struct

Another example that sums all values of a specific type

```rust
use pbc_zk::{Sbi8, Sbi32, secret_variable_ids, load_sbi, SecretBinary, zk_compute};

#[derive(Clone, SecretBinary)]
struct MyStruct {
    variable_type: Sbi8,
    value: Sbi32,
}

#[zk_compute(shortname = 0x02)]
pub fn sum_all_variables() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for variable_id in secret_variable_ids() {
        let value = load_sbi::<MyStruct>(variable_id);
        if value.variable_type == Sbi8::from(0) {
            sum = sum + value.value;
        }
    }
    sum
}
```

