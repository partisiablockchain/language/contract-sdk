//! Testing unsigned secret binary integers
use pbc_zk::*;
use proptest::prelude::*;

fn assert_from_to(value: Sbu8, bits: [Sbu1; 8]) {
    assert_eq!(value, Sbu8::from_le_bits(bits));
    assert_eq!(value.to_le_bits(), bits);
}

#[test]
fn clone() {
    let x = Sbu8::from(0x33);
    assert_eq!(x, x.clone());
}

#[test]
fn from_to_i8_specific() {
    assert_from_to(Sbu8::from(0), [false; 8]);
    assert_from_to(
        Sbu8::from(1),
        [true, false, false, false, false, false, false, false],
    );
    assert_from_to(
        Sbu8::from(2),
        [false, true, false, false, false, false, false, false],
    );
    assert_from_to(
        Sbu8::from(9),
        [true, false, false, true, false, false, false, false],
    );
    assert_from_to(
        Sbu8::from(127),
        [true, true, true, true, true, true, true, false],
    );
}

proptest! {
    #[test]
    fn from_to_u8(i: u8) {
        let e = Sbu8::from(i);
        assert_eq!(e, Sbu8::from_le_bits(e.to_le_bits()));
    }

    #[test]
    fn from_to_u16(i: u16) {
        let e = Sbu16::from(i);
        assert_eq!(e, Sbu16::from_le_bits(e.to_le_bits()));
    }

    #[test]
    fn from_to_u32(i: u32) {
        let e = Sbu32::from(i);
        assert_eq!(e, Sbu32::from_le_bits(e.to_le_bits()));
    }

    #[test]
    fn from_to_u64(i: u64) {
        let e = Sbu64::from(i);
        assert_eq!(e, Sbu64::from_le_bits(e.to_le_bits()));
    }

    #[test]
    fn from_to_u128(i: u128) {
        let e = Sbu128::from(i);
        assert_eq!(e, Sbu128::from_le_bits(e.to_le_bits()));
    }
}

proptest! {
    #[test]
    fn add(lhs: u32, rhs: u32) {
        let expected = lhs.wrapping_add(rhs);
        let gotten = Sbu32::from(lhs) + Sbu32::from(rhs);
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn sub(lhs: u32, rhs: u32) {
        let expected = lhs.wrapping_sub(rhs);
        let gotten = Sbu32::from(lhs) - Sbu32::from(rhs);
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn shl(lhs: u32, rhs: usize) {
        let expected = lhs.wrapping_shl(rhs as u32);
        let gotten = Sbu32::from(lhs) << rhs;
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn shr(lhs: u32, rhs: usize) {
        let expected = lhs.wrapping_shr(rhs as u32);
        let gotten = Sbu32::from(lhs) >> rhs;
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn band(lhs: u32, rhs: u32) {
        let expected = lhs & rhs;
        let gotten = Sbu32::from(lhs) & Sbu32::from(rhs);
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn bor(lhs: u32, rhs: u32) {
        let expected = lhs | rhs;
        let gotten = Sbu32::from(lhs) | Sbu32::from(rhs);
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn bxor(lhs: u32, rhs: u32) {
        let expected = lhs ^ rhs;
        let gotten = Sbu32::from(lhs) ^ Sbu32::from(rhs);
        assert_eq!(gotten, Sbu32::from(expected));
    }
}

proptest! {
    #[test]
    fn eq(lhs: u32, rhs: u32) {
        let expected = lhs == rhs;
        let gotten = Sbu32::from(lhs) == Sbu32::from(rhs);
        assert_eq!(gotten, expected);
    }
}

proptest! {
    #[test]
    fn leq(lhs: u32, rhs: u32) {
        let expected = lhs <= rhs;
        let gotten = Sbu32::from(lhs) <= Sbu32::from(rhs);
        assert_eq!(gotten, expected);
    }
}

proptest! {
    #[test]
    fn geq(lhs: u32, rhs: u32) {
        let expected = lhs >= rhs;
        let gotten = Sbu32::from(lhs) >= Sbu32::from(rhs);
        assert_eq!(gotten, expected);
    }
}

proptest! {
    #[test]
    fn lt(lhs: u32, rhs: u32) {
        let expected = lhs < rhs;
        let gotten = Sbu32::from(lhs) < Sbu32::from(rhs);
        assert_eq!(gotten, expected);
    }
}

proptest! {
    #[test]
    fn gt(lhs: u32, rhs: u32) {
        let expected = lhs > rhs;
        let gotten = Sbu32::from(lhs) > Sbu32::from(rhs);
        assert_eq!(gotten, expected);
    }
}

proptest! {
    #[test]
    fn partial_cmp(lhs: u32, rhs: u32) {
        let expected = lhs.partial_cmp(&rhs);
        let gotten = Sbu32::from(lhs).partial_cmp(&Sbu32::from(rhs));
        assert_eq!(gotten, expected);
    }
}
