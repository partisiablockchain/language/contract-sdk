//! Test for Zk computations that tries to access non-existing secrets.
use pbc_zk::{load_metadata, load_sbi, zk_compute, Sbi32, SecretVarId};

#[zk_compute(shortname = 0x33)]
fn bad_computation_1() -> Sbi32 {
    let id = SecretVarId::new(999);
    let metadata: i32 = load_metadata(id);
    Sbi32::from(metadata)
}

#[zk_compute(shortname = 0x34)]
fn bad_computation_2() -> Sbi32 {
    let id = SecretVarId::new(999);
    load_sbi(id)
}

/// Cannot load the metadata for a non-existing SecretVar.
#[test]
#[should_panic]
fn run_example_computation_1() {
    bad_computation_1();
}

/// Cannot a non-existing secret variable.
#[test]
#[should_panic]
fn run_example_computation_2() {
    bad_computation_2();
}
