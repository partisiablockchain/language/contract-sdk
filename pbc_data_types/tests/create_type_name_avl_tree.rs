//! Tests for [`CreateTypeSpec`] macro.
#![cfg(feature = "abi")]

use pbc_abi::abi_model::AbiSerialize;
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_abi::CreateTypeSpec;
use pbc_contract_common::avl_tree_map::AvlTreeMap;
use pbc_contract_common::upgrade::TypeWitness;
use pbc_contract_common::zk::evm_event::EventSubscriptionId;

use pbc_contract_common::zk::evm_event::ExternalEventId;
use pbc_contract_common::zk::SecretVarId;

fn assert_ty<T: CreateTypeSpec>(ord: &[u8]) {
    let mut writer = Vec::new();
    let vec = T::__ty_generate_spec(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new());
    vec.serialize_abi(&mut writer).unwrap();
    assert_eq!(&writer, ord);
}

/// Must be able to [`CreateTypeSpec`] for maps from [`u32`] to [`String`].
#[test]
pub fn avl_u32_str() {
    type T = AvlTreeMap<u32, String>;
    assert_eq!(T::__ty_name(), "AvlTreeMapU32String");
    assert_eq!(T::__ty_identifier(), "AvlTreeMap<U32, String>");
    assert_ty::<T>(&[0x19, 0x03, 0x0b]);
}

/// Must be able to [`CreateTypeSpec`] with nested avl-trees with identifiers.
#[test]
pub fn avl_id_to_id_to_id() {
    type T = AvlTreeMap<EventSubscriptionId, AvlTreeMap<ExternalEventId, SecretVarId>>;
    assert_eq!(
        T::__ty_name(),
        "AvlTreeMapEventSubscriptionIdAvlTreeMapExternalEventIdSecretVarId"
    );
    assert!(T::__ty_identifier().starts_with("AvlTreeMap<"));
    assert_ty::<AvlTreeMap<ExternalEventId, SecretVarId>>(&[0x19, 0x00, 0x00, 0x00, 0x01]);
}

/// Type witnesses for AVL tree maps are sensitive to the keys and values.
#[test]
pub fn type_witnesses_for_avl_tree_map() {
    type T1 = AvlTreeMap<EventSubscriptionId, AvlTreeMap<ExternalEventId, SecretVarId>>;
    type T2 = AvlTreeMap<u32, String>;
    type T3 = AvlTreeMap<u64, String>;
    type T4 = AvlTreeMap<u32, u32>;

    assert_type_witness::<T1>("6E81A9C7DBB5BB093A2D0193FD59F95E3F9EBCBD504E8467472E47A76B7CAC76");
    assert_type_witness::<T2>("E4C3A3DE90FB9F4C30A624155FDA06FBE932857C47C4EBCC53CDA78F3FFA7FAE");
    assert_type_witness::<T3>("74B2DC7EED2DFE4ED66AEE0D313B844EDB350D9145D217D782A483E92072D635");
    assert_type_witness::<T4>("B473FDE332BABBB81C3E377D56C24E21652880AD7CF19AF42064DAB2D1588AC6");
}

fn assert_type_witness<T: CreateTypeSpec>(expected_hash: &str) {
    let type_witness = TypeWitness::of::<T>();
    assert_eq!(format!("{}", type_witness.hash), expected_hash);
}
