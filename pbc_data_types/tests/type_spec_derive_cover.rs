//! Tests of [`CreateTypeSpec`].

use pbc_abi::abi_model::TypeSpec;
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_abi::CreateTypeSpec;
use pbc_data_types::type_spec_default_impl;

struct DeriveTypeSpec {}

impl CreateTypeSpec for DeriveTypeSpec {
    type_spec_default_impl!("DeriveTypeSpec", String);
}

#[test]
fn derive_type_name() {
    assert_eq!(DeriveTypeSpec::__ty_name(), "DeriveTypeSpec")
}

#[test]
fn derive_type_identifier() {
    assert_eq!(DeriveTypeSpec::__ty_identifier(), "DeriveTypeSpec")
}

#[test]
fn derive_type_write() {
    let mut named_type_indexes = NamedTypeLookup::new();
    let mut named_type_specs = NamedTypeSpecs::new();
    let w = DeriveTypeSpec::__ty_generate_spec(&mut named_type_indexes, &mut named_type_specs);

    assert_eq!(w, TypeSpec::String)
}
