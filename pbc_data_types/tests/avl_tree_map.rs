//! Test of [`AvlTreeMap`]; a map backed by an AVL Tree, used through the WASM Binder.

use pbc_data_types::avl_tree_map::AvlTreeMap;
use pbc_lib::wasm_avl::{deserialize_avl_tree, does_tree_exist};
use pbc_traits::ReadWriteState;

/// [`AvlTreeMap`] can be printed using [`Debug`] trait.
#[test]
fn debug() {
    let tree: AvlTreeMap<i32, u128> = AvlTreeMap::new();

    assert_eq!(
        format!("{tree:?}"),
        "AvlTreeMap { key_type: PhantomData<i32>, value_type: PhantomData<u128>, tree_id: 0 }"
    );
}

/// [`AvlTreeMap`] can be initialized and interacted with as if it was a normal map.
#[test]
fn avl_tree_map() {
    let mut tree: AvlTreeMap<i32, u128> = AvlTreeMap::new();
    assert_eq!(tree.len(), 0);
    assert!(tree.is_empty());
    tree.insert(1, 12);
    assert_eq!(tree.len(), 1);
    assert!(!tree.is_empty());
    tree.insert(2, 42);
    assert_eq!(tree.len(), 2);
    assert!(!tree.is_empty());
    assert_eq!(tree.get(&1).unwrap(), 12);
    assert_eq!(tree.get(&2).unwrap(), 42);
    assert_eq!(tree.get(&3), None);

    tree.remove(&2);

    assert_eq!(tree.get(&2), None);

    assert!(!tree.contains_key(&2));
    assert!(tree.contains_key(&1));
}

/// [`AvlTreeMap`] can be deleted.
#[test]
fn avl_tree_map_delete_tree() {
    let tree: AvlTreeMap<i32, bool> = AvlTreeMap::new();
    assert!(does_tree_exist(0));
    tree.delete();
    assert!(!does_tree_exist(0))
}

/// [`AvlTreeMap`] can store data blobs.
#[test]
fn avl_tree_map_with_blobs() {
    let mut tree: AvlTreeMap<i32, Vec<u8>> = AvlTreeMap::new();
    assert_eq!(tree.len(), 0);
    assert!(tree.is_empty());
    tree.insert(1, vec![0, 1, 2, 3]);
    assert_eq!(tree.len(), 1);
    assert!(!tree.is_empty());
    tree.insert(2, vec![]);
    assert_eq!(tree.len(), 2);
    assert!(!tree.is_empty());
    tree.insert(99, vec![0xFF; 1000]);
    assert_eq!(tree.len(), 3);
    assert!(!tree.is_empty());
    assert_eq!(tree.get(&1).unwrap(), vec![0, 1, 2, 3]);
    assert_eq!(tree.get(&2).unwrap(), vec![]);
    assert_eq!(tree.get(&3), None);
    assert_eq!(tree.get(&98), None);
    assert_eq!(tree.get(&99).unwrap(), vec![0xFF; 1000]);
    assert_eq!(tree.get(&100), None);

    tree.remove(&2);

    assert_eq!(tree.get(&2), None);

    assert!(!tree.contains_key(&2));
    assert!(tree.contains_key(&1));
}

/// [`AvlTreeMap`] can be deserialized from state bytes.
#[test]
fn avl_deserialize() {
    let bytes: Vec<u8> = vec![
        1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 2, 0, 0, 0, 1, 1, 1, 0, 0, 0, 4, 1, 1, 0, 0, 0, 1, 1, 1,
        1, 0, 0, 0, 8, 1, 1, 0, 0, 0, 0,
    ];
    deserialize_avl_tree(&mut bytes.as_slice());
    let tree: AvlTreeMap<u8, bool> = AvlTreeMap::state_read_from(&mut vec![0, 0, 0, 0].as_slice());

    assert!(tree.get(&4).unwrap());
    assert!(!tree.get(&8).unwrap());
    assert_eq!(tree.get(&1), None);
}

/// [`AvlTreeMap`] can be iterated using standard Rust iterator pattern.
#[test]
fn avl_iter() {
    let mut tree: AvlTreeMap<i32, u128> = AvlTreeMap::new();
    tree.insert(3, 32);
    tree.insert(1, 12);
    tree.insert(4, 42);
    tree.insert(2, 22);
    tree.insert(256, 2562);
    tree.insert(257, 2572);
    let values: Vec<(i32, u128)> = tree.iter().collect();
    assert_eq!(
        values,
        [(256, 2562), (1, 12), (257, 2572), (2, 22), (3, 32), (4, 42)]
    );
}

/// Iteration of [`AvlTreeMap`] can happen within the explicit `next` methods.
#[test]
fn avl_iter_explicit_next_copy_serializable() {
    let mut tree: AvlTreeMap<bool, bool> = AvlTreeMap::new();
    tree.insert(false, true);
    tree.insert(true, true);

    let mut iter = tree.iter();

    let val = iter.next();

    assert_eq!(val.unwrap(), (false, true));
    assert_eq!(iter.next().unwrap(), (true, true));
    assert_eq!(iter.next(), None);
}

/// When iterating beyond all entries in an [`AvlTreeMap`] it will return [`None`].
#[test]
fn avl_iter_too_much() {
    let mut tree: AvlTreeMap<i32, u128> = AvlTreeMap::new();
    tree.insert(3, 32);
    let mut iter = tree.iter();
    assert_eq!(iter.next(), Some((3, 32)));
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
}

/// The iterator over [`AvlTreeMap`] supports types that are not `SERIALIZABLE_BY_COPY`.
#[test]
fn avl_iter_not_serializable_by_copy() {
    let mut tree: AvlTreeMap<i32, Option<u128>> = AvlTreeMap::new();
    tree.insert(3, Some(32));
    tree.insert(1, None);
    tree.insert(4, Some(42));
    tree.insert(2, Some(22));
    let values: Vec<(i32, Option<u128>)> = tree.iter().collect();
    assert_eq!(
        values,
        [(1, None), (2, Some(22)), (3, Some(32)), (4, Some(42))]
    );
}
