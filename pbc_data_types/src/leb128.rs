//! Utility functions for working with leb128 encodings

/// Encode unsigned 32-bit ints as LEB128.
pub const fn to_leb128_bytes_const(mut value: u32) -> [u8; 5] {
    if value == 0 {
        return [0; 5];
    }

    let mut result = [0; 5];
    let mut i = 0;
    while value != 0 {
        let lower_seven = value & 0x7f;
        value >>= 7;

        let high_bit = if value != 0 { 0x80 } else { 0 };
        result[i] = (lower_seven | high_bit) as u8;
        i += 1;
    }
    result
}

/// Validate that bytes are leb128 encoded
pub fn validate_leb128_bytes(bytes: &[u8]) -> Result<(), String> {
    if bytes.len() > 5 {
        return Err("Shortname value too large for u32".to_string());
    }

    match bytes.last() {
        None => {
            return Err("Shortname must not be empty".to_string());
        }
        Some(&b) if b >= 0x80 => {
            return Err("Shortname's last byte must not have continuation bit set".to_string());
        }
        Some(&b) if b == 0x00 && bytes.len() > 1 => {
            return Err("Shortname must be normalized, with no trailing zeroes".to_string());
        }
        Some(&b) if b > 0x0f && bytes.len() == 5 => {
            return Err("Shortname value too large for u32".to_string());
        }
        _ => {} // Good
    }

    // Global validation
    let all_non_last_bytes_possess_continuation_bit =
        bytes.iter().rev().skip(1).all(|&b| b >= 0x80);
    if !all_non_last_bytes_possess_continuation_bit {
        return Err("Shortname's non-last bytes must have their continuation bits set".to_string());
    }
    Ok(())
}

/// Decode leb128 to u32
pub fn to_u32(bytes: &[u8]) -> Result<u32, String> {
    validate_leb128_bytes(bytes)?;

    let value_bytes: Vec<_> = bytes
        .iter()
        .enumerate()
        .map(|(i, &b)| actual_checked_shl(b as u32 & 0x7F, i as u32 * 7))
        .collect();

    Ok(value_bytes.iter().map(|x| x.unwrap()).sum())
}

/// Strips trailing zeros from slice
pub fn strip_trailing_zeros(bytes: &[u8]) -> &[u8] {
    if let Some(i) = bytes.iter().rposition(|&x| x != 0) {
        &bytes[..i + 1]
    } else {
        &bytes[..1]
    }
}

fn actual_checked_shl(lhs: u32, rhs: u32) -> Option<u32> {
    lhs.checked_shl(rhs).filter(|result| result >> rhs == lhs)
}

#[cfg(test)]
mod test {
    use super::to_leb128_bytes_const;

    #[test]
    fn leb() {
        assert_eq!(to_leb128_bytes_const(0), [0, 0, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(1), [1, 0, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(65), [65, 0, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(127), [127, 0, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(128), [128, 1, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(192), [192, 1, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(255), [255, 1, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(256), [128, 2, 0, 0, 0]);
        assert_eq!(to_leb128_bytes_const(624485), [0xE5, 0x8E, 0x26, 0, 0]);
        assert_eq!(
            to_leb128_bytes_const(0xFFFFFFFF),
            [0xFF, 0xFF, 0xFF, 0xFF, 0x0F]
        );
    }
}
