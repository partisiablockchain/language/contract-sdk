//! Defines the [`Hash`] data structure, and ways to create and operate on it.

use crate::type_spec_default_impl;
use pbc_abi::CreateTypeSpec;
use sha2::{Digest, Sha256};
use std::fmt;

use read_write_rpc_derive::ReadRPC;
use read_write_rpc_derive::WriteRPC;
use read_write_state_derive::ReadWriteState;

/// A SHA256 hash used on Partisia Blockchain to uniquely identify various types of objects.
///
/// Objects that are uniquely identified using [`struct@Hash`]:
///
/// - Transactions.
/// - Code blobs.
/// - Type witnesses.
#[derive(Eq, PartialEq, Clone, PartialOrd, Ord, ReadWriteState, ReadRPC, WriteRPC)]
pub struct Hash {
    /// The bytes of the hash.
    pub bytes: [u8; 32],
}

impl CreateTypeSpec for Hash {
    type_spec_default_impl!("Hash", Hash);
}

impl Hash {
    /// Generate [`struct@Hash`] from the given data.
    pub fn digest(data: &[u8]) -> Self {
        let bytes = Sha256::digest(data).into();
        Self { bytes }
    }
}

impl fmt::UpperHex for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for b in self.bytes {
            write!(f, "{:02X}", b)?;
        }
        Ok(())
    }
}

impl fmt::LowerHex for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for b in self.bytes {
            write!(f, "{:02x}", b)?;
        }
        Ok(())
    }
}

impl fmt::Display for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::UpperHex::fmt(self, f)
    }
}

impl fmt::Debug for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::UpperHex::fmt(self, f)
    }
}
