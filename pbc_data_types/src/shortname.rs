//! Definitions for contract invocation [`Shortname`]s and similar identifiers.

use pbc_traits::WriteRPC;
use std::io::Write;

use crate::leb128;
use leb128::{to_leb128_bytes_const, validate_leb128_bytes};

/// Unique [LEB128](https://en.wikipedia.org/wiki/LEB128) identifier for a contract's invocations.
///
/// Automatically created for each of the contract's own invocations. Shortnames for invocations in other contracts must be created when
/// calling those contracts.
#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub struct Shortname {
    /// Raw bytes
    value: [u8; 5],
}

impl Shortname {
    /// Create [`Shortname`] from an [`u32`].
    pub const fn from_u32(value: u32) -> Self {
        Self {
            value: to_leb128_bytes_const(value),
        }
    }

    /// Create [`Shortname`] from a slice of bytes. Slice must be valid [LEB128](https://en.wikipedia.org/wiki/LEB128)-encoded.
    pub fn from_be_bytes(bytes: &[u8]) -> Result<Self, String> {
        // Errors for last byte
        validate_leb128_bytes(bytes)?;

        let mut value = [0; 5];
        value[..bytes.len()].copy_from_slice(bytes);
        Ok(Self { value })
    }

    /// Create shortname from raw bytes with no validation check
    pub const fn from_bytes_raw(bytes: [u8; 5]) -> Self {
        Self { value: bytes }
    }

    /// Gets the [`Shortname`] as its [`u32`] representation.
    ///
    /// Note invariant:
    ///
    /// ```
    /// # use pbc_data_types::shortname::Shortname;
    /// # let i = 1231;
    /// assert_eq!(i, Shortname::from_u32(i).as_u32());
    /// ```
    pub fn as_u32(&self) -> u32 {
        leb128::to_u32(leb128::strip_trailing_zeros(&self.value)).unwrap()
    }

    /// Gets the [`Shortname`] as its bytes representation.
    ///
    /// Invariants:
    /// - At least one byte long.
    /// - Last byte is less than 0x80.
    /// - Preceding bytes are larger than 0x80.
    pub fn bytes(&self) -> Vec<u8> {
        leb128::strip_trailing_zeros(&self.value).to_vec()
    }

    /// Gets the [`Shortname`] as its bytes representation.
    ///
    /// Invariants:
    /// - At least one byte long.
    /// - Last byte is less than 0x80.
    /// - Preceding bytes are larger than 0x80.
    pub fn bytes_raw(&self) -> &[u8; 5] {
        &self.value
    }
}

/// Special [`Shortname`] variant for `#[callback]` invocations.
#[non_exhaustive]
#[derive(Eq, PartialEq, Debug, Clone)]
pub struct ShortnameCallback {
    /// Internal [`Shortname`].
    pub shortname: Shortname,
}

impl ShortnameCallback {
    /// Create [`ShortnameCallback`] from an [`u32`].
    pub const fn from_u32(value: u32) -> Self {
        Self {
            shortname: Shortname::from_u32(value),
        }
    }

    /// Create [`ShortnameCallback`] from an raw bytes.
    pub const fn from_bytes_raw(value: [u8; 5]) -> Self {
        Self {
            shortname: Shortname::from_bytes_raw(value),
        }
    }

    /// Create new [`ShortnameCallback`] from [`Shortname`]
    pub fn new(shortname: Shortname) -> Self {
        ShortnameCallback { shortname }
    }
}

/// Special [`Shortname`] variant for `#[zk_compute]` Zero-knowledge computation invocations.
#[non_exhaustive]
#[derive(Eq, PartialEq, Debug, Clone)]
pub struct ShortnameZkComputation {
    /// Internal shortname
    pub shortname: Shortname,
}

impl ShortnameZkComputation {
    /// Create [`ShortnameZkComputation`] from an [`u32`].
    pub const fn from_u32(value: u32) -> Self {
        Self {
            shortname: Shortname::from_u32(value),
        }
    }

    /// Create [`ShortnameZkComputation`] from an raw bytes.
    pub const fn from_bytes_raw(value: [u8; 5]) -> Self {
        Self {
            shortname: Shortname::from_bytes_raw(value),
        }
    }

    /// Create [`ShortnameZkComputation`] from an [`Shortname`].
    pub fn new(shortname: Shortname) -> Self {
        ShortnameZkComputation { shortname }
    }
}

/// Special [`Shortname`] variant for `#[zk_on_compute_complete]` invocations.
#[non_exhaustive]
#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub struct ShortnameZkComputeComplete {
    /// Internal shortname.
    pub shortname: Shortname,
}

impl ShortnameZkComputeComplete {
    /// Create [`ShortnameZkComputeComplete`] from an [`u32`]
    pub const fn from_u32(value: u32) -> Self {
        Self {
            shortname: Shortname::from_u32(value),
        }
    }

    /// Create [`ShortnameZkComputeComplete`] from an raw bytes.
    pub const fn from_bytes_raw(value: [u8; 5]) -> Self {
        Self {
            shortname: Shortname::from_bytes_raw(value),
        }
    }

    /// Create [`ShortnameZkComputeComplete`] from a [`Shortname`]
    pub fn new(shortname: Shortname) -> Self {
        ShortnameZkComputeComplete { shortname }
    }
}

/// Special [`Shortname`] variant for `#[zk_on_variable_inputted]` invocations.
#[non_exhaustive]
#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub struct ShortnameZkVariableInputted {
    /// Internal shortname.
    pub shortname: Shortname,
}

impl ShortnameZkVariableInputted {
    /// Create [`ShortnameZkVariableInputted`] from an [`u32`]
    pub const fn from_u32(value: u32) -> Self {
        Self {
            shortname: Shortname::from_u32(value),
        }
    }

    /// Create [`ShortnameZkVariableInputted`] from an raw bytes.
    pub const fn from_bytes_raw(value: [u8; 5]) -> Self {
        Self {
            shortname: Shortname::from_bytes_raw(value),
        }
    }

    /// Create [`ShortnameZkVariableInputted`] from a [`Shortname`]
    pub fn new(shortname: Shortname) -> Self {
        ShortnameZkVariableInputted { shortname }
    }
}

impl std::fmt::Display for Shortname {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for byte in &self.bytes() {
            write!(f, "{byte:02x}")?;
        }
        Ok(())
    }
}

impl WriteRPC for Shortname {
    fn rpc_write_to<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
        for item in &self.bytes() {
            item.rpc_write_to(writer)?;
        }

        Ok(())
    }
}

impl WriteRPC for ShortnameZkVariableInputted {
    fn rpc_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.shortname.rpc_write_to(writer)
    }
}

impl WriteRPC for ShortnameZkComputeComplete {
    fn rpc_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.shortname.rpc_write_to(writer)
    }
}
