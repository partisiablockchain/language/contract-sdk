#![doc = include_str!("../README.md")]
extern crate quote;

// Internal modules to be reexported

mod address_internal;

// Directly exported modules

use std::io::{Read, Write};

pub mod signature;

pub mod avl_tree_map;

pub mod zk;

use pbc_abi::CreateTypeSpec;
use pbc_traits::{ReadRPC, WriteRPC};
use read_write_rpc_derive::ReadWriteRPC;
use read_write_state_derive::ReadWriteState;

/// Definitions for blockchain [`Address`](address_internal::Address)es.
pub mod address {
    pub use super::address_internal::{Address, AddressType};
    pub use super::shortname::{
        Shortname, ShortnameCallback, ShortnameZkComputeComplete, ShortnameZkVariableInputted,
    };
}

mod hash;
mod leb128;
pub mod shortname;
pub use hash::Hash;

pub use sorted_vec_map;

/// Creates the code for implementing the CreateTypeSpec trait for built-in types.
///
/// The type name and identifier is simply the name of the type.
///
/// The serialization logic writes the identifier byte.
/// ### Parameters:
///
/// * `name`: [&str literal] - The name of the type.
///
/// * `identifier_byte`: byte literal - The identifying byte of the type, as specified by the abi.
///
/// ### Returns:
/// A default implementation of CreateTypeSpec for built-in types.
#[macro_export]
macro_rules! type_spec_default_impl {
    ($name: literal, $val: ident) => {
        fn __ty_name() -> String {
            $name.to_string()
        }

        fn __ty_identifier() -> String {
            Self::__ty_name()
        }

        fn __ty_generate_spec(
            _named_type_index_lookup: &mut pbc_abi::create_type_spec::NamedTypeLookup,
            _named_type_specs: &mut pbc_abi::create_type_spec::NamedTypeSpecs,
        ) -> pbc_abi::abi_model::TypeSpec {
            pbc_abi::abi_model::TypeSpec::$val
        }
    };
}

/// A public key is used to send encrypted transactions on the blockchain.
/// Transactions must be encrypted under a public key registered on the blockchain otherwise they will fail.
///
/// The key consists of a 33 byte array.
#[derive(Eq, PartialEq, Debug, Clone, PartialOrd, Ord, ReadWriteState, ReadWriteRPC)]
pub struct PublicKey {
    /// The bytes of the public key.
    pub bytes: [u8; 33],
}

impl CreateTypeSpec for PublicKey {
    type_spec_default_impl!("PublicKey", PublicKey);
}

/// A BLS (Boneh-Lynn-Shacham) is a different type of [public key](PublicKey), that allows for aggregation.
///
/// It is used to authenticate aggregated [BLS signatures](BlsSignature), by aggregating the public keys that was used for signing, into a 'single' key.
///
/// A BLS public key consists of a 96 byte array.
#[derive(Eq, PartialEq, Debug, Clone, PartialOrd, Ord, ReadWriteState, ReadWriteRPC)]
pub struct BlsPublicKey {
    /// The bytes of the BLS public key.
    pub bytes: [u8; 96],
}

impl CreateTypeSpec for BlsPublicKey {
    type_spec_default_impl!("BlsPublicKey", BlsPublicKey);
}

/// A BLS (Boneh-Lynn-Shacham) is a different type of [signature](signature::Signature), that allows for aggregation.
///
/// It is used to produce a joint (single) signature on e.g. a block between a group of users, rather than one signature per user as is the case for [signature](signature::Signature).
///
/// A BLS signature consists of a 48 byte array.
#[derive(Eq, PartialEq, Debug, Clone, PartialOrd, Ord, ReadWriteState, ReadWriteRPC)]
pub struct BlsSignature {
    /// The bytes of the BLS signature.
    pub bytes: [u8; 48],
}

impl CreateTypeSpec for BlsSignature {
    type_spec_default_impl!("BlsSignature", BlsSignature);
}

/// A u256 is a 256-bit unsigned integer.
///
/// It consists of a 32 byte array.
#[derive(Eq, PartialEq, Debug, Clone, PartialOrd, Ord, ReadWriteState)]
pub struct U256 {
    /// The bytes of the u256.
    pub bytes: [u8; 32],
}

impl CreateTypeSpec for U256 {
    type_spec_default_impl!("U256", U256);
}

impl ReadRPC for U256 {
    fn rpc_read_from<T: Read>(reader: &mut T) -> Self {
        let mut bytes: [u8; 32] = [0; 32];
        reader.read_exact(&mut bytes).unwrap();
        bytes.reverse();
        Self { bytes }
    }
}

impl WriteRPC for U256 {
    fn rpc_write_to<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        let mut bytes: [u8; 32] = self.bytes;
        bytes.reverse();
        writer.write_all(&bytes)
    }
}
