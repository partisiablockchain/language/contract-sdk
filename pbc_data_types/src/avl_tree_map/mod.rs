//! Provides a [`AvlTreeMap`] which allows for partial deserialization of state.
//!
//! [`AvlTreeMap`] should only be used for field variables in the smart contract state and should
//! not be initialized for intermediate computations
mod set;
mod tree;

pub use set::AvlTreeSet;
pub use tree::AvlTreeMap;
