//! [`AvlTreeSet`] provides a set implementation for contract states that loads information lazily from the underlying storage.
//! ### Safety:
//! [`AvlTreeSet::new()`] initialises an [`AvlTreeMap`], which is stored in the contract state, with no automatic garbage collection.
//! Therefore, care should be taken, otherwise it might bloat the contract state, requiring higher storage costs.
use crate::avl_tree_map::AvlTreeMap;
use create_type_spec_derive::CreateTypeSpecInternal;
use pbc_traits::ReadWriteState;
use read_write_state_derive::ReadWriteState;

/// [`Unit`] dummy object for all values in the internal [`AvlTreeMap`] in [`AvlTreeSet`].
#[derive(ReadWriteState, Debug, CreateTypeSpecInternal)]
struct Unit {}

/// [`AvlTreeSet`] a wrapper for [`AvlTreeMap`] and thus provides partial deserialization, enabling bigger smart contract states.
/// [`AvlTreeSet`] should not be used for intermediate computations, as there ís no automatic garbage collection for [`AvlTreeMap`].
///
/// A simple wrapper for [`AvlTreeMap`] allowing to store sets in a state that can be
/// partially deserialized.
/// This is in contrast to [`SortedVecSet`](crate::sorted_vec_map::SortedVecSet), which fully deserializes when stored in `#[state]`.
/// Partial state deserialization can enable smart contracts to have bigger states
/// with gas fees independent of the size of the state.
///
/// The values in the set are stored as keys of [`AvlTreeMap`].
/// ### Example:
/// Adapted from [BTreeSet](https://doc.rust-lang.org/std/collections/struct.BTreeSet.html#examples).
/// ```rust
/// use pbc_contract_common::avl_tree_map::AvlTreeSet;
/// let mut books: AvlTreeSet<String> = AvlTreeSet::new();
/// // Add some books.
/// books.insert(String::from("A Dance With Dragons"));
/// books.insert(String::from("To Kill a Mockingbird"));
/// books.insert(String::from("The Odyssey"));
/// books.insert(String::from("The Great Gatsby"));
///
/// // Check for a specific one.
/// if !books.contains(&String::from("The Winds of Winter")) {
///     println!(
///        "We have {} books, but The Winds of Winter ain't one.",
///        books.len()
///        );
/// }
///
/// // Remove a book.
/// books.remove(&String::from("The Odyssey"));
///
/// // Iterate over everything.
/// for book in books.iter() {
///    println!("{book}");
/// }
/// ```
#[derive(Debug, ReadWriteState, CreateTypeSpecInternal)]
pub struct AvlTreeSet<T> {
    inner_map: AvlTreeMap<T, Unit>,
}

/// Implementation of [`AvlTreeSet`], wrapper for [`AvlTreeMap`].
/// [`AvlTreeSet`] must only be used at the top level of the smart contract and should not be
/// initialized for intermediate computation.
impl<T: pbc_traits::ReadWriteState> AvlTreeSet<T> {
    /// Constructor for [`AvlTreeSet`].
    /// Wrapper for [`AvlTreeMap`], storing the values of the set
    /// as keys with no in the corresponding value.
    ///
    /// ### Safety:
    /// Since [`AvlTreeSet`] is a wrapper, there is no automatic garbage collection,
    /// thus, this should not be used for intermediate computation.
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        AvlTreeSet {
            inner_map: AvlTreeMap::new(),
        }
    }

    /// Check if the [`AvlTreeSet`] har the specifies value.
    ///
    /// ### Parameter:
    ///
    /// * 'value': The value to check for.
    ///
    /// ### Returns:
    ///
    /// * boolean indicating whether the value is present in the set.
    pub fn contains(&self, value: &T) -> bool {
        self.inner_map.contains_key(value)
    }

    /// Insert a value into [`AvlTreeSet`].
    ///
    /// ### Parameters:
    ///
    /// * value: The value to insert.
    pub fn insert(&mut self, value: T) {
        self.inner_map.insert(value, Unit {})
    }

    /// Remove the value from the [`AvlTreeSet`].
    ///
    /// ### Parameters:
    ///
    /// * value: The value to be removed.
    pub fn remove(&mut self, value: &T) {
        self.inner_map.remove(value)
    }

    /// Gets the number of distinct elements of the [`AvlTreeSet`].
    pub fn len(&self) -> usize {
        self.inner_map.len()
    }

    /// Returns `true` if [`AvlTreeSet`] is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Gets an iterator of [`AvlTreeSet`].
    pub fn iter(&self) -> impl Iterator<Item = T> {
        self.inner_map.iter().map(|(x, _u)| x)
    }
}
