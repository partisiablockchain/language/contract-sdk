//! Definitions specifically for Zero-Knowledge Contracts.
//!
//! These should be used in conjunction with the Zk macros in `pbc_contract_codegen`.

use std::io::{Read, Write};

use create_type_spec_derive::CreateTypeSpecInternal;
use pbc_zk_core::{SecretBinary, SecretBinaryFixedSize};
use read_write_rpc_derive::ReadRPC;
use read_write_rpc_derive::WriteRPC;
use read_write_state_derive::ReadWriteState;

/// Identifier for a secret variable.
#[repr(transparent)]
#[derive(
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    ReadRPC,
    WriteRPC,
    ReadWriteState,
    Debug,
    Clone,
    Copy,
    CreateTypeSpecInternal,
    Hash,
)]
#[non_exhaustive]
pub struct SecretVarId {
    /// Raw identifier of the secret variable.
    ///
    /// Should mainly be used for the few circumstances where [`SecretVarId`] itself cannot be
    /// used.
    pub raw_id: u32,
}

impl SecretVarId {
    /// Creates new secret var id
    pub const fn new(raw_id: u32) -> Self {
        Self { raw_id }
    }
}

impl SecretBinary for SecretVarId {
    fn secret_read_from<ReadT: Read>(reader: &mut ReadT) -> Self {
        <Self as pbc_traits::ReadWriteState>::state_read_from(reader)
    }

    fn secret_write_to<WriteT: Write>(&self, writer: &mut WriteT) -> std::io::Result<()> {
        <Self as pbc_traits::ReadWriteState>::state_write_to(self, writer)
    }
}

impl SecretBinaryFixedSize for SecretVarId {
    const BITS: u32 = <u32>::BITS;
}

/// Identifier for an attested piece of data.
///
/// # Invariants
///
/// Cannot be manually created; must be retrieved from state.
#[repr(transparent)]
#[derive(PartialEq, Eq, ReadRPC, WriteRPC, ReadWriteState, Debug, Clone, Copy)]
#[non_exhaustive]
pub struct AttestationId {
    raw_id: u32,
}

impl AttestationId {
    /// Creates new attestation id
    #[allow(dead_code)]
    pub const fn new(raw_id: u32) -> Self {
        Self { raw_id }
    }
}

/// An externally-owned EVM account address is the 20 least significant bytes of the Keccak-256 hash value of the public key of an account.
/// A contract EVM address is also 20 bytes, and is derived from the creator's address and nonce.
pub type EvmAddress = [u8; 20];

/// Identifies a concrete EVM compatible blockchain network.
pub type EvmChainId = String;

/// An event topic is an indexed piece of data attached to an event log, or used to filter events
/// logs. A topic is restricted to 32 bytes of data.
pub type EvmEventTopic = [u8; 32];

/// Identifier for an EVM event subscription.
///
/// # Invariants
///
/// Cannot be manually created; must be retrieved from state.
#[repr(transparent)]
#[derive(
    PartialEq, Eq, ReadRPC, WriteRPC, ReadWriteState, Debug, Clone, Copy, CreateTypeSpecInternal,
)]
#[non_exhaustive]
pub struct EventSubscriptionId {
    raw_id: i32,
}

impl EventSubscriptionId {
    /// Creates new subscription id
    #[allow(dead_code)]
    pub const fn new(raw_id: i32) -> Self {
        Self { raw_id }
    }
}

/// Identifier for an EVM event.
///
/// # Invariants
///
/// Cannot be manually created; must be retrieved from state.
#[repr(transparent)]
#[derive(
    PartialEq, Eq, ReadRPC, WriteRPC, ReadWriteState, Debug, Clone, Copy, CreateTypeSpecInternal,
)]
#[non_exhaustive]
pub struct ExternalEventId {
    raw_id: i32,
}

impl ExternalEventId {
    /// Creates new event id
    #[allow(dead_code)]
    pub const fn new(raw_id: i32) -> Self {
        Self { raw_id }
    }
}
