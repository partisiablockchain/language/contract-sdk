# Partisia Blockchain Data Types

Defines the special data types, such as an `Address` of `PublicKey`, present on the Partisia Blockchain.

Provides a special implementation of collections types such as `Vec` or `Map` to be more efficient when used on the
Partisia Blockchain.

Defines a special map data type, `AvlTreeMap` which allows for partial deserialization of state.