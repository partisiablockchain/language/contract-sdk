//! Test of generated abi type names
#![cfg(feature = "abi")]
use pbc_abi::abi_model::AbiSerialize;
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_abi::CreateTypeSpec;
use std::collections::{BTreeSet, VecDeque};
use std::marker::PhantomData;

use pbc_contract_common::sorted_vec_map::{SortedVec, SortedVecMap, SortedVecSet};
use pbc_zk::{Sbi128, Sbi16, Sbi32, Sbi64, Sbi8};

#[test]
fn ty_names_simple_types() {
    assert_eq!(u8::__ty_name(), "U8");
    assert_eq!(i8::__ty_name(), "I8");
    assert_eq!(u16::__ty_name(), "U16");
    assert_eq!(i16::__ty_name(), "I16");
    assert_eq!(u32::__ty_name(), "U32");
    assert_eq!(i32::__ty_name(), "I32");
    assert_eq!(u64::__ty_name(), "U64");
    assert_eq!(i64::__ty_name(), "I64");
    assert_eq!(u128::__ty_name(), "U128");
    assert_eq!(i128::__ty_name(), "I128");
    assert_eq!(String::__ty_name(), "String");
    assert_eq!(bool::__ty_name(), "Bool");
}

#[test]
fn ty_names_for_sbi_types() {
    assert_eq!(Sbi8::__ty_name(), "Sbi8");
    assert_eq!(Sbi8::__ty_identifier(), "Sbi8");
    assert_eq!(Sbi16::__ty_name(), "Sbi16");
    assert_eq!(Sbi16::__ty_identifier(), "Sbi16");
    assert_eq!(Sbi32::__ty_name(), "Sbi32");
    assert_eq!(Sbi32::__ty_identifier(), "Sbi32");
    assert_eq!(Sbi64::__ty_name(), "Sbi64");
    assert_eq!(Sbi64::__ty_identifier(), "Sbi64");
    assert_eq!(Sbi128::__ty_name(), "Sbi128");
    assert_eq!(Sbi128::__ty_identifier(), "Sbi128");
}

#[test]
fn ty_names_complex_types() {
    assert_eq!(<BTreeSet<String>>::__ty_name(), "BTreeSetString");
    assert_eq!(<BTreeSet<String>>::__ty_identifier(), "BTreeSet<String>");
    assert_eq!(<Vec<String>>::__ty_name(), "VecString");
    assert_eq!(<VecDeque<String>>::__ty_name(), "VecDequeString");
    assert_eq!(<VecDeque<String>>::__ty_identifier(), "VecDeque<String>");
    assert_eq!(<Option<String>>::__ty_name(), "OptionString");
    assert_eq!(<Option<String>>::__ty_identifier(), "Option<String>");

    assert_eq!(
        <Vec<Vec<Vec<BTreeSet<BTreeSet<Vec<BTreeSet<String>>>>>>>>::__ty_name(),
        "VecVecVecBTreeSetBTreeSetVecBTreeSetString"
    );
}

#[test]
fn ty_names_arrays_u8() {
    assert_eq!(<[u8; 1]>::__ty_name(), "U81");
    assert_eq!(<[u8; 2]>::__ty_name(), "U82");
    assert_eq!(<[u8; 3]>::__ty_name(), "U83");
    assert_eq!(<[u8; 4]>::__ty_name(), "U84");
    assert_eq!(<[u8; 5]>::__ty_name(), "U85");
    assert_eq!(<[u8; 6]>::__ty_name(), "U86");
    assert_eq!(<[u8; 7]>::__ty_name(), "U87");
    assert_eq!(<[u8; 8]>::__ty_name(), "U88");
    assert_eq!(<[u8; 9]>::__ty_name(), "U89");
    assert_eq!(<[u8; 10]>::__ty_name(), "U810");
    assert_eq!(<[u8; 11]>::__ty_name(), "U811");
    assert_eq!(<[u8; 12]>::__ty_name(), "U812");
    assert_eq!(<[u8; 13]>::__ty_name(), "U813");
    assert_eq!(<[u8; 14]>::__ty_name(), "U814");
    assert_eq!(<[u8; 15]>::__ty_name(), "U815");
    assert_eq!(<[u8; 16]>::__ty_name(), "U816");
    assert_eq!(<[u8; 16]>::__ty_identifier(), "U816");
    assert_eq!(<[u8; 17]>::__ty_name(), "U817");
    assert_eq!(<[u8; 18]>::__ty_name(), "U818");
    assert_eq!(<[u8; 19]>::__ty_name(), "U819");
    assert_eq!(<[u8; 20]>::__ty_name(), "U820");
    assert_eq!(<[u8; 21]>::__ty_name(), "U821");
    assert_eq!(<[u8; 22]>::__ty_name(), "U822");
    assert_eq!(<[u8; 23]>::__ty_name(), "U823");
    assert_eq!(<[u8; 24]>::__ty_name(), "U824");
    assert_eq!(<[u8; 25]>::__ty_name(), "U825");
    assert_eq!(<[u8; 26]>::__ty_name(), "U826");
    assert_eq!(<[u8; 27]>::__ty_name(), "U827");
    assert_eq!(<[u8; 28]>::__ty_name(), "U828");
    assert_eq!(<[u8; 29]>::__ty_name(), "U829");
    assert_eq!(<[u8; 30]>::__ty_name(), "U830");
    assert_eq!(<[u8; 31]>::__ty_name(), "U831");
    assert_eq!(<[u8; 32]>::__ty_name(), "U832");
    assert_eq!(<[u8; 101]>::__ty_name(), "U8101");
    assert_eq!(<[u8; 101]>::__ty_identifier(), "U8101");
}

#[test]
fn ty_names_arrays_misc() {
    assert_eq!(<[u32; 101]>::__ty_name(), "U32101");
    assert_eq!(<[Option<u32>; 101]>::__ty_name(), "OptionU32101");
}

#[track_caller]
fn assert_ty<T: CreateTypeSpec>(ord: &[u8]) {
    let mut writer = Vec::new();
    let spec = T::__ty_generate_spec(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new());
    spec.serialize_abi(&mut writer).unwrap();
    assert_eq!(&writer, ord);
}

#[test]
fn ty_ordinals_simple_types() {
    assert_ty::<u8>(&[0x01]);
    assert_ty::<u16>(&[0x02]);
    assert_ty::<u32>(&[0x03]);
    assert_ty::<u64>(&[0x04]);
    assert_ty::<u128>(&[0x05]);

    assert_ty::<i8>(&[0x06]);
    assert_ty::<i16>(&[0x07]);
    assert_ty::<i32>(&[0x08]);
    assert_ty::<i64>(&[0x09]);
    assert_ty::<i128>(&[0x0a]);

    assert_ty::<String>(&[0x0b]);
    assert_ty::<bool>(&[0x0c]);
}

#[test]
fn ty_ordinals_for_sbi_matches_public_counter_part() {
    let indexes = &mut NamedTypeLookup::new();
    let specs = &mut NamedTypeSpecs::new();
    assert_eq!(
        Sbi8::__ty_generate_spec(indexes, specs),
        i8::__ty_generate_spec(indexes, specs)
    );
    assert_eq!(
        Sbi16::__ty_generate_spec(indexes, specs),
        i16::__ty_generate_spec(indexes, specs)
    );
    assert_eq!(
        Sbi32::__ty_generate_spec(indexes, specs),
        i32::__ty_generate_spec(indexes, specs)
    );
    assert_eq!(
        Sbi64::__ty_generate_spec(indexes, specs),
        i64::__ty_generate_spec(indexes, specs)
    );
    assert_eq!(
        Sbi128::__ty_generate_spec(indexes, specs),
        i128::__ty_generate_spec(indexes, specs)
    );
}

#[test]
fn ty_ordinals_complex_types() {
    assert_ty::<Vec<u8>>(&[0x0e, 0x01]);
    assert_ty::<Vec<u16>>(&[0x0e, 0x02]);
    assert_ty::<Vec<u32>>(&[0x0e, 0x03]);
    assert_ty::<Vec<u64>>(&[0x0e, 0x04]);
    assert_ty::<Vec<u128>>(&[0x0e, 0x05]);

    assert_ty::<Vec<i8>>(&[0x0e, 0x06]);
    assert_ty::<Vec<i16>>(&[0x0e, 0x07]);
    assert_ty::<Vec<i32>>(&[0x0e, 0x08]);
    assert_ty::<Vec<i64>>(&[0x0e, 0x09]);
    assert_ty::<Vec<i128>>(&[0x0e, 0x0a]);

    assert_ty::<VecDeque<u8>>(&[0x0e, 0x01]);
    assert_ty::<VecDeque<u16>>(&[0x0e, 0x02]);
    assert_ty::<VecDeque<u32>>(&[0x0e, 0x03]);
    assert_ty::<VecDeque<u64>>(&[0x0e, 0x04]);
    assert_ty::<VecDeque<u128>>(&[0x0e, 0x05]);

    assert_ty::<VecDeque<i8>>(&[0x0e, 0x06]);
    assert_ty::<VecDeque<i16>>(&[0x0e, 0x07]);
    assert_ty::<VecDeque<i32>>(&[0x0e, 0x08]);
    assert_ty::<VecDeque<i64>>(&[0x0e, 0x09]);
    assert_ty::<VecDeque<i128>>(&[0x0e, 0x0a]);

    assert_ty::<Option<u8>>(&[0x12, 0x01]);
    assert_ty::<Option<u16>>(&[0x12, 0x02]);
    assert_ty::<Option<u32>>(&[0x12, 0x03]);
    assert_ty::<Option<u64>>(&[0x12, 0x04]);
    assert_ty::<Option<u128>>(&[0x12, 0x05]);

    assert_ty::<Option<i8>>(&[0x12, 0x06]);
    assert_ty::<Option<i16>>(&[0x12, 0x07]);
    assert_ty::<Option<i32>>(&[0x12, 0x08]);
    assert_ty::<Option<i64>>(&[0x12, 0x09]);
    assert_ty::<Option<i128>>(&[0x12, 0x0a]);
    assert_ty::<Option<Option<i128>>>(&[0x12, 0x12, 0x0a]);

    assert_ty::<BTreeSet<i128>>(&[0x10, 0x0a]);

    assert_ty::<BTreeSet<Vec<BTreeSet<Vec<Vec<String>>>>>>(&[0x10, 0x0e, 0x10, 0x0e, 0x0e, 0x0b]);
}

#[test]
fn ty_ordinals_arrays_u8() {
    assert_ty::<[u8; 1]>(&[0x1a, 0x01, 0x1]);
    assert_ty::<[u8; 2]>(&[0x1a, 0x01, 0x2]);
    assert_ty::<[u8; 3]>(&[0x1a, 0x01, 0x3]);
    assert_ty::<[u8; 4]>(&[0x1a, 0x01, 0x4]);
    assert_ty::<[u8; 5]>(&[0x1a, 0x01, 0x5]);
    assert_ty::<[u8; 6]>(&[0x1a, 0x01, 0x6]);
    assert_ty::<[u8; 7]>(&[0x1a, 0x01, 0x7]);
    assert_ty::<[u8; 8]>(&[0x1a, 0x01, 0x8]);
    assert_ty::<[u8; 9]>(&[0x1a, 0x01, 0x9]);
    assert_ty::<[u8; 10]>(&[0x1a, 0x01, 0xa]);
    assert_ty::<[u8; 11]>(&[0x1a, 0x01, 0xb]);
    assert_ty::<[u8; 12]>(&[0x1a, 0x01, 0xc]);
    assert_ty::<[u8; 13]>(&[0x1a, 0x01, 0xd]);
    assert_ty::<[u8; 14]>(&[0x1a, 0x01, 0xe]);
    assert_ty::<[u8; 15]>(&[0x1a, 0x01, 0xf]);
    assert_ty::<[u8; 16]>(&[0x1a, 0x01, 0x10]);
    assert_ty::<[u8; 17]>(&[0x1a, 0x01, 0x11]);
    assert_ty::<[u8; 18]>(&[0x1a, 0x01, 0x12]);
    assert_ty::<[u8; 19]>(&[0x1a, 0x01, 0x13]);
    assert_ty::<[u8; 20]>(&[0x1a, 0x01, 0x14]);
    assert_ty::<[u8; 21]>(&[0x1a, 0x01, 0x15]);
    assert_ty::<[u8; 22]>(&[0x1a, 0x01, 0x16]);
    assert_ty::<[u8; 23]>(&[0x1a, 0x01, 0x17]);
    assert_ty::<[u8; 24]>(&[0x1a, 0x01, 0x18]);
    assert_ty::<[u8; 25]>(&[0x1a, 0x01, 0x19]);
    assert_ty::<[u8; 26]>(&[0x1a, 0x01, 0x1a]);
    assert_ty::<[u8; 27]>(&[0x1a, 0x01, 0x1b]);
    assert_ty::<[u8; 28]>(&[0x1a, 0x01, 0x1c]);
    assert_ty::<[u8; 29]>(&[0x1a, 0x01, 0x1d]);
    assert_ty::<[u8; 30]>(&[0x1a, 0x01, 0x1e]);
    assert_ty::<[u8; 31]>(&[0x1a, 0x01, 0x1f]);
    assert_ty::<[u8; 32]>(&[0x1a, 0x01, 0x20]);
    assert_ty::<[u8; 101]>(&[0x1a, 0x01, 101]);
}

#[test]
fn ty_ordinals_arrays_misc() {
    assert_ty::<[u32; 10]>(&[0x1a, 0x03, 0x0a]);
    assert_ty::<[Option<u32>; 4]>(&[0x1a, 0x12, 0x03, 0x04]);
}

#[test]
fn sorted_vec() {
    assert_eq!(<SortedVec<u32>>::__ty_name(), "SortedVecU32");
    assert_eq!(
        <SortedVec<SortedVec<u32>>>::__ty_name(),
        "SortedVecSortedVecU32"
    );
    assert_eq!(<SortedVec<u32>>::__ty_identifier(), "SortedVec<U32>");
    assert_eq!(
        <SortedVec<SortedVec<u32>>>::__ty_identifier(),
        "SortedVec<SortedVec<U32>>"
    );
    assert_ty::<SortedVec<u32>>(&[0x0e, 0x03]);
    assert_ty::<SortedVec<SortedVec<u32>>>(&[0x0e, 0x0e, 0x03]);
}

#[test]
fn sorted_vec_map() {
    assert_eq!(
        <SortedVecMap<u32, String>>::__ty_name(),
        "SortedVecMapU32String"
    );
    assert_eq!(
        <SortedVecMap<u32, SortedVecMap<String, u32>>>::__ty_name(),
        "SortedVecMapU32SortedVecMapStringU32"
    );
    assert_eq!(
        <SortedVecMap<u32, String>>::__ty_identifier(),
        "SortedVecMap<U32, String>"
    );
    assert_eq!(
        <SortedVecMap<u32, SortedVecMap<String, u32>>>::__ty_identifier(),
        "SortedVecMap<U32, SortedVecMap<String, U32>>"
    );
    assert_ty::<SortedVecMap<u32, String>>(&[0x0f, 0x03, 0xb]);
    assert_ty::<SortedVecMap<u32, SortedVecMap<String, u32>>>(&[0x0f, 0x03, 0x0f, 0xb, 0x03]);
}

#[test]
fn sorted_vec_set() {
    assert_eq!(<SortedVecSet<u32>>::__ty_name(), "SortedVecSetU32");
    assert_eq!(
        <SortedVecSet<SortedVecSet<u32>>>::__ty_name(),
        "SortedVecSetSortedVecSetU32"
    );
    assert_eq!(<SortedVecSet<u32>>::__ty_identifier(), "SortedVecSet<U32>");
    assert_eq!(
        <SortedVecSet<SortedVecSet<u32>>>::__ty_identifier(),
        "SortedVecSet<SortedVecSet<U32>>"
    );
    assert_ty::<SortedVecSet<u32>>(&[0x10, 0x03]);
    assert_ty::<SortedVecSet<SortedVecSet<u32>>>(&[0x10, 0x10, 0x03]);
}

/// [`PhantomData`] are of the format `PhantomData<TYPE>`.
#[test]
fn phantom_data() {
    assert_eq!(<PhantomData<u32>>::__ty_name(), "PhantomDataU32");
    assert_eq!(<PhantomData<String>>::__ty_name(), "PhantomDataString");
    assert_eq!(<PhantomData<u32>>::__ty_identifier(), "PhantomData<U32>");
    assert_eq!(
        <PhantomData<String>>::__ty_identifier(),
        "PhantomData<String>"
    );
}
