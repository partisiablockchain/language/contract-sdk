#![no_main]
use create_type_spec_derive::CreateTypeSpec;

#[derive(CreateTypeSpec)]
struct MyStruct {
    field: (u32, u32),
}