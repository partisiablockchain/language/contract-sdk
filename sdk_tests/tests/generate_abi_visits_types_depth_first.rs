//! Test that named types are visited in a deterministic order, depth-first starting from the
//! left-most type, when generating the contract ABI.
//!
//! This guarantees that the order of named types of a contract's state is always the same
//! when generating the ABI of the contract.
//! Named types are ordered in the types list as depth-first, when traversing the state type tree.
//! For example for a state with types {x: X, y: Y} where X has {z: Z}, the types appear in order
//! [X, Z, Y].
//!
//! Specifically, it is the derived implementations of
//! [`pbc_traits::CreateTypeSpec::__ty_generate_spec`] that are being tested.
//!
//! The feature does not guarantee the order of the functions, and thus named types defined in their
//! arguments, are the same. It is the responsibility of the caller of
//! [`pbc_contract_core::abi::generate::generate_abi`] to ensure that order.
//! However, it is guaranteed that the order of the received function list is preserved.
#![allow(dead_code)]
#![allow(unused_imports)]
#![cfg(feature = "abi")]

use create_type_spec_derive::CreateTypeSpec;
use pbc_abi::abi_model::NamedTypeSpec;
use pbc_contract_codegen::{action, zk_on_secret_input};
use pbc_contract_common::abi::generate::generate_contract_abi;
use pbc_contract_common::address::Address;
use pbc_contract_common::avl_tree_map::AvlTreeMap;
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::events::EventGroup;
use pbc_contract_common::zk::{ZkInputDef, ZkState};
use pbc_data_types::shortname::Shortname;
use pbc_traits::ReadRPC;
use pbc_zk::{Sbi32, SecretBinary};
use read_write_rpc_derive::ReadRPC;
use read_write_state_derive::ReadWriteState;

#[derive(CreateTypeSpec)]
struct StateWithStructs {
    a: StructA,
    x: u32,
    b: StructB,
}

#[derive(CreateTypeSpec, ReadRPC)]
struct StructA {
    _c: StructC,
}

#[derive(CreateTypeSpec, ReadRPC)]
struct StructB {}

#[derive(CreateTypeSpec, ReadRPC)]
struct StructC {}

/// When generating a [`ContractAbi`] from a state with structs, the fields are visited depth-first
/// starting from the left-most type. Given state {a: A, b: B} where A and B are structs and A is
/// {c: C}, the types are visited in order [A, C, B].
#[test]
fn fields_in_structs_are_visited_depth_first_starting_from_the_left() {
    let contract = unsafe { generate_contract_abi::<StateWithStructs>(vec![]) };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec!["StateWithStructs", "StructA", "StructC", "StructB"],
    );
}

#[derive(CreateTypeSpec)]
enum StateEnumA {
    #[discriminant(0)]
    EnumVariantB { e: bool },
    #[discriminant(1)]
    EnumVariantC { e: EnumVariantFieldE },
    #[discriminant(2)]
    EnumVariantD { e: Address },
}

#[derive(CreateTypeSpec)]
struct EnumVariantFieldE {}

/// When generating a [`ContractAbi`] from a named Enum the variants and the variants' fields are
/// visited depth-first starting from the left-most type. Given Enum A with variants B, C, D where
/// C is { e: E }, the types are visited in order [A, B, C, E, D]
#[test]
fn variants_and_fields_in_enums_are_visited_depth_first() {
    let contract = unsafe { generate_contract_abi::<StateEnumA>(vec![]) };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec![
            "StateEnumA",
            "EnumVariantB",
            "EnumVariantC",
            "EnumVariantFieldE",
            "EnumVariantD",
        ],
    );
}

#[derive(CreateTypeSpec)]
struct StateWithGeneric {
    a: Vec<ValueA>,
    b: AvlTreeMap<KeyB, ValueB>,
    c: [DataTypeC; 4],
    de: WrapperD<InnerE>,
}
#[derive(CreateTypeSpec, ReadWriteState, ReadRPC)]
struct ValueA {}

#[derive(CreateTypeSpec)]
enum KeyB {
    #[discriminant(0)]
    VariantF {},
    #[discriminant(1)]
    VariantG {},
}

#[derive(CreateTypeSpec, ReadWriteState)]
struct ValueB {}

#[derive(CreateTypeSpec, ReadWriteState, ReadRPC)]
struct DataTypeC {}

#[derive(CreateTypeSpec)]
struct WrapperD<T> {
    value: T,
}

#[derive(CreateTypeSpec)]
struct InnerE {}

/// When generating a [`ContractAbi`] from a state struct where the fields have types with generic
/// arguments, the types are visited depth first, starting from the left most type. For map types
/// the Key type is visited first, and the Value type second.
/// Given State {a: Vec<A>, b: Map<BKey, BValue>, c: [C; 4], de: D<E>}, where BKey is an Enum with
/// variants F and G, is  the types are visited in order [State, A, BKey, F, G, BValue, C, DE, E].
#[test]
fn generic_arguments_are_visited_depth_first() {
    let contract = unsafe { generate_contract_abi::<StateWithGeneric>(vec![]) };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec![
            "StateWithGeneric",
            "ValueA",
            "KeyB",
            "VariantF",
            "VariantG",
            "ValueB",
            "DataTypeC",
            "WrapperDInnerE",
            "InnerE",
        ],
    );
}

#[derive(CreateTypeSpec)]
struct StateWithDuplicateTypes {
    a: StructA,
    d: WrapperD<StructC>,
}

/// When a type has been visited once, it is not added again to the types list if encountered again.
/// The types index in the list is the index of the first appearance.
#[test]
fn types_are_only_added_once() {
    let contract = unsafe { generate_contract_abi::<StateWithDuplicateTypes>(vec![]) };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec![
            "StateWithDuplicateTypes",
            "StructA",
            "StructC",
            "WrapperDStructC",
        ],
    );
}

#[derive(CreateTypeSpec, ReadWriteState)]
struct EmptyState {}

#[action(zk = true, shortname = 0x01)]
fn action_a(
    _context: ContractContext,
    state: EmptyState,
    _zk_state: ZkState<u32>,
    _x: u32,
    _a: StructA,
    _b: StructB,
) -> EmptyState {
    state
}

/// When generating a [`ContractAbi`] with a list of FnAbi generating functions, the named types in
/// the function arguments are visited depth-first starting from the left-most argument.
/// Given function with arguments [a: A, b: B] where A is {c: C}, the types are visited in order
/// [A, C, B].
#[test]
fn function_arguments_are_visited_depth_first() {
    let contract = unsafe { generate_contract_abi::<EmptyState>(vec![__abi_fn_2_01_action_a]) };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec!["EmptyState", "StructA", "StructC", "StructB"],
    );
}

#[derive(CreateTypeSpec, ReadWriteState)]
struct StateWithArgumentReuse {
    a: ValueA,
    b: ValueB,
}

#[action(zk = true, shortname = 0x02)]
fn action_with_argument_type_reuse(
    _context: ContractContext,
    state: StateWithArgumentReuse,
    _zk_state: ZkState<u32>,
    _c: Vec<DataTypeC>,
    _a: ValueA,
) -> StateWithArgumentReuse {
    state
}

/// When generating [`ContractAbi`] for a function that reuses some of the types in the state type
/// tree, they are not added again to the types list. Types that are unique to that function are
/// added after the state types. Given state with types {a: A, b: B} and a function with arguments
/// [c: C, a: A] the types are ordered as [A, B, C].
#[test]
fn when_functions_reuse_state_types_in_arguments_they_are_not_added_again() {
    let contract = unsafe {
        generate_contract_abi::<StateWithArgumentReuse>(vec![
            __abi_fn_2_02_action_with_argument_type_reuse,
        ])
    };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec!["StateWithArgumentReuse", "ValueA", "ValueB", "DataTypeC"],
    );
}

#[derive(CreateTypeSpec, ReadRPC)]
struct ActionRpcA {}

#[derive(CreateTypeSpec, ReadRPC)]
struct ActionRpcB {}

#[derive(CreateTypeSpec, ReadRPC)]
enum ActionRpcC {
    #[discriminant(0)]
    FirstVariant { a: ActionRpcA },
}

#[action(zk = true, shortname = 0x03)]
fn action_a_with_arg_reuse(
    _context: ContractContext,
    state: EmptyState,
    _zk_state: ZkState<u32>,
    _a: ActionRpcA,
    _b: ActionRpcB,
    _c: ActionRpcC,
) -> EmptyState {
    state
}

#[derive(CreateTypeSpec, ReadRPC)]
struct ActionRpcD {}

#[action(zk = true, shortname = 0x04)]
fn action_b_with_arg_reuse(
    _context: ContractContext,
    state: EmptyState,
    _zk_state: ZkState<u32>,
    _d: ActionRpcD,
    _b: ActionRpcB,
) -> EmptyState {
    state
}

/// When generating [`ContractAbi`] for multiple functions that share one of their argument types,
/// that type is only added once, and is in the index for the first time it appears.
/// Given functions A with arguments [a: A, b: B, c: C] and function B with arguments [d: D, b: B]
/// the types are visited in order [A, B, C, D].
#[test]
fn when_functions_reuse_arguments_from_previous_functions_they_are_not_added_again() {
    let contract = unsafe {
        generate_contract_abi::<EmptyState>(vec![
            __abi_fn_2_03_action_a_with_arg_reuse,
            __abi_fn_2_04_action_b_with_arg_reuse,
        ])
    };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec![
            "EmptyState",
            "ActionRpcA",
            "ActionRpcB",
            "ActionRpcC",
            "FirstVariant",
            "ActionRpcD",
        ],
    );
}

const __PBC_IS_ZK_CONTRACT: bool = true;

#[derive(SecretBinary, CreateTypeSpec)]
struct SbiPair {
    a: Sbi32,
    b: Sbi32,
}

#[zk_on_secret_input(shortname = 0x05, secret_type = "SbiPair")]
fn secret_input(
    _contract_context: ContractContext,
    state: EmptyState,
    _zk_state: ZkState<u32>,
    _x: String,
    _y: StructA,
) -> (EmptyState, Vec<EventGroup>, ZkInputDef<u32, SbiPair>) {
    let shortname = None;
    let def = ZkInputDef::with_metadata(shortname, 0u32);
    (state, vec![], def)
}

/// When generating [`ContractAbi`] for a function with a secret argument, the type of the secret
/// argument is added after the other arguments for that function.
#[test]
fn secret_argument_types_are_added_after_regular_argument_types() {
    let contract = unsafe {
        generate_contract_abi::<EmptyState>(vec![
            __abi_fn_2_04_action_b_with_arg_reuse,
            __abi_fn_23_05_secret_input,
            __abi_fn_2_03_action_a_with_arg_reuse,
        ])
    };
    let visited_types = contract.types;

    assert_type_names(
        visited_types,
        vec![
            "EmptyState",
            "ActionRpcD",
            "ActionRpcB",
            "StructA",
            "StructC",
            "SbiPair",
            "ActionRpcA",
            "ActionRpcC",
            "FirstVariant",
        ],
    );
}

/// When generating a [`ContractAbi`] the order of the generated [`FnAbi`]s is the same as the order
/// of the argument list of generating functions.
#[test]
fn generated_fn_abi_preserve_order_of_their_generating_functions() {
    let contract = unsafe {
        generate_contract_abi::<EmptyState>(vec![
            __abi_fn_2_04_action_b_with_arg_reuse,
            __abi_fn_23_05_secret_input,
            __abi_fn_2_03_action_a_with_arg_reuse,
        ])
    };

    let shortnames: Vec<&Vec<u8>> = contract
        .actions
        .iter()
        .map(|fn_abi| fn_abi.shortname())
        .collect();
    assert_eq!(shortnames.first().unwrap(), &&vec![0x04u8]);
    assert_eq!(shortnames.get(1).unwrap(), &&vec![0x05u8]);
    assert_eq!(shortnames.get(2).unwrap(), &&vec![0x03u8]);
}

fn assert_type_names(visited_types: Vec<NamedTypeSpec>, expected_names: Vec<&str>) {
    let actual_names: Vec<String> = visited_types.iter().map(|t| t.name.to_string()).collect();

    let actual_len = actual_names.len();
    let expected_len = expected_names.len();
    assert_eq!(
        actual_len, expected_len,
        "Expected `generate_contract` to have visited {} types in {:?} but was {}",
        expected_len, &actual_names, actual_len
    );

    for (idx, expected) in expected_names.iter().enumerate() {
        let actual = actual_names.get(idx).unwrap();
        assert_eq!(
            actual, expected,
            "Expected type {} in {:?} to be {} but was {}",
            idx, &actual_names, expected, actual
        );
    }
}
