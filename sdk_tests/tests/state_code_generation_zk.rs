//! Testing that zk contract generates version symbols
use pbc_contract_codegen::init;
use pbc_contract_codegen::state;
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::zk::ZkState;

#[state]
struct Something {
    num: u8,
}

#[init(zk = true)]
fn initialize(_ctx: ContractContext, _zk_state: ZkState<u8>) -> Something {
    Something { num: 0 }
}

#[test]
#[allow(clippy::unit_cmp)]
fn smoke_test_versions() {
    assert_eq!(__PBC_VERSION_BINDER_11_6_0, ());
    assert_eq!(__PBC_VERSION_CLIENT_5_6_0, ());
}
