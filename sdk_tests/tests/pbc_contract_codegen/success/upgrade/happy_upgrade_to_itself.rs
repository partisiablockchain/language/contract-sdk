/// Contract can define an upgrade to itself.
use pbc_contract_codegen::{init, state, upgrade};
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::address::Address;


#[state]
pub struct MyState {
    pub upgrader: Address,
}

#[init]
fn initialize(context: ContractContext) -> MyState {
    MyState { upgrader: context.sender }
}

#[upgrade]
fn upgrade(_context: ContractContext, prev_state: MyState) -> MyState {
    prev_state
}

/// Needed to avoid Rust compile errors.
pub fn main() {}
