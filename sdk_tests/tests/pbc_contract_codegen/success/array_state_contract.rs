use pbc_contract_codegen::{action, callback, init};

pub fn main() {}

#[init]
fn uno(_context: pbc_contract_common::context::ContractContext) -> [u16; 4] {
    [6, 3, 9, 2]
}

#[action(shortname = 0x01)]
fn dos(_context: pbc_contract_common::context::ContractContext, state: [u16; 4]) -> [u16; 4] {
    state
}

#[callback(shortname = 0x02)]
fn tres(
    _context: pbc_contract_common::context::ContractContext,
    _callback: pbc_contract_common::context::CallbackContext,
    state: [u16; 4],
) -> [u16; 4] {
    state
}

