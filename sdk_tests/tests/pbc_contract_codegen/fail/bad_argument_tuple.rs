use pbc_contract_codegen::{action, init};

pub fn main() {}

type MyState = Vec<u16>;

#[init]
fn uno(_context: pbc_contract_common::context::ContractContext) -> MyState {
    vec![6, 3, 9, 2]
}

#[action(shortname = 0x01)]
fn dos(
    _context: pbc_contract_common::context::ContractContext,
    _state: MyState,
    tup: (u16, u16),
) -> MyState {
    vec![tup.0, tup.1]
}
