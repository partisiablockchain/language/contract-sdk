use pbc_contract_codegen::{action, init};

pub fn main() {}

type MyState = u64;

#[init]
fn uno(_context: pbc_contract_common::context::ContractContext) -> MyState {
    0
}

#[action(shortname = 0x01)]
fn dos(
    _context: pbc_contract_common::context::ContractContext,
    _state: MyState,
    arg: *const u64,
) -> MyState {
    let res = unsafe { *arg };
    res
}
