/// Contracts must not have extranious arguments for [`upgrade`].
use pbc_contract_codegen::{init, state, upgrade};
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::address::Address;
use create_type_spec_derive::CreateTypeSpec;
use read_write_state_derive::ReadWriteState;


#[derive(CreateTypeSpec, ReadWriteState)]
pub struct MyStateV1 {
    pub upgrader: Address,
}

#[state]
pub struct MyStateV2 {
    pub upgrader: Address,
    pub counter: u32,
}

#[init]
fn initialize(context: ContractContext) -> MyStateV1 {
    MyStateV1 { upgrader: context.sender }
}

#[upgrade]
fn upgrade(_context: ContractContext, prev_state: MyStateV1, some_arg:u32) -> MyStateV2 {
    MyStateV2 {
        upgrader: prev_state.upgrader,
        counter: some_arg,
    }
}

/// Needed to avoid Rust compile errors.
pub fn main() {}
