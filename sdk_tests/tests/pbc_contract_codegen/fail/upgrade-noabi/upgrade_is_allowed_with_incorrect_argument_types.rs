/// Contract must not have an [`upgrade_is_allowed`] invocation with extranious arguments.
use pbc_contract_codegen::{init, state, upgrade_is_allowed};
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::upgrade::ContractHashes;
use pbc_contract_common::address::Address;

#[state]
pub struct MyState {
    pub upgrader: Address,
}

#[init]
fn initialize(context: ContractContext) -> MyState {
    MyState { upgrader: context.sender }
}

#[upgrade_is_allowed]
fn upgrade_is_allowed(
    context: ContractContext,
    state: MyState,
    _current_hashes: ContractHashes,
    _upgrade_to_hashes: ContractHashes,
    _incorrect_rpc_type: u32,
) -> bool {
    state.upgrader == context.sender
}

/// Needed to avoid Rust compile errors.
pub fn main() {}
