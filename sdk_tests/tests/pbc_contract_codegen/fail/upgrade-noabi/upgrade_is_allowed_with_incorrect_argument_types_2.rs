/// `contract_is_allowed` must have types `(ContractContext, MyState, ContractHashes, ContractHashes, Vec<u8>)`.
use pbc_contract_codegen::{init, state, upgrade_is_allowed};
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::address::Address;

#[state]
pub struct MyState {
    pub upgrader: Address,
}

#[init]
fn initialize(context: ContractContext) -> MyState {
    MyState { upgrader: context.sender }
}

#[upgrade_is_allowed]
fn upgrade_is_allowed(
    context: ContractContext,
    state: MyState,
    _current_hashes: u32,
    _upgrade_to_hashes: u32,
    _incorrect_rpc_type: Vec<u8>,
) -> bool {
    state.upgrader == context.sender
}

/// Needed to avoid Rust compile errors.
pub fn main() {}
