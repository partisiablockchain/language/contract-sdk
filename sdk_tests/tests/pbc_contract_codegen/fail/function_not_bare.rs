use pbc_contract_codegen::{action, init};
use read_write_state_derive::ReadWriteState;

pub fn main() {}

#[derive(ReadWriteState)]
struct MyState {
    nums: Vec<u16>,
}

impl MyState {
    #[action(shortname = 0x01)]
    fn g(&self, _context: pbc_contract_common::context::ContractContext, state: MyState) -> MyState {
        state
    }
}

#[init]
fn f(_context: pbc_contract_common::context::ContractContext) -> MyState {
    MyState {
        nums: vec![6, 3, 9, 2]
    }
}