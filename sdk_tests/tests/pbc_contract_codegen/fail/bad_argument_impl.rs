use pbc_contract_codegen::{action, init};

pub fn main() {}

trait MyTrait {}

struct MyStruct {}

impl MyTrait for MyStruct {}

type MyState = u64;

#[init]
fn uno(_context: pbc_contract_common::context::ContractContext) -> MyState {
    0
}

#[action(shortname = 0x01)]
fn dos(
    _context: pbc_contract_common::context::ContractContext,
    state: MyState,
    _arg: impl MyTrait,
) -> MyState {
    state
}
