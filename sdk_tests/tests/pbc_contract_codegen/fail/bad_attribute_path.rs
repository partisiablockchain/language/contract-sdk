use pbc_contract_codegen::action;

pub fn main() {}

type ContractState = u64;

#[action(pbc_contract_common::address::shortname = 0x10)]
pub fn literal(
    _context: pbc_contract_common::context::ContractContext,
    state: ContractState,
) -> ContractState {
    state
}
