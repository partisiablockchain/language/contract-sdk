/// Contracts must not have double [`upgrade`] invocations.
use pbc_contract_codegen::{init, state, upgrade};
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::address::Address;
use create_type_spec_derive::CreateTypeSpec;
use read_write_state_derive::ReadWriteState;


#[derive(CreateTypeSpec, ReadWriteState)]
pub struct MyStateV1 {
    pub upgrader: Address,
}

#[state]
pub struct MyStateV2 {
    pub upgrader: Address,
    pub counter: u32,
}

#[init]
fn initialize(context: ContractContext) -> MyStateV1 {
    MyStateV1 { upgrader: context.sender }
}

#[upgrade]
fn upgrade_v1(_context: ContractContext, prev_state: MyStateV2) -> MyStateV2 {
    prev_state
}

#[upgrade]
fn upgrade_v2(_context: ContractContext, prev_state: MyStateV1) -> MyStateV2 {
    MyStateV2 {
        upgrader: prev_state.upgrader,
        counter: 42,
    }
}

/// Needed to avoid Rust compile errors.
pub fn main() {}
