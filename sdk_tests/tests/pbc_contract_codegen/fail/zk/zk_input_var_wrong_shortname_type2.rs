use pbc_contract_codegen::init;
use read_write_state_derive::ReadWriteState;
use pbc_zk::{SecretBinary, SecretBinaryFixedSize};
use pbc_contract_common::events::EventGroup;
use pbc_contract_common::zk::ZkInputDef;
use pbc_contract_common::address::ShortnameCallback;
use pbc_contract_codegen::state;

pub fn main() {}

#[state]
struct ContractState {
    own_variable_id: u64,
}

#[derive(ReadWriteState, PartialEq)]
struct SecretVarMetadata {
    value: u64,
}

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> u64 {
    0
}

fn secret_input<T: SecretBinary + SecretBinaryFixedSize>(
    state: ContractState,
) -> (
    ContractState,
    Vec<EventGroup>,
    ZkInputDef<SecretVarMetadata, T>,
) {
    let shortname = Some(ShortnameCallback::from_u32(1));
    let input_def = ZkInputDef::with_metadata(
        shortname,
        SecretVarMetadata { value: 77 },
    );
    (state, vec![], input_def)
}
