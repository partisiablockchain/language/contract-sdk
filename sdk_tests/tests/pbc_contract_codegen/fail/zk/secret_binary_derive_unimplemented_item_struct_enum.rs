use pbc_zk::SecretBinary;

pub fn main() {}

#[derive(SecretBinary)]
enum MyOption {
    #[discriminant(0)]
    Some { value: u16 },
    #[discriminant(1)]
    None {},
}