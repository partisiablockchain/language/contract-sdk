use pbc_contract_codegen::init;
use pbc_contract_codegen::state;
use pbc_contract_common::shortname::ShortnameZkComputation;
use pbc_contract_common::events::EventGroup;
use pbc_contract_common::zk::ZkStateChange;
use pbc_contract_common::address::ShortnameCallback;
use read_write_state_derive::ReadWriteState;

pub fn main() {}

#[state]
struct ContractState {
    own_variable_id: u64,
}

#[derive(ReadWriteState, PartialEq)]
struct SecretVarMetadata {
    value: u64,
}

const SHORTNAME_ZK_COMPUTE: ShortnameZkComputation = ShortnameZkComputation::from_u32(0);

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
    let shortname = Some(ShortnameCallback::from_u32(1));
    (ContractState { own_variable_id: 0 }, vec![], vec![ZkStateChange::start_computation::<SecretVarMetadata>(
        SHORTNAME_ZK_COMPUTE,
        vec![SecretVarMetadata { value: 0 }],
        shortname,
    )])
}
