use pbc_contract_codegen::zk_on_variable_inputted;
use pbc_contract_codegen::init;

pub fn main() {}

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> u64 {
    0
}

#[zk_on_variable_inputted]
pub fn zk_on_variable_inputted(
    _context: ContractContext,
    state: ContractState,
    _zk_state: ZkState<Metadata>,
    _variable_id: SecretVarId,
) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
    (state, vec![], vec![])
}
