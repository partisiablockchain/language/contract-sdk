use pbc_contract_codegen::init;
use pbc_contract_codegen::zk_on_external_event;

type ContractState = u64;
type Metadata = u32;

pub fn main() {}

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> ContractState   {
    0
}

#[zk_on_external_event(shortname = 0x66)]
pub fn zk_on_external_event(
    _context: ContractContext,
    state: ContractState,
    _zk_state: ZkState<Metadata>,
    _subscription_id: EventSubscriptionId,
    _event_id: ExternalEventId,
) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
    (state, vec![], vec![])
}