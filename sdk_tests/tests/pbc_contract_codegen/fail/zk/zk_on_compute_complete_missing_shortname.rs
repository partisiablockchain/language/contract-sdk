use pbc_contract_codegen::zk_on_compute_complete;
use pbc_contract_codegen::init;

pub fn main() {}

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> u64 {
    0
}

#[zk_on_compute_complete]
pub fn zk_on_compute_complete(
    _context: ContractContext,
    state: ContractState,
    _zk_state: ZkState<Metadata>,
    created_variables: Vec<SecretVarId>,
) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
    (state, vec![], vec![ZkStateChange::OpenVariables { variables: created_variables }])
}
