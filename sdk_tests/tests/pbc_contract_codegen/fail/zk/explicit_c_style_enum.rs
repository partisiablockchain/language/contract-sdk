use pbc_zk::SecretBinary;

pub fn main() {}

#[derive(SecretBinary)]
enum SecretColor {
    Red = 0xff0000,
    Green = 0x00ff00,
    Blue = 0x0000ff,
}
