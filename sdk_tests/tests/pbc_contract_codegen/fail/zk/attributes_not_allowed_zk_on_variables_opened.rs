use pbc_contract_codegen::init;
use pbc_contract_codegen::zk_on_variables_opened;

pub fn main() {}

type ContractState = u64;

#[init(zk = true)]
fn init(
    _context: pbc_contract_common::context::ContractContext,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
) -> u64   {
    0
}

#[zk_on_variables_opened(shortname=0x42)]
fn do_zk_on_variables_opened(
    _context: pbc_contract_common::context::ContractContext,
    state: u64,
    _zk_state: pbc_contract_common::zk::ZkState<u64>,
    _opened_variables: Vec<pbc_zk::SecretVarId>,
) -> u64 {
    state
}

