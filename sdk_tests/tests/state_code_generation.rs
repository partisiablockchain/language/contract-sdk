//! Testing that pub contract generates version symbols
use pbc_contract_codegen::init;
use pbc_contract_codegen::state;
use pbc_contract_common::context::ContractContext;

#[state]
struct Something {
    num: u8,
}

#[init]
fn initialize(_ctx: ContractContext) -> Something {
    Something { num: 0 }
}

/// Test that specific symbols are exposed by the contract.
#[test]
#[allow(clippy::unit_cmp)]
pub fn smoke_test_versions() {
    assert_eq!(__PBC_VERSION_BINDER_10_5_0, ());
    assert_eq!(__PBC_VERSION_CLIENT_5_6_0, ());
}
