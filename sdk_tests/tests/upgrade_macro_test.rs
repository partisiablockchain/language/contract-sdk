//! Contract can have a [`upgrade`] invocation.
use create_type_spec_derive::CreateTypeSpec;
use pbc_contract_codegen::{init, upgrade, upgrade_is_allowed};
use pbc_contract_common::address::Address;
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::upgrade::ContractHashes;
use read_write_state_derive::ReadWriteState;

/// Struct for testing contract upgrading.
#[derive(CreateTypeSpec, ReadWriteState)]
pub struct MyStateV1 {
    /// User with permission to upgrade contract.
    pub upgrader: Address,
}

/// Initialize contract
#[init]
pub fn initialize(context: ContractContext) -> MyStateV1 {
    MyStateV1 {
        upgrader: context.sender,
    }
}

/// Upgrade contract from it's own state.
#[upgrade]
pub fn upgrade(_context: ContractContext, prev_state: MyStateV1) -> MyStateV1 {
    prev_state
}

/// Check that we trust the upgrader.
#[upgrade_is_allowed]
pub fn upgrade_is_allowed(
    context: ContractContext,
    state: MyStateV1,
    _current_hashes: ContractHashes,
    _upgrade_to_hashes: ContractHashes,
    _rpc_to_upgrade: Vec<u8>,
) -> bool {
    state.upgrader == context.sender
}
