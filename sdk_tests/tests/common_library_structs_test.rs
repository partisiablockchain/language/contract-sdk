//! Testing some common functionality of the sdk define blockchain types

use create_type_spec_derive::CreateTypeSpec;
use pbc_abi::CreateTypeSpec;
use pbc_contract_common::abi::abi_to_ptr;
use pbc_data_types::address::{Address, AddressType};
use pbc_data_types::signature::Signature;
use pbc_data_types::{BlsPublicKey, BlsSignature, Hash, PublicKey, U256};
use read_write_state_derive::ReadWriteState;
use std::cmp::Ordering;

#[derive(Eq, PartialEq, Debug, Clone, PartialOrd, Ord, CreateTypeSpec, ReadWriteState)]
struct CommonFunctionalityState {
    address: Address,
    signature: Signature,
    hash: Hash,
    public_key: PublicKey,
    bls_public_key: BlsPublicKey,
    bls_signature: BlsSignature,
    u256: U256,
}

const EXAMPLE_COMMON_1: CommonFunctionalityState = CommonFunctionalityState {
    address: Address {
        address_type: AddressType::Account,
        identifier: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        ],
    },
    signature: Signature {
        recovery_id: 0,
        value_r: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
        value_s: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
    hash: Hash {
        bytes: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
    public_key: PublicKey {
        bytes: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 32,
        ],
    },
    bls_public_key: BlsPublicKey {
        bytes: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31,
        ],
    },
    bls_signature: BlsSignature {
        bytes: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
        ],
    },
    u256: U256 {
        bytes: [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
};

const EXAMPLE_COMMON_2: CommonFunctionalityState = CommonFunctionalityState {
    address: Address {
        address_type: AddressType::Account,
        identifier: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        ],
    },
    signature: Signature {
        recovery_id: 0,
        value_r: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
        value_s: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
    hash: Hash {
        bytes: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
    public_key: PublicKey {
        bytes: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 32,
        ],
    },
    bls_public_key: BlsPublicKey {
        bytes: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31,
        ],
    },
    bls_signature: BlsSignature {
        bytes: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
        ],
    },
    u256: U256 {
        bytes: [
            255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31,
        ],
    },
};

#[test]
fn clone_common_functionality() {
    let example = EXAMPLE_COMMON_1;
    let example_clone = example.clone();

    assert_eq!(example, example_clone);
}

#[test]
fn debug_common_functionality() {
    assert_eq!(format!("{:?}", EXAMPLE_COMMON_1),
               "CommonFunctionalityState { \
                    address: Address { \
                        address_type: Account, \
                        identifier: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19] \
                    }, \
                    signature: Signature { \
                        recovery_id: 0, \
                        value_r: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31], \
                        value_s: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] \
                    }, \
                    hash: 000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F, \
                    public_key: PublicKey { \
                        bytes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32] \
                    }, \
                    bls_public_key: BlsPublicKey { \
                        bytes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] \
                    }, \
                    bls_signature: BlsSignature { \
                        bytes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] \
                    }, \
                    u256: U256 { \
                        bytes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] \
                    } \
               }");
}

#[test]
fn order_common_functionality() {
    assert_eq!(EXAMPLE_COMMON_1.cmp(&EXAMPLE_COMMON_2), Ordering::Less);
    assert_eq!(EXAMPLE_COMMON_1.cmp(&EXAMPLE_COMMON_1), Ordering::Equal);
    assert_eq!(EXAMPLE_COMMON_2.cmp(&EXAMPLE_COMMON_1), Ordering::Greater);

    assert_eq!(
        EXAMPLE_COMMON_1.partial_cmp(&EXAMPLE_COMMON_2),
        Some(Ordering::Less)
    );
    assert_eq!(
        EXAMPLE_COMMON_1.partial_cmp(&EXAMPLE_COMMON_1),
        Some(Ordering::Equal)
    );
    assert_eq!(
        EXAMPLE_COMMON_2.partial_cmp(&EXAMPLE_COMMON_1),
        Some(Ordering::Greater)
    );
}

#[test]
fn type_spec() {
    assert_eq!(
        CommonFunctionalityState::__ty_name(),
        "CommonFunctionalityState"
    );
}

#[test]
fn abi_to_ptr_test() {
    let example_1_clone = EXAMPLE_COMMON_1.clone();
    let mut bytes = [0u8; std::mem::size_of::<CommonFunctionalityState>()];
    let byte_ptr = bytes.as_mut_ptr();

    // Write to pointer;
    abi_to_ptr(example_1_clone, byte_ptr);

    let expected_bytes = [
        0u8, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 0, 1, 2, 3,
        4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
        28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
        14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5,
        6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
        30, 31, 32, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
        22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7,
        8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
        24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1,
        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
        27, 28, 29, 30, 31,
    ];

    assert_eq!(bytes, expected_bytes);
}
