//! Test stability of [`TypeWitness`]es.
use pbc_contract_common::upgrade::TypeWitness;

use sdk_tests::state_types::*;

/// Different types produce different type witnesses.
///
/// This test should never change.
macro_rules! type_witnesses_parameterized_tests {
    ($($test_name:ident, $type:ty, $expected_hash:literal)*) => {
        $(
            #[test]
            fn $test_name() {
                type T = $type;
                let type_witness = TypeWitness::of::<T>();
                let expected_hash = $expected_hash;
                assert_eq!(format!("{}", type_witness.hash), expected_hash);
            }
        )*
    }
}

// Result values should not be changing.
type_witnesses_parameterized_tests!(
    primitive_u8, u8, "4BF5122F344554C53BDE2EBB8CD2B7E3D1600AD631C385A5D7CCE23C7785459A"
    primitive_u16, u16, "DBC1B4C900FFE48D575B5DA5C638040125F65DB0FE3E24494B76EA986457D986"
    primitive_u32, u32, "084FED08B978AF4D7D196A7446A86B58009E636B611DB16211B65A9AADFF29C5"
    primitive_u64, u64, "E52D9C508C502347344D8C07AD91CBD6068AFC75FF6292F062A09CA381C89E71"
    primitive_u128, u128, "E77B9A9AE9E30B0DBDB6F510A264EF9DE781501D7B6B92AE89EB059C5AB743DB"
    primitive_i8, i8, "67586E98FAD27DA0B9968BC039A1EF34C939B9B8E523A8BEF89D478608C5ECF6"
    primitive_i16, i16, "CA358758F6D27E6CF45272937977A748FD88391DB679CEDA7DC7BF1F005EE879"
    primitive_i32, i32, "BEEAD77994CF573341EC17B58BBF7EB34D2711C993C1D976B128B3188DC1829A"
    primitive_i64, i64, "2B4C342F5433EBE591A1DA77E013D1B72475562D48578DCA8B84BAC6651C3CB9"
    primitive_i128, i128, "01BA4719C80B6FE911B091A7C05124B64EEECE964E09C058EF8F9805DACA546B"
);

// Result values should not be changing.
type_witnesses_parameterized_tests!(
    built_in_string, String, "E7CF46A078FED4FAFD0B5E3AFF144802B853F8AE459A4F0C14ADD3314B7CC3A6"
    built_in_vec_u8, Vec<u8>, "7EDDC22B5FB2E3EDCB7FBD4F6556CE1FF8CEE856B56D4B7EED51DC3C8BDA6DA0"
);

// type witnesses are indifferent to type struct names but not field names
type_witnesses_parameterized_tests!(
    struct_point, Point, "4373F6B366ED9DA8A3C6A6F6C713F22BD6665CC06D8D1D59E98AEAE015ED1850"
    struct_pointflippedorder, PointFlippedOrder, "68849D902489F5F0921DCFA9F2224315B0E682556A355238DDE098B7ED095542"
    struct_pointbyanyothername, PointByAnyOtherName, "4373F6B366ED9DA8A3C6A6F6C713F22BD6665CC06D8D1D59E98AEAE015ED1850"
);

// Different types produce different type witnesses.
type_witnesses_parameterized_tests!(
    struct_deriveabiforme, DeriveAbiForMe, "259CF79831D2EF88CC9000663776182B5320AB17E5DE2914ABC674BE1D89F579"
    struct_nested, Nested, "396454887A9FD67A138B08C3D130AE7A8512DFCD1E4982BA290C381CF9FA4C2D"
    struct_inner, Inner, "CE167B4B2A253092285A18506A1F2ED28ACB1E4AE4716820AA24B29366776A92"
    struct_outer, Outer, "C9CE8BE508396FE2FF30F41831A4D5154F2937AC9533A0490721B9CA9E2E2BEA"
    struct_outermapkey, OuterMapKey, "416C4D1FDEE92002C465D36413EA43D22636E6CEBDA5067A5FA9C952AAEA7BAC"
    struct_idmapstate, IdMapState, "F4223B1D4F19EDD78742FD7902929E3F5939244AD9BA7B6BBAC5F7EF0731CF18"
    struct_outermapvalue, OuterMapValue, "A5371DA8E937652CF8415B85E204C6263100CF45A0E781C91B7B35093A0688D7"
    struct_outervec, OuterVec, "FEDCE0ADB1C70D2E0DF5F48997FC576F4CA3226DCD619D005B7440A1750166F2"
    struct_outerbtreeset, OuterBTreeSet, "62774D7A3BA2F75049FF1AC32D8748A2BA99DABF2BA8BA3B825D8DA198524CEE"
    struct_outercomposite, OuterComposite, "99D2C307CA1C54EFA6DEE27F4509FC572D07AAD9B3AE75993C9CDC74D3A36190"
    struct_witharray, WithArray, "B1681F082F4FDD35510B890A96901FB5A9A84410A19578F789BA8B694D5AD5FE"
    struct_miniaccess, MiniAccess, "9CBC73D18D70C94FE366E696035C4F2CFFDBAB7EA6D6C2C039CA185F9C9F2746"
    struct_statewithaccesscontrol, StateWithAccessControl, "C60A7E9C6DC3F0CECA35F29A699D9150CDE3C012E1EDFB388D9C5E9B2F53767F"
);

// [`TypeWitness`] generation ignores enum variant names, but not variant discriminants.
type_witnesses_parameterized_tests!(
    enum_enumitemstruct, EnumItemStruct, "D8F1FD14D98BE90017B4B302B12DC3CA682C67AA60151F41951B318249B5614F"
    enum_nestedenum, NestedEnum, "209E0B2EFF8D7C4B599581D6BACB8BB36EE7B653DEFED74C8AB06FE0F4060E2D"
    enum_enumone, EnumOne, "219B39AB70D66990B5367AB7998077F9303BC1AE971756E1ECB0CBD046491F21"
    enum_enum2, Enum2, "219B39AB70D66990B5367AB7998077F9303BC1AE971756E1ECB0CBD046491F21"
    enum_enum2withdifferentdiscriminants, Enum2WithDifferentDiscriminants, "ABA68DF4BE7676AF5A2407A1F315BCB7E6F1DFB62EA1AC0989EB146C5F30040F"
);
