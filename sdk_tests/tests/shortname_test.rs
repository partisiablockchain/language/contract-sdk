//! Testing the different Shortname implementations
use pbc_contract_common::address::{ShortnameZkComputeComplete, ShortnameZkVariableInputted};
use pbc_data_types::shortname::{Shortname, ShortnameCallback, ShortnameZkComputation};

#[test]
fn clone_shortname() {
    let shortname: Shortname = Shortname::from_u32(3);
    assert_eq!(shortname.clone(), shortname);
}

#[test]
fn clone_shortnamecallback() {
    let shortname: ShortnameCallback = ShortnameCallback::from_u32(3);
    assert_eq!(shortname.clone(), shortname);
}

#[test]
fn clone_shortnamezkcomputation() {
    let shortname: ShortnameZkComputation = ShortnameZkComputation::from_u32(3);
    assert_eq!(shortname.clone(), shortname);
}

#[test]
fn clone_shortnamezkvariableinputted() {
    let shortname: ShortnameZkVariableInputted = ShortnameZkVariableInputted::from_u32(3);
    assert_eq!(shortname.clone(), shortname);
}

#[test]
fn clone_shortnamezkcomputecomplete() {
    let shortname: ShortnameZkComputeComplete = ShortnameZkComputeComplete::from_u32(3);
    assert_eq!(shortname.clone(), shortname);
}

/// Can create a shortname from raw bytes with no validation check
#[test]
pub fn from_bytes_raw() {
    let shortname = Shortname::from_bytes_raw([1, 0, 0, 0, 0]);
    assert_eq!(shortname.as_u32(), 1);
    let shortname_callback = ShortnameCallback::from_bytes_raw([2, 0, 0, 0, 0]);
    assert_eq!(shortname_callback.shortname.as_u32(), 2);
    let shortname_zk_compute = ShortnameZkComputation::from_bytes_raw([3, 0, 0, 0, 0]);
    assert_eq!(shortname_zk_compute.shortname.as_u32(), 3);
    let shortname_zk_compute_complete = ShortnameZkComputeComplete::from_bytes_raw([4, 0, 0, 0, 0]);
    assert_eq!(shortname_zk_compute_complete.shortname.as_u32(), 4);
    let shortname_zk_var_inputted = ShortnameZkVariableInputted::from_bytes_raw([5, 0, 0, 0, 0]);
    assert_eq!(shortname_zk_var_inputted.shortname.as_u32(), 5);
}

#[test]
fn debug_shortnamecallback() {
    let shortname: ShortnameCallback = ShortnameCallback::new(Shortname::from_u32(3));
    assert_eq!(
        format!("{:?}", shortname),
        "ShortnameCallback { shortname: Shortname { value: [3, 0, 0, 0, 0] } }"
    );
}

#[test]
fn debug_shortnamezkcomputation() {
    let shortname: ShortnameZkComputation = ShortnameZkComputation::new(Shortname::from_u32(3));
    assert_eq!(
        format!("{:?}", shortname),
        "ShortnameZkComputation { shortname: Shortname { value: [3, 0, 0, 0, 0] } }"
    );
}

#[test]
fn debug_shortnamezkvariableinputted() {
    let shortname: ShortnameZkVariableInputted =
        ShortnameZkVariableInputted::new(Shortname::from_u32(3));
    assert_eq!(
        format!("{:?}", shortname),
        "ShortnameZkVariableInputted { shortname: Shortname { value: [3, 0, 0, 0, 0] } }"
    );
}

#[test]
fn debug_shortnamezkcomputecomplete() {
    let shortname: ShortnameZkComputeComplete =
        ShortnameZkComputeComplete::new(Shortname::from_u32(3));
    assert_eq!(
        format!("{:?}", shortname),
        "ShortnameZkComputeComplete { shortname: Shortname { value: [3, 0, 0, 0, 0] } }"
    );
}

#[test]
fn empty_shortname() {
    let shortname = Shortname::from_be_bytes(&[]);
    assert_eq!(shortname, Err("Shortname must not be empty".to_string()));
}

#[test]
fn continuation_on_last_byte() {
    let shortname = Shortname::from_be_bytes(&[0x80, 0x81]);
    assert_eq!(
        shortname,
        Err("Shortname's last byte must not have continuation bit set".to_string())
    );
}

#[test]
fn normalized_shortname() {
    let shortname = Shortname::from_be_bytes(&[0x70, 0, 0, 0]);
    assert_eq!(
        shortname,
        Err("Shortname must be normalized, with no trailing zeroes".to_string())
    );
}

#[test]
fn no_continuation_on_non_last_bytes() {
    let shortname = Shortname::from_be_bytes(&[0x70, 0x01]);
    assert_eq!(
        shortname,
        Err("Shortname's non-last bytes must have their continuation bits set".to_string())
    );
}

#[test]
fn too_large() {
    let shortname = Shortname::from_be_bytes(&[0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01]);
    assert_eq!(
        shortname,
        Err("Shortname value too large for u32".to_string())
    );
}

/// Can't create a shortname if the leb value is too large for an u32
#[test]
pub fn too_large_2() {
    let shortname = Shortname::from_be_bytes(&[0xFF, 0xFF, 0xFF, 0xFF, 0x7F]);
    assert_eq!(
        shortname,
        Err("Shortname value too large for u32".to_string())
    );
}

fn interesting_shortname_values() -> Vec<(u32, Vec<u8>)> {
    vec![
        (0, vec![0x00]),
        (1, vec![0x01]),
        (127, vec![0x7F]),
        (128, vec![0x80, 0x01]),
        (256, vec![0x80, 0x02]),
        (1000, vec![0xe8, 0x07]),
        (586977299, vec![0x93, 0xA0, 0xF2, 0x97, 0x02]),
    ]
}

#[test]
fn shortname_as_u32() {
    for (i, shortname_bytes) in interesting_shortname_values() {
        let shortname = Shortname::from_be_bytes(&shortname_bytes).unwrap();
        assert_eq!(Shortname::from_u32(i), shortname);
        assert_eq!(shortname.as_u32(), i);
    }
}

#[test]
fn u32_as_shortname_as_u32() {
    for (i, _) in interesting_shortname_values() {
        let shortname = Shortname::from_u32(i);
        assert_eq!(i, shortname.as_u32());
    }
}

#[test]
fn u32_as_shortname_bytes_as_u32() {
    for (shortname_value, shortname_bytes) in interesting_shortname_values() {
        let parsed = Shortname::from_be_bytes(&shortname_bytes).unwrap();
        assert_eq!(shortname_bytes, parsed.bytes());
        assert_eq!(shortname_value, parsed.as_u32());
    }
}

#[test]
fn invalid_shortnames() {
    let invalid_shortname_bytes = [
        vec![0x00, 0x00],
        vec![0x00, 0x01],
        vec![0x00, 0x7F],
        vec![0x00, 0x80, 0x01],
        vec![0x80, 0x02, 0x00],
        vec![0x80],
        vec![0x80, 0x00], // Technically valid LEB128, but not normalized
        vec![0x80, 0x80, 0x80, 0x80, 0x32], // Too large for u32
        vec![0x93, 0xA0, 0xF2, 0x97, 0x32], // Too large for u32
    ];
    for bytes in invalid_shortname_bytes {
        let result = Shortname::from_be_bytes(&bytes);
        assert!(result.is_err());
    }
}
