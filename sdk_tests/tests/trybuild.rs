//! Testing building of contract macros

#[test]
fn read_write_macro_fail() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests/read-write-macro-fail/*.rs");
}

#[test]
fn create_type_spec_macro_fail() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests/create-type-spec-macro-fail/*.rs");
}

#[test]
fn trybuild_codegen_fail() {
    let t = trybuild::TestCases::new();
    #[cfg(feature = "abi")]
    t.compile_fail("tests/pbc_contract_codegen/fail/*.rs");
    #[cfg(feature = "abi")]
    t.compile_fail("tests/pbc_contract_codegen/fail/zk/*.rs");
    #[cfg(feature = "abi")]
    t.compile_fail("tests/pbc_contract_codegen/compile_but_fail_abi/*.rs");

    // Upgrade functionality
    t.compile_fail("tests/pbc_contract_codegen/fail/upgrade-noabi/*.rs");
}

#[test]
fn trybuild_codegen() {
    let t = trybuild::TestCases::new();
    t.pass("tests/pbc_contract_codegen/success/upgrade/*.rs");
    t.pass("tests/pbc_contract_codegen/success/*.rs");
    t.pass("tests/pbc_contract_codegen/success/zk/*.rs");
    #[cfg(not(feature = "abi"))]
    t.pass("tests/pbc_contract_codegen/compile_but_fail_abi/*.rs");
}
