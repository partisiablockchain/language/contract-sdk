//! Module for various example contract states. Used for testing.
#![allow(dead_code)]
use std::collections::BTreeSet;

use create_type_spec_derive::CreateTypeSpec;
use pbc_abi::CreateTypeSpec;
use pbc_contract_common::sorted_vec_map::SortedVecMap;
use read_write_state_derive::ReadWriteState;
use std::marker::PhantomData;

// Structs

/// Struct with several different types.
#[derive(CreateTypeSpec)]
pub struct DeriveAbiForMe {
    a: String,
    b: u64,
    c: u8,
    d: Vec<Vec<Vec<Vec<u8>>>>,
}

/// Struct containing another struct
#[derive(CreateTypeSpec)]
pub struct Nested {
    derived: DeriveAbiForMe,
}

/// Struct with a single value
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, CreateTypeSpec, ReadWriteState)]
pub struct Inner {
    x: u8,
}

/// Struct with two values with names "x" and "y".
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, CreateTypeSpec, ReadWriteState)]
pub struct Point {
    x: u8,
    y: u8,
}

/// Struct with two values with names "x" and "y".
///
/// Only difference from [`Point`] is the name.
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, CreateTypeSpec, ReadWriteState)]
pub struct PointByAnyOtherName {
    x: u8,
    y: u8,
}

/// Struct with two values with names "y" and "x".
///
/// Only difference from [`Point`] is the flipped order.
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, CreateTypeSpec, ReadWriteState)]
pub struct PointFlippedOrder {
    y: u8,
    x: u8,
}

/// CreateTypeSpec supports PhantomData in ABI.
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, CreateTypeSpec)]
pub struct Id<T> {
    raw: u32,
    type_t: PhantomData<T>,
}

/// Struct containing a different struct.
#[derive(Clone, CreateTypeSpec)]
pub struct Outer {
    inner: Inner,
}

/// Struct containing a map with a struct key
#[derive(Clone, CreateTypeSpec)]
pub struct OuterMapKey {
    inner: SortedVecMap<Inner, String>,
}

/// CreateTypeSpec supports PhantomData in ABI.
#[derive(Clone, CreateTypeSpec)]
pub struct IdMapState {
    map: SortedVecMap<Id<String>, String>,
}

/// Struct containing a map with a struct value
#[derive(Clone, CreateTypeSpec)]
pub struct OuterMapValue {
    inner: SortedVecMap<String, Inner>,
}

/// Struct containing a vec of structs.
#[derive(Clone, CreateTypeSpec)]
pub struct OuterVec {
    inner: Vec<Inner>,
}

/// Struct containing a set of structs.
#[derive(Clone, CreateTypeSpec)]
pub struct OuterBTreeSet {
    inner: BTreeSet<Inner>,
}

/// Struct containing a complex generic type.
#[derive(Clone, CreateTypeSpec)]
pub struct OuterComposite {
    inner: Vec<BTreeSet<Vec<SortedVecMap<Inner, String>>>>,
}

/// Struct containing a byte array with a fixed size.
#[derive(Clone, CreateTypeSpec)]
pub struct WithArray {
    inner: [u8; 50],
}

/// Generic enum with several variants.
#[derive(Clone, PartialOrd, Ord, PartialEq, Eq, CreateTypeSpec)]
pub enum MyRoleEnum<RankT: PartialOrd + PartialEq + Eq + Ord + Clone> {
    /// Variant 1
    #[discriminant(1)]
    Admin {},
    /// Variant 2
    #[discriminant(2)]
    Moderator {},
    /// Variant 3 using the generic type
    #[discriminant(3)]
    User {
        /// Generic type
        rank: RankT,
    },
}

/// Empty struct
#[derive(Clone, PartialOrd, Ord, PartialEq, Eq, CreateTypeSpec)]
pub struct MiniAccess {} // No access

/// Generic struct containing a mapping of the generic type.
#[derive(Clone, CreateTypeSpec)]
pub struct AccessControl<RoleT: Ord + Clone> {
    /// Generic mapping of the type.
    pub role_mappings: SortedVecMap<RoleT, RoleT>,
}

/// Struct containing a generic struct.
#[derive(Clone, CreateTypeSpec)]
pub struct StateWithAccessControl {
    /// Generic struct
    pub access_control: AccessControl<MyRoleEnum<u32>>,
}

/// Recursively defined generic struct.
#[derive(CreateTypeSpec)]
pub struct LinkedTree<T: CreateTypeSpec> {
    /// Generic value
    value: T,
    /// Recursive values in a vec
    children: Vec<LinkedTree<T>>,
}

/// Generic struct pair of values.
#[derive(CreateTypeSpec)]
pub struct Pair<V1, V2> {
    /// Generic value 1
    v1: V1,
    /// Generic value 2
    v2: V2,
}

/// Enum with generic values.
#[derive(CreateTypeSpec)]
pub enum MyOptionEnum<T: CreateTypeSpec> {
    /// Variant 1
    #[discriminant(0)]
    None {},
    /// Variant 2
    #[discriminant(1)]
    Some {
        /// Generic type
        value: T,
    },
    /// Variant 3
    #[discriminant(2)]
    More {
        /// Recursive generic tpye
        values: Vec<MyOptionEnum<T>>,
    },
}

/// Enum containing multiple non-empty variants.
#[derive(CreateTypeSpec)]
pub enum EnumItemStruct {
    /// Variant 1
    #[discriminant(0)]
    A {
        /// Id
        a: u8,
    },
    /// Variant 2
    #[discriminant(3)]
    B {
        /// Id 1
        a: u8,
        /// Id 2
        b: u8,
    },
    /// Variant 3
    #[discriminant(125)]
    C {
        /// Inner struct
        a: Inner,
    },
}

/// Enum containing a different enum
#[derive(CreateTypeSpec)]
pub enum NestedEnum {
    /// Variant one.
    #[discriminant(9)]
    One {
        /// Inner enum
        a: EnumItemStruct,
    },
}

/// Enum containing multiple inner structs
#[derive(CreateTypeSpec)]
pub enum DepthFirstEnum {
    /// Variant 1
    #[discriminant(0)]
    A {
        /// Field 1
        a: Inner,
    },
    /// Variant 2
    #[discriminant(3)]
    B {
        /// Field 1
        a: u8,
        /// Field 2
        b: Nested,
    },
    /// Variant 3
    #[discriminant(125)]
    C {
        /// Field 1
        a: Outer,
    },
}

/// Enum containing several empty variants
#[derive(CreateTypeSpec)]
pub enum EnumOne {
    /// Variant 1
    #[discriminant(1)]
    VariantOne {},
    /// Variant 2
    #[discriminant(2)]
    VariantTwo {},
}

/// Enum containing several empty variants
///
/// Only difference from [`EnumOne`] is the names.
#[derive(CreateTypeSpec)]
pub enum Enum2 {
    /// Variant 1
    #[discriminant(1)]
    Variant1 {},
    /// Variant 2
    #[discriminant(2)]
    Variant2 {},
}

/// Enum containing several empty variants
///
/// Only difference from [`EnumOne`] is the discriminants, and the name of the enum.
#[derive(CreateTypeSpec)]
pub enum Enum2WithDifferentDiscriminants {
    /// Variant 1
    #[discriminant(10)]
    Variant1 {},
    /// Variant 2
    #[discriminant(20)]
    Variant2 {},
}
