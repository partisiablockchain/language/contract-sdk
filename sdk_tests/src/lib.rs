//! A crate for testing the rest of the SDK.

pub mod state_types;

#[cfg(any(test, doc, feature = "test_lib"))]
pub mod test_contract_behaviour;
