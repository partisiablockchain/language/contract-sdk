#![doc = include_str!("../README.md")]

use pbc_abi::abi_model::AbiSerialize;
use pbc_traits::WriteRPC;

/// Struct used for testing failed writes.
///
/// Never actually writes anything, but will fail after specified amount of writes.
pub struct DummyWriter {
    remaining_writes: i32,
}

impl DummyWriter {
    /// Constructs a new [`DummyWriter`] which fails after `remaining_writes` bytes or more have been written.
    pub fn new(remaining_writes: i32) -> DummyWriter {
        DummyWriter { remaining_writes }
    }
}

impl std::io::Write for DummyWriter {
    /// Returns `Ok(buf.len())` without writing anything.
    ///
    /// If `buf.len() > remaining_bytes` will return `Ok(0)` indicating failure.
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.remaining_writes -= buf.len() as i32;
        if self.remaining_writes < 0 {
            Ok(0)
        } else {
            Ok(buf.len())
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

/// Calls `rpc_write_to` on `data with a writer meant to fail after `writes_to_allow` amount of writes,
/// with an assertion that the write failed.
pub fn fail_write_rpc(writes_to_allow: i32, data: impl WriteRPC + std::panic::UnwindSafe) {
    let mut writ = DummyWriter::new(writes_to_allow);

    let res = std::panic::catch_unwind(move || data.rpc_write_to(&mut writ));
    assert!(res.unwrap().is_err());
}

/// Calls `serialize_abi` on `data` with a writer meant to fail after `writes_to_allow` amount of writes,
/// with an assertion that the write failed.
pub fn fail_write_abi(writes_to_allow: i32, data: impl AbiSerialize + std::panic::UnwindSafe) {
    let mut writ = DummyWriter::new(writes_to_allow);

    let res = std::panic::catch_unwind(move || data.serialize_abi(&mut writ));
    assert!(res.unwrap().is_err());
}
