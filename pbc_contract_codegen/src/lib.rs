#![doc = include_str!("../README.md")]
#![recursion_limit = "128"]
extern crate proc_macro;
extern crate proc_macro2;
#[macro_use]
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;

use pbc_abi::abi_model::FunctionKind;
use syn::{parse_macro_input, parse_quote, AttributeArgs};

use pbc_contract_codegen_internal::{
    action_macro, callback_macro, init_macro, parse_attributes,
    parse_secret_input_type_from_attributes, parse_secret_input_type_from_function_return,
    parse_shortname_override, parse_zk_argument, state_macro, upgrade_macro, zk_macro, SecretInput,
    WrappedFunctionKind,
};

/// State contract annotation
///
/// **REQUIRED ANNOTATION**: This is a required annotation. A contract cannot be created without
/// a state.
///
/// Declares that the annotated struct is the top level of the contract state. This
/// macro must occur exactly once in any given contract.
///
/// # Example
///
/// ```ignore
/// # use pbc_contract_common::address::Address;
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::sorted_vec_map::SortedVecMap;
/// #[state]
/// pub struct VotingContractState {
///     proposal_id: u64,
///     mp_addresses: Vec<Address>,
///     votes: SortedVecMap<Address, u8>,
///     closed: u8,
/// }
/// ```
///
/// This macro implicitly derives [`ReadWriteState`](pbc_traits::ReadWriteState) for the struct.
/// The [`ReadWriteState`](pbc_traits::ReadWriteState) derive may fail if any of the state struct's
/// fields aren't impl [`ReadWriteState`](pbc_traits::ReadWriteState).
///
/// Furthermore, note that state serialization speeds are heavily affected by the types contained
/// in the state struct. Types with dynamic sizes ([`Option<T>`], [`String`])
/// are especially slow. For more background, see
/// [`ReadWriteState::SERIALIZABLE_BY_COPY`](pbc_traits::ReadWriteState::SERIALIZABLE_BY_COPY)
#[proc_macro_attribute]
pub fn state(_attrs: TokenStream, input: TokenStream) -> TokenStream {
    state_macro::handle_state_macro(input)
}

/// Initializer contract annotation
///
/// **REQUIRED HOOK**: This is a required hook. A contract cannot be created without an
/// initializer.
///
/// Init declares how the contract can be initialized. Init is an [`macro@action`] that is forced to run to initialize
/// the contract. Must occur exactly once in any given contract.
///
/// If the annotated function panics the contract will not be created.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
/// #[init]
/// pub fn initialize(
///     context: ContractContext,
///     // ... Invocation RPC arguments
/// ) -> (ContractState, Vec<EventGroup>)
/// # { (0, vec![]) }
/// ```
///
/// with following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - Additional arguments are must have [`pbc_traits::ReadRPC`] and [`pbc_traits::WriteRPC`]
///   implementations. These are treated as invocation arguments, and are included in the ABI.
///
/// Note that there are no previous state when initializing, in contrast to the
/// [`macro@action`] macro. If the initializer fails the contract will not be created.
#[proc_macro_attribute]
pub fn init(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args: AttributeArgs = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(args, vec!["zk".to_string()], vec![]);
    let zk = parse_zk_argument(&attributes);
    init_macro::handle_init_macro(input, zk)
}

/// Public `action` contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook. Most contracts will need at least one `action` hook.
///
/// Annotated function is a contract `action` that can be called from other contracts and dashboard.
///
/// If the annotated function panics the contract state [will be rolled back to the state before the action](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html).
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { 0 }
/// #[action(shortname = 0x33)]
/// pub fn action_internal_name(
///   context: ContractContext,
///   state: ContractState,
///   // ... Invocation RPC arguments
/// ) -> (ContractState, Vec<EventGroup>)
/// # { (0, vec![]) }
/// ```
///
/// with the following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - Additional arguments are must have [`pbc_traits::ReadRPC`] and [`pbc_traits::WriteRPC`]
///   implementations. These are treated as invocation arguments, and are included in the ABI.
///
/// The invocation receives the previous state, along with a context, and the declared
/// arguments, and must return the new state, along with a vector of
/// [`pbc_contract_common::events::EventGroup`]; a list of interactions with other contracts.
///
/// # Example
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::events::*;
/// # use pbc_contract_common::address::*;
/// # use pbc_contract_common::sorted_vec_map::SortedVecMap;
/// type ContractState = SortedVecMap<Address, bool>;
/// # type Metadata = u32;
///
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { SortedVecMap::new() }
/// #[action(shortname = 0x11)]
/// pub fn vote(
///     context: ContractContext,
///     mut state: ContractState,
///     vote: bool,
/// ) -> ContractState {
///     state.insert(context.sender, vote);
///     state
/// }
/// ```
///
/// # Shortname
///
/// In addition to the readable name, every invocation needs a shortname, a small unique identifier.
/// This shortname is automatically generated by default, but for cases where a specific shortname
/// is desirable, it can be set using the `shortname = <shortname>` attribute.
/// This has to be a [`u32`] and gets encoded as LEB128 (up to 5 bytes). These bytes are then
/// encoded as lowercase zero-padded hex.
///
/// For example:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// type Metadata = u32;
///
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { 0 }
/// #[action(shortname = 0x53)]
/// pub fn some_action(
///     context: ContractContext,
///     mut state: ContractState,
/// ) -> (ContractState, Vec<EventGroup>) {
///   // Do things
///   (state, vec![])
/// }
/// ```
#[proc_macro_attribute]
pub fn action(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args: AttributeArgs = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(
        args,
        vec!["shortname".to_string(), "zk".to_string()],
        vec![],
    );
    let shortname_override = parse_shortname_override(&attributes);
    let zk = parse_zk_argument(&attributes);
    action_macro::handle_action_macro(input, shortname_override, zk)
}

/// Public callback contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, only required if the contract needs callback
/// functionality.
///
/// Annotated function is a callback from an event sent by this contract.  Unlike actions,
/// callbacks must specify their shortname explicitly.
///
/// If the annotated function panics the contract state [will be rolled back to the state before the callback](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html).
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState  = u32;
/// # type Metadata = u32;
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { 0 }
/// #[callback(shortname = 0x13)]
/// pub fn callback_internal_name(
///   contract_context: ContractContext,
///   callback_context: CallbackContext,
///   state: ContractState,
///   // ... Invocation RPC arguments
/// ) -> (ContractState, Vec<EventGroup>)
/// # { (0, vec![]) }
/// ```
///
/// with following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - Additional arguments are must have [`pbc_traits::ReadRPC`] and [`pbc_traits::WriteRPC`]
///   implementations. These are treated as invocation arguments, and are included in the ABI.
///
/// The callback receives the previous state, along with two context objects, and the declared
/// arguments. The [`CallbackContext`](pbc_contract_common::context::CallbackContext) object contains the execution status of all the events
/// sent by the original transaction.
/// Just like actions, callbacks must return the new state, along with a vector of
/// [`EventGroup`](pbc_contract_common::events::EventGroup); a list of interactions with other contracts.
///
/// # Shortname
///
/// In addition to the readable name the callback needs a shortname, a small unique identifier.
/// This shortname must be set using the `shortname = <shortname>` attribute.
/// This has to be a [`u32`] and gets encoded as LEB128 (up to 5 bytes). These bytes are then
/// encoded as lowercase zero-padded hex.
#[proc_macro_attribute]
pub fn callback(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args: AttributeArgs = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(
        args,
        vec!["shortname".to_string(), "zk".to_string()],
        vec!["shortname".to_string()],
    );
    let shortname_override = parse_shortname_override(&attributes);
    let zk = parse_zk_argument(&attributes);
    callback_macro::handle_callback_macro(input, shortname_override, zk)
}

/// Secret input/action contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook. Most zero-knowledge contracts will need at least
/// one.
///
/// Annotated function is a contract invocation that allows a user to deliver secret input to the
/// contract. Can be thought of as the Zk variant of [`macro@action`]. The notable change is the
/// introduction of a required return value, of type
/// [`ZkInputDef`](pbc_contract_common::zk::ZkInputDef), that contains contract-supplied metadata,
/// along with some other configuration for the secret variable.
///
/// If the annotated function panics the input will be rejected, and changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html).
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// use pbc_zk_core::Sbi32;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_secret_input(shortname = 0xDEADB00F)]
/// pub fn function_name(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   // ... Invocation RPC arguments.
/// ) -> (ContractState, Vec<EventGroup>, ZkInputDef<Metadata, Sbi32>) {
///     (state, vec![], ZkInputDef::with_metadata(Some(SHORTNAME_MY_ON_INPUTTED), 0))
/// }
///
/// #[zk_on_variable_inputted(shortname=0x13)]
/// pub fn my_on_inputted(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   variable_id: SecretVarId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     // ...
/// #   (state, vec![], vec![])
/// }
/// ```
///
/// with following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - Invocation arguments must have a [`pbc_traits::ReadRPC`]
///   and a [`pbc_traits::WriteRPC`] implementation.
/// - The `Metadata` type given to [`ZkState`](pbc_contract_common::zk::ZkState) and [`ZkInputDef`](pbc_contract_common::zk::ZkInputDef) must be identical both for individual
///   functions, and across the entire contract.
/// - This function is only available with the `zk` feature enabled.
///
/// The function receives the previous states `ContractState` and
/// [`ZkState<Metadata>`](pbc_contract_common::zk::ZkState), along with the
/// [`ContractContext`](pbc_contract_common::context::ContractContext), and the declared RPC
/// arguments.
///
/// The function must return a tuple containing:
///
/// - New public state.
/// - Vector of [`EventGroup`](pbc_contract_common::events::EventGroup); a list of interactions with other contracts.
/// - Instance of [`ZkInputDef<Metadata>`](pbc_contract_common::zk::ZkInputDef) for declaring the
///   layout and metadata of a secret variable.
///
/// # Example
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// use pbc_zk_core::Sbi32;
/// type ContractState = u32;
/// type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_secret_input(shortname = 0x13, secret_type = "Sbi32")]
/// pub fn receive_bitlengths_10_10(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<u32>,
/// ) -> (ContractState, Vec<EventGroup>, ZkInputDef<u32, Sbi32>) {
///     let def = ZkInputDef::with_metadata(Some(SHORTNAME_MY_ON_INPUTTED), 23u32);
///     (state, vec![], def)
/// }
///
/// #[zk_on_variable_inputted(shortname=0x13)]
/// pub fn my_on_inputted(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   variable_id: SecretVarId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     // ...
/// #   (state, vec![], vec![])
/// }
/// ```
#[proc_macro_attribute]
pub fn zk_on_secret_input(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let fn_ast: syn::ItemFn = syn::parse(input.clone()).unwrap();

    let secret_type_input = parse_secret_input_type_from_function_return(&fn_ast.sig.output);

    let args: AttributeArgs = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(
        args,
        vec!["shortname".to_string(), "secret_type".to_string()],
        vec!["shortname".to_string()],
    );
    let shortname_override = parse_shortname_override(&attributes);

    // Parse secret type from attribute to validate equality, but otherwise ignore.
    let secret_type_input_attribute = parse_secret_input_type_from_attributes(attributes);
    if let (SecretInput::Some(t1), SecretInput::Some(t2)) =
        (&secret_type_input, secret_type_input_attribute)
    {
        if *t1 != t2 {
            panic!("Type of secret variable in function return type doesn't match type in function attribute.");
        }
    }

    let zk_input_def_arg = quote! { pbc_contract_common::zk::ZkInputDef<_, _> };

    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 3,
        additional_return_types: vec![(
            zk_input_def_arg,
            format_ident!("write_zk_input_def_result"),
        )],
        system_arguments: 3,
        fn_kind: FunctionKind::ZkSecretInputWithExplicitType,
        allow_rpc_arguments: true,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        shortname_override,
        None,
        "zk_on_secret_input",
        &function_kind,
        true,
        secret_type_input,
    )
}

/// Secret variable input zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when a Zk variable is confirmed and fully inputted.
/// This hook is exclusively called by the blockchain itself, and cannot be called manually from
/// the dashboard, nor from another contract.
///
/// Allows the contract to automatically react to ZK inputs.
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_variable_inputted(shortname = 0x13)]
/// pub fn zk_on_variable_inputted(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   variable_id: SecretVarId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
///
/// with following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - The `Metadata` type given to [`ZkState`](pbc_contract_common::zk::ZkState) and [`ZkInputDef`](pbc_contract_common::zk::ZkInputDef) must be identical both for individual
///   functions, and across the entire contract.
/// - This function is only available with the `zk` feature enabled.
///
/// The function receives:
/// - `ContractState`: The previous states.
/// - [`ZkState<Metadata>`](pbc_contract_common::zk::ZkState): The current state of the zk computation.
/// - [`ContractContext`](pbc_contract_common::context::ContractContext): The current contract context.
/// - [`SecretVarId`](pbc_contract_common::zk::SecretVarId): Id of the variable.
///
/// The function must return a tuple containing:
///
/// - New public state.
/// - Vector of [`EventGroup`](pbc_contract_common::events::EventGroup); a list of interactions with other contracts.
/// - [`Vec<ZkStateChange>`](pbc_contract_common::zk::ZkStateChange) declaring how to change the zk contract state.
///
/// # Example
///
/// This hook is commonly used to start the computation when enough inputs have been given, as
/// demonstrated in the following example:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # use pbc_contract_common::shortname::ShortnameZkComputation;
/// type ContractState = u32;
/// type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// const SHORTNAME_MY_COMPUTATION : ShortnameZkComputation = ShortnameZkComputation::from_u32(0x11);
///
/// #[zk_on_variable_inputted(shortname=0x13)]
/// pub fn zk_on_variable_inputted(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   variable_id: SecretVarId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     let zk_state_changes = if (zk_state.secret_variables.len() > 5) {
///         vec![ZkStateChange::start_computation(SHORTNAME_MY_COMPUTATION, vec![1, 2, 3], Some(SHORTNAME_MY_ON_COMPUTE_COMPLETE))]
///     } else {
///         vec![]
///     };
///     (state, vec![], zk_state_changes)
/// }
///
/// #[zk_on_compute_complete(shortname = 0x13)]
/// pub fn my_on_compute_complete(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   created_variables: Vec<SecretVarId>,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     // ...
/// # { (state, vec![], vec![]) }
/// }
/// ```
#[proc_macro_attribute]
pub fn zk_on_variable_inputted(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(
        args,
        vec!["shortname".to_string()],
        vec!["shortname".to_string()],
    );
    let shortname_override = parse_shortname_override(&attributes);

    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 4,
        fn_kind: FunctionKind::ZkVarInputted,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        shortname_override,
        Some(parse_quote! { pbc_contract_common::address::ShortnameZkVariableInputted }),
        "zk_on_variable_inputted",
        &function_kind,
        true,
        SecretInput::None,
    )
}

/// Secret variable rejection zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when a Zk variable is rejected for any reason.
/// This hook is exclusively called by the blockchain itself, and cannot be called manually from
/// the dashboard, nor from another contract.
///
/// Allows the contract to automatically react to ZK input rejection.
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_variable_rejected]
/// pub fn zk_on_variable_rejected(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   variable_id: SecretVarId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
#[proc_macro_attribute]
pub fn zk_on_variable_rejected(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(
        attrs.is_empty(),
        "No attributes are supported for zk_on_variable_rejected"
    );
    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 4,
        fn_kind: FunctionKind::ZkVarRejected,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        None,
        None,
        "zk_on_variable_rejected",
        &function_kind,
        false,
        SecretInput::None,
    )
}

/// Computation complete zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when a zero-knowledge computation is finished; this
/// can only happen after the use of
/// [`ZkStateChange::StartComputation`](pbc_contract_common::zk::ZkStateChange::StartComputation).
/// This hook is exclusively called by the blockchain itself, and cannot be called manually from
/// the dashboard, nor from another contract.
///
/// Allows the contract to automatically react to ZK computation completion.
///
/// Must have a signature of the following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_compute_complete(shortname = 0x13)]
/// pub fn function_name(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   created_variables: Vec<SecretVarId>,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
///
/// with following constraints:
///
/// - `ContractState` must be the type annotated with [`macro@state`], and must have an
///   [`pbc_traits::ReadWriteState`] implementation.
/// - The `Metadata` type given to [`ZkState`](pbc_contract_common::zk::ZkState) and [`ZkInputDef`](pbc_contract_common::zk::ZkInputDef) must be identical both for individual
///   functions, and across the entire contract.
/// - This function is only available with the `zk` feature enabled.
///
/// The function receives:
/// - `ContractState`: The previous states.
/// - [`ZkState<Metadata>`](pbc_contract_common::zk::ZkState): The current state of the zk computation.
/// - [`ContractContext`](pbc_contract_common::context::ContractContext): The current contract context.
/// - [`Vec<SecretVarId>`](pbc_contract_common::zk::SecretVarId): Ids of the computation's output variables.
///
/// The function must return a tuple containing:
///
/// - New public state.
/// - Vector of [`EventGroup`](pbc_contract_common::events::EventGroup); a list of interactions with other contracts.
/// - [`Vec<ZkStateChange>`](pbc_contract_common::zk::ZkStateChange) declaring how to change the zk contract state.
///
/// # Example
///
/// A commonly used pattern is to open the output variables given to `zk_on_compute_complete`, as
/// demonstrated in the following example:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// type ContractState = u32;
/// type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_compute_complete(shortname=0x13)]
/// pub fn zk_on_compute_complete(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   created_variables: Vec<SecretVarId>,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     (state, vec![], vec![ZkStateChange::OpenVariables { variables: created_variables }])
/// }
/// ```
#[proc_macro_attribute]
pub fn zk_on_compute_complete(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(attrs as AttributeArgs);
    let attributes = parse_attributes(
        args,
        vec!["shortname".to_string()],
        vec!["shortname".to_string()],
    );
    let shortname_override = parse_shortname_override(&attributes);

    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 4,
        fn_kind: FunctionKind::ZkComputeComplete,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        shortname_override,
        Some(parse_quote! { pbc_contract_common::address::ShortnameZkComputeComplete }),
        "zk_on_compute_complete",
        &function_kind,
        true,
        SecretInput::None,
    )
}

/// Secret variable opened zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when a contract opens one or more secret
/// variables; this can only happen after the use of
/// [`ZkStateChange::OpenVariables`](pbc_contract_common::zk::ZkStateChange::OpenVariables).
/// This hook is exclusively called by the blockchain itself, and cannot be called manually from
/// the dashboard, nor from another contract.
///
/// Allows the contract to automatically react to opening of ZK variables.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_variables_opened]
/// pub fn zk_on_variables_opened(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   opened_variables: Vec<SecretVarId>,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
///
/// Where `opened_variables` is a [`Vec`] of the opened variables.
///
/// # Example
///
/// Common usages include post-processing of computation results; for example
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = Vec<Vec<u8>>;
/// # type Metadata = u32;
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { vec![] }
/// #[zk_on_variables_opened]
/// pub fn zk_on_sum_variable_opened(
///   context: ContractContext,
///   mut state: ContractState,
///   zk_state: ZkState<Metadata>,
///   opened_variables: Vec<SecretVarId>,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>) {
///     let result_var_id: SecretVarId = *opened_variables.get(0).unwrap();
///     let result_var: ZkClosed<Metadata> = zk_state.get_variable(result_var_id).unwrap();
///     let result: Vec<u8> = result_var.data.as_ref().unwrap().clone();
///     state.push(result);
///     (state, vec![], vec![])
/// }
/// ```
#[proc_macro_attribute]
pub fn zk_on_variables_opened(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(
        attrs.is_empty(),
        "No attributes are supported for zk_on_variables_opened"
    );
    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 4,
        fn_kind: FunctionKind::ZkVarOpened,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        None,
        None,
        "zk_on_variables_opened",
        &function_kind,
        false,
        SecretInput::None,
    )
}

/// Data-attestation complete zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when the contract is informed of the availability of
/// signatures for attested data.  This can only happen after the use of
/// [`ZkStateChange::Attest`](pbc_contract_common::zk::ZkStateChange::Attest).  This hook is
/// exclusively called by the blockchain itself, and cannot be called manually from the dashboard,
/// nor from another contract.
///
/// Allows the contract to automatically react to data-attestations.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_attestation_complete]
/// pub fn zk_on_attestation_complete(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   attestation_id: AttestationId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
///
/// Where [`ZkState`](pbc_contract_common::zk::ZkState) can be further accessed to determine signatures, etc.
#[proc_macro_attribute]
pub fn zk_on_attestation_complete(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(
        attrs.is_empty(),
        "No attributes are supported for zk_on_attestation_complete"
    );
    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 4,
        fn_kind: FunctionKind::ZkAttestationComplete,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        None,
        None,
        "zk_on_attestation_complete",
        &function_kind,
        false,
        SecretInput::None,
    )
}

/// External event zero-knowledge contract annotation
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// zero-knowledge contract. The default behaviour is to do nothing.
///
/// Changes to the contract state [will be rolled back](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-interactions-on-the-blockchain.html)
/// if the contract panics.
///
/// Annotated function is automatically called when the contract is informed of a new external event
/// that has been confirmed by the EVM oracle to have occurred on an external EVM chain. This can
/// only happen after the use of
/// [`ZkStateChange::SubscribeToEvmEvents`](pbc_contract_common::zk::ZkStateChange::SubscribeToEvmEvents).
/// This hook is exclusively called by the blockchain itself, and cannot be called manually from the
/// dashboard, nor from another contract.
///
/// Allows the contract to automatically react to EVM events.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # use pbc_contract_common::zk::*;
/// # use pbc_contract_common::events::*;
/// use pbc_contract_common::zk::evm_event::{ExternalEventId, EventSubscriptionId};
/// # type ContractState = u32;
/// # type Metadata = u32;
///
/// # #[init(zk = true)] pub fn initialize(context: ContractContext, zk_state: ZkState<Metadata>) -> ContractState { 0 }
///
/// #[zk_on_external_event]
/// pub fn zk_on_external_event(
///   context: ContractContext,
///   state: ContractState,
///   zk_state: ZkState<Metadata>,
///   subscription_id: EventSubscriptionId,
///   event_id: ExternalEventId,
/// ) -> (ContractState, Vec<EventGroup>, Vec<ZkStateChange>)
/// # { (state, vec![], vec![]) }
/// ```
///
/// Where [`ZkState`](pbc_contract_common::zk::ZkState) can be further accessed to read the event, etc.
#[proc_macro_attribute]
pub fn zk_on_external_event(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(
        attrs.is_empty(),
        "No attributes are supported for zk_on_external_event"
    );
    let function_kind = WrappedFunctionKind {
        return_state_and_events: true,
        min_num_return_types: 1,
        additional_return_types: vec![(
            quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
            format_ident!("write_zk_state_change"),
        )],
        system_arguments: 5,
        fn_kind: FunctionKind::ZkExternalEvent,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: false,
    };
    zk_macro::handle_zk_macro(
        input,
        None,
        None,
        "zk_on_external_event",
        &function_kind,
        false,
        SecretInput::None,
    )
}

/// Contract annotation for checking whether the contract can be upgraded.
///
/// Rust smart contracts can be upgraded through a two phase process:
///
/// 1. The original smart contract must allow the upgrade, through whatever process it deems
///    appropriate. This might be as simple as a user with upgrade permissions, or as complex as
///    a DAO. The upgrade permission system is defined through the [`macro@upgrade_is_allowed`]
///    annotation. A contract cannot be upgraded without a [`macro@upgrade_is_allowed`] annotated function.
/// 2. The smart contract that is being upgraded to must be capable of converting from the
///    state of the original contract to its own state. This is done through [`macro@upgrade`].
///    A hidden check ensures that the upgraded smart contract has correctly modelled the state of
///    the original.
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// contract. Contracts cannot be upgraded without this hook.
///
/// **NOTE**: This hook is _only_ needed when planning to upgrade the contract in the future. You
/// do not need this if this is the final version of your contract.
///
/// **WARNING**: By implementing this hook, you are taking responsibility for maintaining your
/// contract securely in perputuity. If your upgrade logic is faulty, it can allow a hacker to
/// replace your contract with a backdoor'ed contract. Consider your upgrade permissions carefully.
///
/// The contract will not be upgraded if the Rust contract panics.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// use pbc_contract_common::upgrade::ContractHashes;
/// # type ContractState = u32;
///
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { 0 }
///
/// #[upgrade_is_allowed]
/// pub fn upgrade_is_allowed(
///   context: ContractContext,
///   state: ContractState,
///   hashes_of_original_contract: ContractHashes,
///   hashes_of_new_contract: ContractHashes,
///   rpc_for_upgraded_contract: Vec<u8>,
/// ) -> bool
/// # { false }
/// ```
#[proc_macro_attribute]
pub fn upgrade_is_allowed(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(
        attrs.is_empty(),
        "No attributes are supported for upgrade_is_allowed"
    );
    upgrade_macro::handle_upgrade_is_allowed(input)
}

/// Contract annotation for upgrading the contract state.
///
/// Rust smart contracts can be upgraded through a two phase process:
///
/// 1. The original smart contract must allow the upgrade, through whatever process it deems
///    appropriate. This might be as simple as a user with upgrade permissions, or as complex as
///    a DAO. The upgrade permission system is defined through the [`macro@upgrade_is_allowed`]
///    annotation. A contract cannot be upgraded without a [`macro@upgrade_is_allowed`] annotated function.
/// 2. The smart contract that is being upgraded to must be capable of converting from the
///    state of the original contract to its own state. This is done through [`macro@upgrade`].
///    A hidden check ensures that the upgraded smart contract has correctly modelled the state of
///    the original.
///
/// **OPTIONAL HOOK**: This is an optional hook, and is not required for a well-formed
/// contract. Contracts cannot be the target of an upgrade if it doesn't possess a function
/// annotated with [`macro@upgrade`].
///
/// **NOTE**: This hook is _only_ needed when upgrading a different contract. You do not need this
/// if you are writing an entirely new contract, or if you do not plan to upgrade from a different contract.
///
/// The contract upgrade will be aborted if the upgrade hook panics.
///
/// Annotated function must have a signature of following format:
///
/// ```
/// # use pbc_contract_codegen::*;
/// # use pbc_contract_common::context::*;
/// # type OriginalContractState = u32;
/// # type ContractState = u32;
///
/// # #[init(zk = false)] pub fn initialize(context: ContractContext) -> ContractState { 0 }
///
/// #[upgrade]
/// pub fn upgrade(
///   context: ContractContext,
///   original_state: OriginalContractState,
/// ) -> ContractState
/// # { original_state }
/// ```
#[proc_macro_attribute]
pub fn upgrade(attrs: TokenStream, input: TokenStream) -> TokenStream {
    assert!(attrs.is_empty(), "No attributes are supported for upgrade");
    upgrade_macro::handle_upgrade(input)
}
