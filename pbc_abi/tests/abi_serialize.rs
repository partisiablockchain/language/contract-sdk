//! Test serialization of abi types
use pbc_abi::abi_model::TypeSpec::NamedTypeRef;
use pbc_abi::abi_model::{
    AbiSerialize, FnAbi, FunctionKind, NamedEntityAbi, NamedTypeSpec, TypeSpec,
};
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};

/// Asserts that abi type is serialized to given slice. Also asserts that when deserialized
/// it serializes to the same result again.
fn assert_serialized_to<T: AbiSerialize>(e: &T, expected_buf: &[u8]) {
    let mut gotten_buf = Vec::<u8>::new();
    e.serialize_abi(&mut gotten_buf).unwrap();
    assert_eq!(gotten_buf.as_slice(), expected_buf);
}

/// Named entity types are serialized and deserialized correctly
#[test]
pub fn serialize_named_entity_0() {
    let obj = NamedEntityAbi::new::<u64>(
        "name".to_string(),
        &mut NamedTypeLookup::new(),
        &mut NamedTypeSpecs::new(),
    );
    let expected_buf = [
        0, 0, 0, 4, // Name Length
        0x6e, 0x61, 0x6d, 0x65, // Name
        0x04, // Field 0 type ordinal
    ];
    assert_serialized_to(&obj, &expected_buf);
}

/// Empty struct is serialized and deserialized correctly
#[test]
pub fn serialize_type_abi_0() {
    let obj = NamedTypeSpec::new_struct("name".to_string());
    let expected_buf = [
        1, // It's a struct
        0, 0, 0, 4, // Name Length
        0x6e, 0x61, 0x6d, 0x65, // Name
        0, 0, 0, 0, // Arguments length
    ];
    assert_serialized_to(&obj, &expected_buf);
}

/// Struct with fields is serialized and deserialized correctly
#[test]
pub fn serialize_type_abi_1() {
    let mut obj = NamedTypeSpec::new_struct("name".to_string());

    let field = NamedEntityAbi::new::<u64>(
        "field".to_string(),
        &mut NamedTypeLookup::new(),
        &mut NamedTypeSpecs::new(),
    );

    obj.add_field(field);
    let expected_buf = [
        1, // It's a struct
        0, 0, 0, 4, // Name Length
        0x6e, 0x61, 0x6d, 0x65, // Name
        0, 0, 0, 1, // Arguments length
        0, 0, 0, 5, // Field 0 name length
        0x66, 0x69, 0x65, 0x6c, 0x64, // Field 0 name
        0x04, // Field 0 type ordinal
    ];
    assert_serialized_to(&obj, &expected_buf);
}

/// Function abi is serialized and deserialized correctly
#[test]
pub fn serialize_fn_abi_0() {
    let obj = FnAbi::new("name".to_string(), None, FunctionKind::Action);
    let expected_buf = [
        2, // Function kind: Action
        0, 0, 0, 4, // Name Length
        0x6e, 0x61, 0x6d, 0x65, // Name
        0,    // shortname
        0, 0, 0, 0, // Arguments length
    ];
    assert_serialized_to(&obj, &expected_buf);
}

/// Function abi with leb shortname is serialized and deserialized correctly
#[test]
pub fn serialize_fn_abi_with_shortname() {
    let obj = FnAbi::new(
        "name".to_string(),
        Some(vec![0xff, 0x01]),
        FunctionKind::Action,
    );
    let expected_buf = [
        2, // Function kind: Action
        0, 0, 0, 4, // Name Length
        0x6e, 0x61, 0x6d, 0x65, // Name
        0xff, 0x01, // shortname
        0, 0, 0, 0, // Arguments length
    ];
    assert_serialized_to(&obj, &expected_buf);
}

/// Simple types are serialized and deserialized correctly
#[test]
pub fn serialize_simple_type_spec() {
    assert_serialized_to(&TypeSpec::U8, &[0x01]);
    assert_serialized_to(&TypeSpec::U16, &[0x02]);
    assert_serialized_to(&TypeSpec::U32, &[0x03]);
    assert_serialized_to(&TypeSpec::U64, &[0x04]);
    assert_serialized_to(&TypeSpec::U128, &[0x05]);
    assert_serialized_to(&TypeSpec::U256, &[0x18]);
    assert_serialized_to(&TypeSpec::I8, &[0x06]);
    assert_serialized_to(&TypeSpec::I16, &[0x07]);
    assert_serialized_to(&TypeSpec::I32, &[0x08]);
    assert_serialized_to(&TypeSpec::I64, &[0x09]);
    assert_serialized_to(&TypeSpec::I128, &[0x0a]);
    assert_serialized_to(&TypeSpec::String, &[0x0b]);
    assert_serialized_to(&TypeSpec::Bool, &[0x0c]);
    assert_serialized_to(&TypeSpec::Address, &[0x0d]);
    assert_serialized_to(&TypeSpec::Hash, &[0x13]);
    assert_serialized_to(&TypeSpec::PublicKey, &[0x14]);
    assert_serialized_to(&TypeSpec::Signature, &[0x15]);
    assert_serialized_to(&TypeSpec::BlsPublicKey, &[0x16]);
    assert_serialized_to(&TypeSpec::BlsSignature, &[0x17]);
}

/// Named type reference is serialized and deserialized correctly
#[test]
pub fn serialize_named_type_ref() {
    let ref_1 = NamedTypeRef { index: 0 };
    assert_serialized_to(&ref_1, &[0, 0]);
    let ref_1 = NamedTypeRef { index: 16 };
    assert_serialized_to(&ref_1, &[0, 16]);
}

/// Composite types are serialized and deserialized correctly
#[test]
pub fn serialize_composite_type_spec() {
    let vec = TypeSpec::Vec {
        value_type: TypeSpec::U8.into(),
    };
    assert_serialized_to(&vec, &[0x0e, 0x01]);
    let map = TypeSpec::Map {
        key_type: TypeSpec::Address.into(),
        value_type: TypeSpec::U16.into(),
    };
    assert_serialized_to(&map, &[0x0f, 0x0d, 0x02]);
    let set = TypeSpec::Set {
        value_type: TypeSpec::U8.into(),
    };
    assert_serialized_to(&set, &[0x10, 0x01]);
    let sized_byte_array = TypeSpec::SizedArray {
        value_type: TypeSpec::U8.into(),
        len: 4,
    };
    assert_serialized_to(&sized_byte_array, &[0x1a, 0x01, 0x04]);
    let option = TypeSpec::Option {
        value_type: TypeSpec::I32.into(),
    };
    assert_serialized_to(&option, &[0x12, 0x08]);
    let avl_tree_map = TypeSpec::AvlTreeMap {
        key_type: TypeSpec::Address.into(),
        value_type: TypeSpec::U16.into(),
    };
    assert_serialized_to(&avl_tree_map, &[0x19, 0x0d, 0x02]);
}

/// Composite types with other composite types are serialized and deserialized correctly
#[test]
pub fn serialize_recursive_composite() {
    let rec = TypeSpec::Vec {
        value_type: TypeSpec::Vec {
            value_type: TypeSpec::Option {
                value_type: TypeSpec::Map {
                    key_type: TypeSpec::Address.into(),
                    value_type: TypeSpec::Vec {
                        value_type: TypeSpec::U8.into(),
                    }
                    .into(),
                }
                .into(),
            }
            .into(),
        }
        .into(),
    };
    assert_serialized_to(&rec, &[0x0e, 0x0e, 0x12, 0x0f, 0x0d, 0x0e, 0x01]);
}
