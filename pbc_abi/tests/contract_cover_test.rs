//! Test the ABI generation for a contract.
use pbc_abi::abi_model::{AbiSerialize, ContractAbi, FnAbi, FunctionKind, NamedTypeSpec, TypeSpec};
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use test_utility::fail_write_abi;

/// Adding a new on_secret_input action to a ContractAbi, adds the action to the list of actions in
/// the ABI.
#[test]
pub fn contract_should_update_actions() {
    let state_abi = TypeSpec::U8;
    let mut contract = ContractAbi::new(state_abi);

    let mut contract_action = FnAbi::new(
        "f".to_string(),
        Some(vec![16]),
        FunctionKind::ZkSecretInputWithExplicitType,
    );
    contract_action.secret_argument::<i32>(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new());
    contract.actions(vec![contract_action]);

    let mut output = Vec::new();
    contract.serialize_abi(&mut output).unwrap();

    let expected_abi_bytes = [
        0, 0, 0, 0, // No types
        0, 0, 0, 1,  // 1 Action
        23, // ZkSecretInputWithExplicitType kind
        0, 0, 0, 1,   // Name length
        102, // 'f'
        16,  // shortname
        0, 0, 0, 0, // No args in function
        0, 0, 0, 12, // length of secret input name
        115, 101, 99, 114, 101, 116, 95, 105, 110, 112, 117, 116, 8, // 'secret_input'
        1, // State
    ];

    assert_eq!(expected_abi_bytes, output.as_slice());
}

/// Adding a new type to a ContractAbi adds the type to the list of custom types in the ABI.
#[test]
pub fn contract_should_update_types() {
    let state_abi = TypeSpec::U8;
    let mut contract = ContractAbi::new(state_abi);

    let struct_abi_instance = NamedTypeSpec::new_struct("MyS".to_string());
    contract.types(vec![struct_abi_instance]);

    let mut output = Vec::new();
    contract.serialize_abi(&mut output).unwrap();

    let expected_abi_bytes = [
        0, 0, 0, 1, // 1 type
        1, // Struct type kind
        0, 0, 0, 3, // Name length
        77, 121, 83, // 'MyS'
        0, 0, 0, 0, // No fields
        0, 0, 0, 0, // No actions
        1, // State
    ];

    assert_eq!(expected_abi_bytes, output.as_slice());
}

/// Writing the ABI with an added type, to a buffer with too little capacity panics.
#[test]
pub fn buffer_too_small_for_types() {
    let state_abi = TypeSpec::U8;
    let mut contract = ContractAbi::new(state_abi);

    let struct_abi_instance = NamedTypeSpec::new_struct("MyS".to_string());
    contract.types(vec![struct_abi_instance]);

    fail_write_abi(0, contract);
}

/// Writing the ABI with an added action, to a buffer with too little capacity panics.
#[test]
pub fn buffer_too_small_for_actions() {
    let state_abi = TypeSpec::U8;
    let mut contract = ContractAbi::new(state_abi);

    let contract_action = FnAbi::new("f".to_string(), Some(vec![16]), FunctionKind::Action);
    contract.actions(vec![contract_action]);

    fail_write_abi(0, contract);
}
