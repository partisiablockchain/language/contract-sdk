//! Tests of [`FnAbi`].
use pbc_abi::abi_model::{FnAbi, FunctionKind};
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};

/// Test that function must be be an zk input invocation in order to define an secret argument.
#[test]
pub fn secret_argument_action_kind() {
    let mut fnabi = FnAbi::new("my_action".to_string(), None, FunctionKind::Action);
    let res = std::panic::catch_unwind(move || {
        fnabi.secret_argument::<i32>(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new())
    });

    assert!(res.is_err());
}
