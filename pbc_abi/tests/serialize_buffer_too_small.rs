//! Testing error handling of failing to write abi types
use pbc_abi::abi_model::{FnAbi, FunctionKind};
use test_utility::fail_write_abi;

/// Coverage test for failing to write abi bytes for function kind
#[test]
pub fn buffer_too_small_for_kind() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(0, fnabi);
}

/// Coverage test for failing to write abi bytes for function name
#[test]
pub fn buffer_too_small_for_name() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(5, fnabi);
}

/// Coverage test for failing to write abi bytes for function shortname
#[test]
pub fn buffer_too_small_for_shortname() {
    let fnabi = FnAbi::new(
        "my_func".to_string(),
        Some(vec![0xff, 0xff, 0x01]),
        FunctionKind::Action,
    );
    fail_write_abi(12, fnabi);
}

/// Coverage test for failing to write abi bytes for function arguments
#[test]
pub fn buffer_too_small_for_args() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(16, fnabi);
}
