use crate::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use read_write_int::WriteInt;
use std::io::Write;

use crate::CreateTypeSpec;

use super::{AbiSerialize, NamedEntityAbi};

/// A struct representing a function in the ABI.
///
/// Serialized with the ABI format.
pub struct FnAbi {
    name: String,
    shortname: Vec<u8>,
    fn_kind: FunctionKind,
    args: Vec<NamedEntityAbi>,
    secret_arg: Option<NamedEntityAbi>,
}

impl FnAbi {
    /// Create a function abi with the supplied name.
    pub fn new(name: String, shortname: Option<Vec<u8>>, fn_kind: FunctionKind) -> Self {
        FnAbi {
            name,
            shortname: shortname.unwrap_or(vec![0]),
            fn_kind,
            args: Vec::new(),
            secret_arg: None,
        }
    }

    /// Add an argument to this instance. Types are inferred.
    ///
    /// * `name` - the name of the type.
    /// * `named_types` - the lookup table of named types to index for the ABI generation. See `pbc-abigen` for details.
    /// * `named_type_specs` - the lookup table of type index to type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn argument<T: CreateTypeSpec>(
        &mut self,
        name: String,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) {
        self.args.push(NamedEntityAbi::new::<T>(
            name,
            named_types,
            named_type_specs,
        ));
    }

    /// Add a secret argument to this instance.
    /// Name is "secret_input"
    ///
    /// * `named_types` - the lookup table of named types to index for the ABI generation. See `pbc-abigen` for details.
    /// * `named_type_specs` - the lookup table of type index to type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn secret_argument<T: CreateTypeSpec>(
        &mut self,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) {
        assert_eq!(
            self.fn_kind,
            FunctionKind::ZkSecretInputWithExplicitType,
            "Only function with kind ZkSecretInputWithExplicitType can take a secret argument"
        );
        self.secret_arg = Some(NamedEntityAbi::new::<T>(
            "secret_input".to_string(),
            named_types,
            named_type_specs,
        ));
    }

    /// Gets the shortname of the function.
    pub fn shortname(&self) -> &Vec<u8> {
        &self.shortname
    }
}

impl AbiSerialize for FnAbi {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        writer.write_u8(self.fn_kind as u8)?;
        self.name.serialize_abi(writer)?;
        writer.write_all(&self.shortname)?;
        self.args.serialize_abi(writer)?;
        match self.secret_arg {
            None => Ok(()),
            Some(ref arg) => arg.serialize_abi::<T>(writer),
        }
    }
}

/// Denotes the kind of the ABI function hook.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
#[repr(u8)]
pub enum FunctionKind {
    /// Kind for `init` hook.
    Init = 0x01,
    /// Kind for `action` hook.
    Action = 0x02,
    /// Kind for `callback` hook.
    Callback = 0x03,
    /// Kind for `zk_on_secret_input` hook.
    #[deprecated(note = "Use ZkSecretInputWithExplicitType instead")]
    ZkSecretInput = 0x10,
    /// Kind for `zk_on_variable_inputted` hook.
    ZkVarInputted = 0x11,
    /// Kind for `zk_on_variable_rejected` hook.
    ZkVarRejected = 0x12,
    /// Kind for `zk_on_compute_complete` hook.
    ZkComputeComplete = 0x13,
    /// Kind for `zk_on_variable_opened` hook.
    ZkVarOpened = 0x14,
    /// Kind for `zk_on_user_variable_opened` hook.
    #[deprecated(note = "Use ZkVarOpened instead")]
    ZkUserVarOpened = 0x15,
    /// Kind for `zk_on_attestation_complete` hook.
    ZkAttestationComplete = 0x16,
    /// Kind for `zk_on_secret_input` hook.
    ZkSecretInputWithExplicitType = 0x17,
    /// Kind for `zk_on_external_event` hook.
    ZkExternalEvent = 0x18,
    /// Kind for `upgrade_is_allowed` hook.
    UpgradeIsAllowed = 0x20,
    /// Kind for `upgrade` hook.
    UpgradeFromState = 0x21,
}
