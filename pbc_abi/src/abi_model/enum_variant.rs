use std::io::Write;

use read_write_int::WriteInt;

use super::{AbiSerialize, TypeSpec};

/// A struct representing an enum variant.
///
/// Serialized with the ABI format.
pub struct EnumVariant {
    /// The discriminant of the variant.
    pub discriminant: u8,
    /// The raw type spec for the variant, should always be a struct.
    pub type_spec: TypeSpec,
}

impl EnumVariant {
    /// Instantiate an `EnumVariant` with  the specified discriminant.
    ///
    /// * `discriminant` - the discriminant of the variant.
    /// * `type_spec` - the type_spec for the variant.
    pub fn new(discriminant: u8, type_spec: TypeSpec) -> Self {
        EnumVariant {
            discriminant,
            type_spec,
        }
    }
}

impl AbiSerialize for EnumVariant {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        writer.write_u8(self.discriminant)?;
        self.type_spec.serialize_abi(writer)
    }
}
