use super::{AbiSerialize, FnAbi, NamedTypeSpec, TypeSpec};
use std::io::Write;

/// The `ContractAbi` describes the ABI for a contract including all the actions
/// in the contract and the contract state + all user-defined structs within the state and actions.
///
/// Serialized with the ABI format.
pub struct ContractAbi {
    /// The declared named types in the contract.
    pub types: Vec<NamedTypeSpec>,
    /// The declared actions in the contract.
    pub actions: Vec<FnAbi>,
    state: TypeSpec,
}

impl ContractAbi {
    /// Construct a new `ContractAbi` with the specified init function and state type ordinal list.
    pub fn new(state: TypeSpec) -> Self {
        let actions = Vec::new();
        let types = Vec::new();
        ContractAbi {
            actions,
            state,
            types,
        }
    }

    /// Set the actions of this `ContractAbi` instance to the supplied vector.
    pub fn actions(&mut self, actions: Vec<FnAbi>) {
        self.actions = actions;
    }

    /// Set the types of this `ContractAbi` instance to the supplied vector.
    pub fn types(&mut self, types: Vec<NamedTypeSpec>) {
        self.types = types;
    }
}

impl AbiSerialize for ContractAbi {
    /// Serialize this struct according to the ABI specification.
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.types.serialize_abi(writer)?;
        self.actions.serialize_abi(writer)?;
        self.state.serialize_abi(writer)
    }
}
