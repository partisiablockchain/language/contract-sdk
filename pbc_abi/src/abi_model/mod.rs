//! The abi model specification

mod contract;
mod enum_variant;
mod func;
mod named_entity;
mod named_type_spec;
mod type_spec;

pub use contract::ContractAbi;
pub use enum_variant::EnumVariant;
pub use func::{FnAbi, FunctionKind};
pub use named_entity::NamedEntityAbi;
pub use named_type_spec::NamedTypeSpec;
pub use type_spec::TypeSpec;

use read_write_int::WriteInt;
use std::io::Write;

/// A trait for serializing ABI objects.
/// This is different from `ReadWriteState` and `WriteRPC` since it is intended
/// for serializing across the FFI layer between contracts and `pbc-abigen`.
/// It does not serialize struct fields directly, but according to the ABI specification.
pub trait AbiSerialize {
    /// Serialize the ABI to the given writer.
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()>;
}

impl AbiSerialize for String {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        let utf_bytes = self.as_bytes();
        writer.write_u32_be(utf_bytes.len() as u32).unwrap();
        writer.write_all(utf_bytes)
    }
}

impl<T: AbiSerialize> AbiSerialize for Vec<T> {
    fn serialize_abi<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        writer.write_u32_be(self.len() as u32)?;
        for t in self.iter() {
            AbiSerialize::serialize_abi(t, writer)?;
        }
        Ok(())
    }
}
