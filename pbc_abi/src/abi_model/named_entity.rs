use crate::abi_model::TypeSpec;
use crate::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use crate::CreateTypeSpec;
use std::io::Write;

/// A struct representing any (name, type) entity.
/// In this case it is function arguments and struct fields.
///
/// Serialized with the ABI format.
pub struct NamedEntityAbi {
    /// The name of the field or argument.
    pub name: String,
    /// The raw type spec for the type of the argument.
    pub type_spec: TypeSpec,
}

impl NamedEntityAbi {
    /// Instantiate a `NamedEntityAbi` with  the specified name.
    ///
    /// * `name` - the name of the type.
    /// * `type_spec` - the type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn new<T: CreateTypeSpec>(
        name: String,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Self {
        let type_spec = T::__ty_generate_spec(named_types, named_type_specs);
        NamedEntityAbi { name, type_spec }
    }
}

impl super::AbiSerialize for NamedEntityAbi {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.name.serialize_abi(writer)?;
        self.type_spec.serialize_abi(writer)
    }
}

#[cfg(test)]
mod test {
    use crate::abi_model::AbiSerialize;

    use super::{NamedEntityAbi, NamedTypeLookup, NamedTypeSpecs};

    #[test]
    fn read_write_rpc_smoke_test() {
        let abi = NamedEntityAbi::new::<String>(
            "my_name".to_string(),
            &mut NamedTypeLookup::new(),
            &mut NamedTypeSpecs::new(),
        );

        let mut output: Vec<u8> = Vec::new();
        abi.serialize_abi(&mut output).unwrap();

        assert_eq!(
            output,
            vec![0, 0, 0, 7, 109, 121, 95, 110, 97, 109, 101, 11]
        )
    }
}
