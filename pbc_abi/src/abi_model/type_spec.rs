use crate::abi_model::AbiSerialize;
use read_write_int::WriteInt;
use std::io::Write;

/// An [abi type](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#type-specifier-binary-format).
///
/// Defines the possible types for struct fields or function arguments.
#[derive(Eq, PartialEq, Debug)]
pub enum TypeSpec {
    /// Reference to a named type
    NamedTypeRef {
        /// Index of the named type
        index: u8,
    },
    /// [`u8`]
    U8,
    /// [`u16`]
    U16,
    /// [`u32`]
    U32,
    /// [`u64`]
    U64,
    /// [`u128`]
    U128,
    /// u256
    U256,
    /// [`i8`]
    I8,
    /// [`i16`]
    I16,
    /// [`i32`]
    I32,
    /// [`i64`]
    I64,
    /// [`i128`]
    I128,
    /// [`String`]
    String,
    /// [`bool`]
    Bool,
    /// An address on the blockchain
    Address,
    /// A hash
    Hash,
    /// A public key on the blockchain
    PublicKey,
    /// A signature on the blockchain
    Signature,
    /// A BLS public key
    BlsPublicKey,
    /// A BLS signature
    BlsSignature,
    /// A vector of elements
    Vec {
        /// The inner type
        value_type: Box<TypeSpec>,
    },
    /// A map of elements
    Map {
        /// Type of keys
        key_type: Box<TypeSpec>,
        /// Type of values
        value_type: Box<TypeSpec>,
    },
    /// A set of elements
    Set {
        /// The inner type
        value_type: Box<TypeSpec>,
    },
    /// A sized byte array [`[u8; LEN]`]
    SizedArray {
        /// Type of values
        value_type: Box<TypeSpec>,
        /// Length of the array
        len: u8,
    },
    /// The option type
    Option {
        /// The inner type
        value_type: Box<TypeSpec>,
    },
    /// An avl tree map
    AvlTreeMap {
        /// Type of keys
        key_type: Box<TypeSpec>,
        /// Type of values
        value_type: Box<TypeSpec>,
    },
}

impl AbiSerialize for TypeSpec {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        match self {
            TypeSpec::NamedTypeRef { index } => {
                writer.write_u8(0x00)?;
                writer.write_u8(*index)
            }
            TypeSpec::U8 { .. } => writer.write_u8(0x01),
            TypeSpec::U16 { .. } => writer.write_u8(0x02),
            TypeSpec::U32 { .. } => writer.write_u8(0x03),
            TypeSpec::U64 { .. } => writer.write_u8(0x04),
            TypeSpec::U128 { .. } => writer.write_u8(0x05),
            TypeSpec::U256 { .. } => writer.write_u8(0x18),
            TypeSpec::I8 { .. } => writer.write_u8(0x06),
            TypeSpec::I16 { .. } => writer.write_u8(0x07),
            TypeSpec::I32 { .. } => writer.write_u8(0x08),
            TypeSpec::I64 { .. } => writer.write_u8(0x09),
            TypeSpec::I128 { .. } => writer.write_u8(0x0a),
            TypeSpec::String { .. } => writer.write_u8(0x0b),
            TypeSpec::Bool { .. } => writer.write_u8(0x0c),
            TypeSpec::Address { .. } => writer.write_u8(0x0d),
            TypeSpec::Hash { .. } => writer.write_u8(0x13),
            TypeSpec::PublicKey { .. } => writer.write_u8(0x14),
            TypeSpec::Signature { .. } => writer.write_u8(0x15),
            TypeSpec::BlsPublicKey { .. } => writer.write_u8(0x16),
            TypeSpec::BlsSignature { .. } => writer.write_u8(0x17),
            TypeSpec::Vec { value_type } => {
                writer.write_u8(0x0e)?;
                value_type.serialize_abi(writer)
            }
            TypeSpec::Map {
                key_type,
                value_type,
            } => {
                writer.write_u8(0x0f)?;
                key_type.serialize_abi(writer)?;
                value_type.serialize_abi(writer)
            }
            TypeSpec::Set { value_type } => {
                writer.write_u8(0x10)?;
                value_type.serialize_abi(writer)
            }
            TypeSpec::SizedArray { value_type, len } => {
                writer.write_u8(0x1a)?;
                value_type.serialize_abi(writer)?;
                writer.write_u8(*len)
            }
            TypeSpec::Option { value_type } => {
                writer.write_u8(0x12)?;
                value_type.serialize_abi(writer)
            }
            TypeSpec::AvlTreeMap {
                key_type,
                value_type,
            } => {
                writer.write_u8(0x19)?;
                key_type.serialize_abi(writer)?;
                value_type.serialize_abi(writer)
            }
        }
    }
}
