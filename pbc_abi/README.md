# Partisia Blockchain ABI

This crate contains
the [ABI-model](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#abi-binary-format)
definition describing the ABI structure and ABI serialization for a smart contract.

The ABI is used to describe all the interactions that can be made for a smart contract on the blockchain. These include:

- The type of the state for the contract.
- The number of functions, their type, name, and name and types for all arguments.
- All the named types of the contract.

The ABI can be used to e.g. build the rpc for an action or read the state of the contract, through the state and rpc
[binary formats](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html).

### CreateTypeSpec

The crate also contains the `CreateTypeSpec` trait. Data types can implement this trait to designate their corresponding
ABI type.