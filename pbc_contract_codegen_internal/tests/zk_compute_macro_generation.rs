//! Test macro expansion for the zk_compute macro.
use pbc_contract_codegen_internal::{parse_secret_input_type_from_function_return, SecretInput};
use proc_macro2::TokenStream;
use syn::token::RArrow;
use syn::{ReturnType, Type};

#[test]
fn secret_input_derives() {
    let s1 = SecretInput::None;
    assert_eq!(format!("{:?}", s1), format!("{:?}", s1));

    assert!(s1.eq(&s1));
}

#[test]
#[should_panic(expected = "Expected Path type at ZkInputDef type location.")]
fn parse_secret_return_not_path_type() {
    let ret_type = ReturnType::Type(
        RArrow::default(),
        Box::new(Type::Verbatim(TokenStream::new())),
    );

    parse_secret_input_type_from_function_return(&ret_type);
}

#[test]
#[should_panic(expected = "Expected AngleBracketed arguments in type.")]
fn parse_secret_return_no_angle_brackets() {
    let ret_type = syn::parse_str("-> ZkInputDef").unwrap();

    parse_secret_input_type_from_function_return(&ret_type);
}

#[test]
#[should_panic(expected = "Expected the last argument in angle brackets to be a type.")]
fn parse_secret_return_last_not_type() {
    let ret_type = syn::parse_str("-> ZkInputDef<'a>").unwrap();

    parse_secret_input_type_from_function_return(&ret_type);
}
