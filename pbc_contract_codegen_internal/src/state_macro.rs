//! Defines logic for handling the `#[state]` attribute.

use proc_macro::TokenStream;

use syn::Item;
use syn::__private::TokenStream2;

/// This handles the actual state struct AST with regards to it being a contract state.
///
/// It adds derives for `CreateTypeSpec` and `ReadWriteState` on the struct and generates
/// a couple of helper methods used by the ABI generation tool.
pub fn handle_state_macro(input: TokenStream) -> TokenStream {
    let original_state_struct: proc_macro2::TokenStream = input.clone().into();
    let struct_ast: Item = syn::parse(input).unwrap();
    let state_type = match struct_ast {
        Item::Struct(i) => i.ident,
        _ => unimplemented!("The state attribute is only valid for structs."),
    };

    let version_client_token = crate::version::create_abi_version_client();
    let version_binder_token = crate::version::create_abi_version_binder();
    let version_binder_zk_token = crate::version::create_abi_version_binder_zk();

    let result: TokenStream2 = quote! {
        use create_type_spec_derive::CreateTypeSpec as InternalDeriveCreateType;
        use read_write_state_derive::ReadWriteState as InternalDeriveReadWriteState;

        #[repr(C)]
        #[derive(InternalDeriveCreateType, InternalDeriveReadWriteState)]
        #original_state_struct

        #[cfg(feature = "abi")]
        #[doc = r###"
        ABI: Generate the ABI, write it to memory and return a pointer to said memory.

        **Deprecated**: use [`__abi_state_generate_to_ptr`] instead.

        To call this function two list of function pointers is needed.

        - `fn_list` list of pointers to functions that can generate abi for actions.
        - `ty_list` list of pointers to functions that can generate abi for named types. No longer used to generate ABI.

        _Input:_

        - `fn_len: u32` - The number of elements in `fn_list`.
        - `fn_list_ptr: *const usize` - Pointer to the location in memory where `fn_list` is stored.
        - `_ty_len: u32` - The number of elements in `ty_list`. The `ty_list` is not ued anymore and this input is ignored.
        - `_ty_list_ptr: *const usize` - Pointer to the location in memory where `ty_list` is stored. The `ty_list` is not ued anymore and this input is ignored.

        _Output:_

        See [`__abi_state_generate_to_ptr`].
        "###]
        // #[doc = "ABI: Generate the ABI, write it to memory and return a pointer to said memory."]
        // #[doc = "**Deprecated**: use `__abi_state_generate_to_ptr` instead."]
        #[no_mangle]
        #[automatically_derived]
        #[deprecated(since = "16.63.0", note = "Use __abi_state_generate_to_ptr instead")]
        pub unsafe extern "C" fn __abi_generate_to_ptr(
            fn_len: u32, fn_list_ptr: *const usize,
            _ty_len: u32, _ty_list_ptr: *const usize) -> u64 {
            __abi_state_generate_to_ptr(fn_len, fn_list_ptr)
        }

        #[cfg(feature = "abi")]
        #[doc = r###"
        ABI: Generate the ABI, write it to memory and return a pointer to said memory.
        To call this function a list of function pointers is needed.

        - `fn_list` list of pointers to functions that can generate abi for actions.

        _Input:_

        - `fn_len: u32` - The number of elements in `fn_list`.
        - `fn_list_ptr: *const usize` - Pointer to the location in memory where `fn_list` is stored.

        _Output:_

        A single `u32` value that is the concatenation of a pointer `ptr` to location of a buffer
        in memory, and the size `len` of that buffer. The length is placed in the most significant
        bits of the u32, and the pointer is placed in the least significant bits such that the
        output is `len | ptr`.

        The content of the buffer is the generated ABI.
        "###]
        #[no_mangle]
        #[automatically_derived]
        pub unsafe extern "C" fn __abi_state_generate_to_ptr(
            fn_len: u32,
            fn_list_ptr: *const usize) -> u64 {

            let version_client = #version_client_token;
            let version_binder = if __PBC_IS_ZK_CONTRACT {
                #version_binder_zk_token
            } else {
                #version_binder_token
            };

            let buffer = pbc_contract_common::abi::generate::generate_abi::<#state_type>(version_binder, version_client, fn_len, fn_list_ptr);
            let length = buffer.len() as u64;
            let pointer = buffer.as_ptr() as u64;
            std::mem::forget(buffer);

            (length << 32) | pointer
        }

        #[cfg(feature = "abi")]
        #[doc = r###"
        ABI: Expose a vector based malloc to the host.

        _Input:_

        - `size: u32` - The request size of the buffer to allocate.

        _Output:_

        A `u32` pointer to the buffer in memory.
        "###]
        #[no_mangle]
        #[automatically_derived]
        pub unsafe extern "C" fn __abi_malloc(size: u32) -> u32 {
            let allocated: Vec<u8> = Vec::with_capacity(size as usize);
            let ptr = allocated.as_ptr();
            std::mem::forget(allocated);
            ptr as u32
        }
    };

    // Convert the token from quote into the proc macro token stream
    result.into()
}
