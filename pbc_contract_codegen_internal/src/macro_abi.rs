use pbc_abi::abi_model::FunctionKind;
use syn::{Ident, ItemFn};

use crate::{get_arguments_names_and_types, SecretInput, TokenStream2};

fn fn_kind_snippet(fn_kind: FunctionKind) -> TokenStream2 {
    let fn_kind_name = format_ident!("{}", format!("{fn_kind:?}"));
    quote! {
        pbc_contract_common::abi::abi_model::FunctionKind::#fn_kind_name
    }
}

pub(crate) fn make_hook_abi_fn(
    fn_ast: &ItemFn,
    abi_fn_name: &Ident,
    fn_kind: FunctionKind,
    rpc_pos: usize,
    shortname_ident: TokenStream2,
    secret_type_input: SecretInput,
) -> TokenStream2 {
    let fn_name = &fn_ast.sig.ident.to_string();
    let (params, types) = get_arguments_names_and_types(fn_ast, rpc_pos).convert_to_tuple();
    let fn_kind_snippet = fn_kind_snippet(fn_kind);
    let add_secret_argument = match secret_type_input {
        SecretInput::None => quote! {},
        SecretInput::Some(secret_type) => {
            quote! {
                fn_abi.secret_argument::<#secret_type>(named_types, named_type_specs);
            }
        }
    };
    quote! {
        #[cfg(feature = "abi")]
        #[doc=concat!("ABI: Create ABI for [`", #fn_name, "`]")]
        #[automatically_derived]
        fn #abi_fn_name(
            named_types: &mut pbc_contract_common::abi::create_type_spec::NamedTypeLookup,
            named_type_specs: &mut pbc_contract_common::abi::create_type_spec::NamedTypeSpecs,
        ) -> pbc_contract_common::abi::abi_model::FnAbi {
            let mut fn_abi = pbc_contract_common::abi::abi_model::FnAbi::new(#fn_name.to_string(), #shortname_ident, #fn_kind_snippet);
            #(
                fn_abi.argument::<#types>(#params.to_string(), named_types, named_type_specs);
            )*
            #add_secret_argument
            fn_abi
        }
    }
}

pub fn make_hook_abi_fn_delegator(delegated_function_to_call: &Ident) -> proc_macro2::TokenStream {
    let function_name = format_ident!("__abi_fn_as_fn_ptr_{}", delegated_function_to_call);
    let result = quote! {
        #[cfg(feature = "abi")]
        #[no_mangle]
        #[doc = r###"
        ABI: Function pointer to fn ABI generating function.

        The specific name of this function must have the following format:

        `__abi_fn_as_fn_ptr___abi_fn_{kind_id}_{shortname}_{fn_name}`

        The `{kind_id}` is the id of the function kind as decimal, e.g. `2` for `#[action]` and `19`
        for `#[zk_on_compute_complete]`. The `{shortname}` is the shortname of the action,
        as hexadecimal. The `{fn_name}` is the declared function identifier in the contract.

        The function `_abi_fn_{kind_id}_{shortname}_{fn_name}` is not exported, but is used
        internally by [`__abi_state_generate_to_ptr`] to generate the ABI.

        _Input:_

        None.

        _Output:_

        A `*const ()` pointer to the function named `__abi_fn_{kind_id}_{shortname}_{fn_name}`,
        "###]
        #[automatically_derived]
        pub unsafe extern "C" fn #function_name() -> *const () {
            #delegated_function_to_call as *const ()
        }
    };
    result
}
