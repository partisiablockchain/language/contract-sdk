//! This submodule handles the wrapping of zero-knowledge macros in the public part of the contract.

use proc_macro::TokenStream;

use crate::macro_abi::{make_hook_abi_fn, make_hook_abi_fn_delegator};
use pbc_contract_common::address::Shortname;

use crate::{
    determine_names, variables_for_inner_call, wrap_function_for_export, FnKindCallProtocol,
    SecretInput, TokenStream2, WrappedFunctionKind,
};

/// Handles the wrapping of zero-knowledge macros in the public part of the contract.
pub fn handle_zk_macro(
    input: TokenStream,
    shortname_override: Option<Shortname>,
    shortname_override_type: Option<syn::Path>,
    export_symbol_base: &str,
    kind: &WrappedFunctionKind,
    shortname_in_export: bool,
    secret_type_input: SecretInput,
) -> TokenStream {
    let fn_ast: syn::ItemFn = syn::parse(input.clone()).unwrap();
    let names = determine_names(
        shortname_override,
        &fn_ast,
        export_symbol_base,
        shortname_in_export,
    );
    let docs = format!(
        "Serialization wrapper for contract zk hook `{}`.",
        names.fn_identifier
    );

    let invocation = variables_for_inner_call(&fn_ast, FnKindCallProtocol::Action, true);
    let mut result = wrap_function_for_export(
        &names.fn_identifier,
        &names.export_symbol,
        &docs,
        invocation,
        kind,
        Some(true),
    );

    let kind_id_and_shortname = format!("{:?}_{}", kind.fn_kind as u8, names.function_shortname);
    let abi_fn_name = format_ident!(
        "__abi_fn_{}_{}",
        &kind_id_and_shortname,
        &names.fn_identifier
    );
    let shortname_bytes = &names.function_shortname.bytes();
    let abi_fn = {
        let rpc_pos = kind.system_arguments;
        let shortname_ident = quote! {Some(vec![#(#shortname_bytes,)*])};

        make_hook_abi_fn(
            &fn_ast,
            &abi_fn_name,
            kind.fn_kind,
            rpc_pos,
            shortname_ident,
            secret_type_input,
        )
    };

    let shortname_fn: TokenStream2 = match (shortname_override, shortname_override_type) {
        (Some(_), Some(shortname_type)) => {
            let shortname_ident = format_ident!(
                "SHORTNAME_{}",
                names.fn_identifier.to_string().to_uppercase()
            );
            let shortname_bytes_raw = names.function_shortname.bytes_raw();
            quote! {
                const #shortname_ident: #shortname_type = #shortname_type::from_bytes_raw([#(#shortname_bytes_raw,)*]);
            }
        }
        _ => quote! {},
    };

    result.extend(TokenStream2::from(input));
    result.extend(abi_fn);
    result.extend(make_hook_abi_fn_delegator(&abi_fn_name));
    result.extend(shortname_fn);
    result.into()
}
