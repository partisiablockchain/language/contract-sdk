#![doc = include_str!("../README.md")]
#![recursion_limit = "128"]
extern crate proc_macro;
extern crate proc_macro2;
#[macro_use]
extern crate quote;
extern crate syn;

use sha2::{Digest, Sha256};
use std::cmp::max;
use std::collections::HashMap;

use pbc_abi::abi_model::FunctionKind;
use quote::ToTokens;
use syn::__private::TokenStream2;
use syn::{
    AttributeArgs, FnArg, GenericArgument, Ident, Lit, Meta, NestedMeta, PatType, PathArguments,
    ReturnType, Type, TypeArray, TypePath,
};

use crate::tokenized::{ArgumentList, InstantiableArgument, TokenizedInvocation};
use pbc_contract_common::address::Shortname;

pub mod action_macro;
pub mod callback_macro;
pub mod init_macro;
mod macro_abi;
pub mod state_macro;
mod tokenized;
pub mod upgrade_macro;
mod version;
pub mod zk_compute_macro;
pub mod zk_macro;

/// Parses the attributes of a macro into a map from attribute names to a `Literal`.
///
/// ### Parameters:
///
/// * `args`: [`AttributeArgs`] - the args to be parsed.
///
/// * `valid_names`: [`Vec<String>`] - valid names of attributes. Panics if an attribute name not
///   in valid_names is present in args.
///
/// * `required_names`: [`Vec<String>`] - required names. Panics if any of the attribute names is
///   not present in args.
///
/// ### Returns
///
/// A map from attribute name to their value.
pub fn parse_attributes(
    args: AttributeArgs,
    valid_names: Vec<String>,
    required_names: Vec<String>,
) -> HashMap<String, Lit> {
    let metas = args.iter().map(|nested_meta| match nested_meta {
        NestedMeta::Meta(meta) => meta,
        _ => panic!("Invalid attribute: {}", nested_meta.to_token_stream()),
    });

    let mut result = HashMap::new();

    for meta in metas {
        match meta {
            Meta::NameValue(pair) => {
                let name = pair
                    .path
                    .get_ident()
                    .map(|ident| ident.to_string())
                    .unwrap_or_else(|| "INVALID".to_string());

                if !valid_names.contains(&name) {
                    panic!(
                        "Invalid attribute found, valid attributes are: {}",
                        valid_names.join(", ")
                    );
                }
                result.insert(name, pair.lit.clone());
            }
            _ => panic!("Invalid attribute: {}", meta.to_token_stream()),
        }
    }

    for required_name in required_names {
        assert!(
            result.contains_key(&required_name),
            "Required attribute '{required_name}' is missing",
        );
    }

    result
}

/// Gets the shortname attribute of the arguments and if present parses it into a `Shortname`.
/// Panics if the attribute is not a valid shortname literal.
///
/// ### Parameters:
///
/// * `args`: &[HashMap<String, Lit>] - parsed attributes of a macro.
///
/// ### Returns
/// Some of the parsed shortname if present in args, None if it is not present.
pub fn parse_shortname_override(args: &HashMap<String, Lit>) -> Option<Shortname> {
    args.get("shortname").map(|lit: &Lit| match lit {
        Lit::Int(lit_int) if is_hex_literal(lit_int) => {
            let x: u64 = lit_int
                .base10_parse()
                .expect("Invalid shortname, expecting a u32 hex literal");
            parse_leb128_bytes_to_shortname(x.to_be_bytes())
                .expect("Invalid shortname, should be LEB128 encoded")
        }
        _ => panic!(
            "Invalid shortname, expecting a u32 hex literal, but got: {}",
            lit.to_token_stream()
        ),
    })
}

/// Gets the zk attribute of the arguments and if present parses it into a `bool`.
/// Panics if the attribute is not a valid boolean literal.
///
/// ### Parameters:
///
/// * `args`: &[HashMap<String, Lit>] - parsed attributes of a macro.
///
/// ### Returns
/// `true` if the zk attribute is present and set to true, `false` otherwise.
pub fn parse_zk_argument(args: &HashMap<String, Lit>) -> bool {
    let zk = args.get("zk").map(|lit: &Lit| match lit {
        Lit::Bool(lit_bool) => lit_bool.value,
        _ => panic!(
            "Invalid zk attribute, expecting a boolean literal, but got: {}",
            lit.to_token_stream()
        ),
    });
    zk.unwrap_or(false)
}

/// Describes the kind of secret input argument have been defined.
#[derive(Debug, Eq, PartialEq)]
pub enum SecretInput {
    /// The action does not require a secret input.
    None,
    /// The type given by the string name is given.
    Some(syn::Type),
}

/// Whether the given integer literal is hex formatted or not.
fn is_hex_literal(lit: &syn::LitInt) -> bool {
    let token_text = format!("{}", lit.token());
    token_text.starts_with("0x") && token_text.chars().skip(2).all(|c| c.is_ascii_hexdigit())
}

/// Parses the given bytes to a shortname.
///
/// If the bytes doesn't represent a shortname it returns an error.
fn parse_leb128_bytes_to_shortname<const N: usize>(bytes: [u8; N]) -> Result<Shortname, String> {
    let idx_first_non_zero = bytes.iter().position(|&x| x != 0).unwrap_or(N - 1);
    let vec_bytes: Vec<_> = bytes.iter().skip(idx_first_non_zero).copied().collect();
    Shortname::from_be_bytes(&vec_bytes)
}

/// Describes how to wrap a function as an action.
pub struct WrappedFunctionKind {
    /// Whether the wrapped function will output state and events.
    pub return_state_and_events: bool,
    /// What other kinds of types the function can return. The function doesn't need to return them
    /// all, only [`WrappedFunctionKind::min_num_return_types`] of them.
    pub additional_return_types: Vec<(TokenStream2, Ident)>,
    /// The minimum allowed number of results.
    pub min_num_return_types: usize,
    /// Number of "system" arguments before RPC arguments occur
    pub system_arguments: usize,
    /// The [`FunctionKind`] of the wrapped function.
    pub fn_kind: FunctionKind,
    /// If `false`, RPC arguments are disallowed, and only system arguments must occur.
    pub allow_rpc_arguments: bool,
    /// Whether the function kind should return the state type witness in its own section. This is only used for
    /// `upgrade_is_allowed`.
    pub return_state_type_witness: bool,
    /// Whether to read a `TypeWitness` from the RPC stream and validate it against the contract
    /// state argument's `TypeWitness`.
    ///
    /// The `TypeWitness` read is placed between the Context and the State argument, to allow the
    /// macro to validate before the state is read in. If the State argument's type is incompatible
    /// with the read state it will be caught by the `TypeWitness` validation, instead of
    /// potentially giving an uninformative serialization error.
    pub read_and_validate_type_witness: bool,
}

impl WrappedFunctionKind {
    /// Configuration of a public contract hook.
    ///
    /// - `system_arguments`: Number of arguments that are passed from the binder.
    ///   Arguments beyond these will be included in the ABI.
    /// - `fn_kind`: The function kind of the hook.
    /// - `zk_state_in_arguments`: Whether the zk_state should be included in the system arguments.
    fn public_contract_hook_kind(
        system_arguments: usize,
        fn_kind: FunctionKind,
        zk_state_in_arguments: bool,
    ) -> Self {
        let additional_return_types = if zk_state_in_arguments {
            // With Zk we only need state, events optional, and ZkStateChange optional.
            vec![(
                quote! { Vec<pbc_contract_common::zk::ZkStateChange> },
                format_ident!("write_zk_state_change"),
            )]
        } else {
            // Without Zk we only need state, events optional.
            vec![]
        };
        Self {
            return_state_and_events: true,
            additional_return_types,
            min_num_return_types: 1,
            system_arguments: system_arguments + usize::from(zk_state_in_arguments),
            fn_kind,
            allow_rpc_arguments: true,
            return_state_type_witness: false,
            read_and_validate_type_witness: false,
        }
    }

    /// Expected return types of the wrapped function kind.
    fn return_types(&self) -> Vec<TokenStream2> {
        let mut return_types = vec![];
        if self.return_state_and_events {
            return_types.push(quote! { _ });
            return_types.push(quote! { Vec<pbc_contract_common::events::EventGroup> });
        }
        for (typ, _) in &self.additional_return_types {
            return_types.push(typ.clone());
        }
        return_types
    }

    /// Expected write methods of the wrapped function kind.
    fn write_methods(&self) -> Vec<Ident> {
        let mut methods = vec![];
        if self.return_state_and_events {
            methods.push(format_ident!("write_state"));
            methods.push(format_ident!("write_events"));
        }
        for (_, method) in &self.additional_return_types {
            methods.push(method.clone());
        }
        methods
    }
}

/// Wraps an invocation function by generating a new exported function.
///
/// The wrapping is what allows contract developers to write strongly typed contracts without
/// having to worry about the [serialization formats used by the WASM
/// binders](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html)
/// and [Partisia Blockchain in
/// general](https://partisiablockchain.gitlab.io/documentation/smart-contracts/transaction-binary-format.html).
///
/// The wrapping function works by:
///
/// * Direct invocation by one of the WASM contract binders.
/// * Accepting contract state, contract context, and invocation arguments in serialized format.
/// * Deserializes arguments using [`pbc_traits::ReadRPC`], and contract state using [`pbc_traits::ReadWriteState`].
/// * Invokes the wrapped strongly typed function with the deserialized inputs.
/// * The resulting outputs (contract state, invocations, zk events) are serialized to a [`ContractResultBuffer`](pbc_contract_common::ContractResultBuffer).
/// * The pointer location of the [`ContractResultBuffer`](pbc_contract_common::ContractResultBuffer) is returned by the wrapping function.
/// * When the wrapping function returns control is returned to the invoking binder.
/// * The invoking binder will use the produced pointer to locate [`ContractResultBuffer`](pbc_contract_common::ContractResultBuffer) in order
///   to read the update contract state and contract events.
///
/// ### Parameters:
/// * `fn_identifier`: [`struct@Ident`], The identifier for the function to wrap.
/// * `export_symbol`: [`struct@Ident`], The identifier for the wrapper function.
/// * `docs`: &[str], The documentation to insert above the wrapper function.
/// * `arguments`: [`TokenizedInvocation`], The arguments for the wrapped function.
/// * `function_kind`: [`WrappedFunctionKind`], The function kind, e.g. action or callback.
/// * `check_zk_contract`: `Option<[bool]>`, If `Some(true)` asserts that the contract is a zk-contract.
///     if `Some(false)` asserts that the contract is a public contract, otherwise no check is performed.
///
/// ### Returns:
/// The [TokenStream2] for the wrapper function.
#[allow(clippy::too_many_arguments)]
fn wrap_function_for_export(
    fn_identifier: &Ident,
    export_symbol: &Ident,
    docs: &str,
    arguments: TokenizedInvocation,
    function_kind: &WrappedFunctionKind,
    check_zk_contract: Option<bool>,
) -> TokenStream2 {
    // Check that function is well-formed
    if arguments.num_params() > function_kind.system_arguments && !function_kind.allow_rpc_arguments
    {
        panic!(
            "Functions annotated with this macro must have at most {} arguments",
            function_kind.system_arguments,
        );
    }
    let fn_ident = fn_identifier.to_string();
    let kind = format!("{:?}", function_kind.fn_kind).to_lowercase();
    let zk_check_stream = if let Some(zk_argument) = check_zk_contract {
        let error_message = if zk_argument {
            format!("{fn_ident} cannot be zk if the init function is not zk. Consider using #[init(zk = true)]")
        } else {
            format!("{fn_ident} cannot be non-zk if the init function is zk. Consider using #[{kind}(zk = true)]")
        };
        check_valid_zk_contract(zk_argument, error_message)
    } else {
        TokenStream2::new()
    };

    let reader = format_ident!("input_reader");
    let rpc_read = &arguments.param_instantiation_expr();
    let rpc_param_names = &arguments.param_names();
    let ctx_expression = arguments.context.expression;

    let mut invoke_read_expr: Vec<TokenStream2> = Vec::new();
    let mut invoke_vars: Vec<Ident> = Vec::new();
    if let Some(callback_context) = arguments.callback_context {
        invoke_vars.push(callback_context.variable_name());
        invoke_read_expr.push(callback_context.expression);
    }

    // Create identifier that is difficult to accidentally collide with, and extremely obvious
    // when deliberately colliding.
    let rust_visible_symbol = format_ident!("__pbc_autogen__{}_wrapped", fn_identifier);

    let mut result_types = function_kind.return_types();
    result_types.truncate(max(
        arguments.result_types.len(),
        function_kind.min_num_return_types,
    ));

    if arguments.result_types.len() < function_kind.min_num_return_types {
        panic!(
            "Functions annotated with this macro must have at least {} return values, but had only {}",
            function_kind.min_num_return_types, arguments.result_types.len()
        );
    }

    let result_tuple_indice = (0..result_types.len()).map(syn::Index::from);

    let write_methods: Vec<_> = function_kind.write_methods();
    let indices_with_methods: Vec<_> = write_methods.iter().zip(result_tuple_indice).collect();

    let is_single = indices_with_methods.len() == 1;

    let mut write_statements: Vec<_> = indices_with_methods
        .iter()
        .map(|(write_method, result_idx)| {
            let path = if is_single {
                quote! { result }
            } else {
                quote! { result.#result_idx }
            };
            quote! { result_buffer.#write_method(#path); }
        })
        .collect();

    let mut read_and_validate_type_witness = quote! {};

    if let Some(state) = arguments.state {
        // System argument: Contract state
        invoke_vars.push(state.variable_name());
        invoke_read_expr.push(state.expression);

        // Return value: State type witness
        let state_type = state.arg_type.to_token_stream();
        if function_kind.return_state_type_witness {
            write_statements.push(quote! { result_buffer.write_state_witness::<#state_type>(); })
        }

        // System argument: State type witness
        if function_kind.read_and_validate_type_witness {
            read_and_validate_type_witness = quote! {
                let __rpc_type_witness = <pbc_contract_common::upgrade::TypeWitness as pbc_traits::ReadRPC>::rpc_read_from(&mut #reader);
                let __state_argument_type_witness = pbc_contract_common::upgrade::TypeWitness::of::<#state_type>();
                assert_eq!(__rpc_type_witness, __state_argument_type_witness, "State type witness from RPC was not consistent with that from the state object. This indicates that the definitions for the old contract state (in the new contract) does not match the definitions as they were in the old contract!");
            }
        }
    }

    // Emit the wrapper
    quote! {
        #[allow(clippy::not_unsafe_ptr_arg_deref)]
        #[doc = #docs]
        #[no_mangle]
        #[automatically_derived]
        #[export_name = stringify!(#export_symbol)]
        pub extern "C-unwind" fn #rust_visible_symbol(
            input_buf_ptr: *mut u8, input_buf_len: usize,
        ) -> u64 {
            #[cfg(all(not(feature = "abi"), any(target_arch = "wasm32", doc)))]
            pbc_lib::exit::override_panic();
            #zk_check_stream

            // Setup reader and read context.
            let mut #reader = unsafe { std::slice::from_raw_parts(input_buf_ptr, input_buf_len) };
            let context = #ctx_expression;

            // Run type witness validate before reading state to give more intelligeble error messages.
            // This only happens when the state type validation is required.
            #read_and_validate_type_witness

            // Read state and RPC arguments
            #(let #invoke_vars = #invoke_read_expr;)*
            #rpc_read
            assert!(#reader.is_empty(), "Input data too long; {} bytes remaining", #reader.len());

            // Execute inner code
            let result: (#(#result_types),*) = #fn_identifier(context, #(#invoke_vars,)* #(#rpc_param_names,)*);

            // Produce output, based on the specified return values.
            let mut result_buffer = pbc_contract_common::ContractResultBuffer::new();
            #(#write_statements)*

            unsafe { result_buffer.finalize_result_buffer() }
        }
    }
}

/// The
/// [FnKind](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html)'s
/// call protocol, dictating how the annotated function must format it's function singature.
pub(crate) enum FnKindCallProtocol {
    /// Calling protocol for contract initialization, identical to [`FnKindCallProtocol::Action`] with no existing contract state.
    Init,
    /// Calling protocol for normal invocations, with a context, state and rpc arguments.
    Action,
    /// Calling protocol for callback invocations, with a context, callback context, state and rpc arguments.
    Callback,
}

/// Various names for a function.
pub(crate) struct Names {
    fn_identifier: Ident,
    function_shortname: Shortname,
    export_symbol: Ident,
}

/// Determines various names to be used to address the given [`syn::ItemFn`].
pub(crate) fn determine_names(
    shortname_def: Option<Shortname>,
    fn_ast: &syn::ItemFn,
    export_symbol_base: &str,
    shortname_in_symbol: bool,
) -> Names {
    let fn_identifier: &Ident = &fn_ast.sig.ident;

    let function_shortname = shortname_def.unwrap_or(name_to_shortname(&fn_identifier.to_string()));
    let export_symbol = if shortname_in_symbol {
        format_ident!("{}_{}", export_symbol_base, function_shortname.to_string())
    } else {
        format_ident!("{}", export_symbol_base)
    };

    Names {
        fn_identifier: fn_identifier.clone(),
        function_shortname,
        export_symbol,
    }
}

/// Create a shortname from the given function name.
/// The shortname consists of the first 4 bytes of the SHA256 hash of the name.
fn name_to_shortname(raw_name: &str) -> Shortname {
    let mut digest = Sha256::new();
    Digest::update(&mut digest, raw_name.as_bytes());
    let output = digest.finalize();
    let first_four = output.chunks(4).next().unwrap();
    let shortname_u32 = u32::from_be_bytes(first_four.try_into().unwrap());
    Shortname::from_u32(shortname_u32)
}

/// Determines the concrete return types of a function, from the given type. Flattens tuples.
fn extract_concrete_return_types(t: &Type) -> Vec<Type> {
    match t {
        Type::Tuple(syn::TypeTuple { elems, .. }) => elems.iter().cloned().collect(),
        Type::Paren(syn::TypeParen { elem, .. }) => extract_concrete_return_types(elem),
        some_type => {
            vec![some_type.clone()]
        }
    }
}

/// Determines the vector of returned types from the given function. Flattens tuples.
pub fn extract_concrete_return_types_from_function_return(return_type: &ReturnType) -> Vec<Type> {
    match return_type {
        syn::ReturnType::Default => vec![],
        syn::ReturnType::Type(_, t) => extract_concrete_return_types(t),
    }
}

/// Determines the variable data to be used with [`wrap_function_for_export`].
fn variables_for_inner_call(
    item: &syn::ItemFn,
    call_protocol: FnKindCallProtocol,
    require_zk_state: bool,
) -> TokenizedInvocation {
    // Constants by FnKindCallProtocol
    let expected_min_arguments = match call_protocol {
        FnKindCallProtocol::Init => 1,
        FnKindCallProtocol::Action => 2,
        FnKindCallProtocol::Callback => 3,
    } + usize::from(require_zk_state);

    // Parse
    let mut item_iterator = item.sig.inputs.iter();
    assert!(
        item_iterator.len() >= expected_min_arguments,
        "Functions annotated with this macro must have at least {} arguments, but had only {}",
        expected_min_arguments,
        item_iterator.len()
    );

    let ctx = read_arguments_for_instantiation(item_iterator.next().unwrap(), false);
    let callback_context = match call_protocol {
        FnKindCallProtocol::Callback => {
            let token = item_iterator
                .next()
                .expect("Callbacks must possess a CallbackContext argument");
            let callback = read_arguments_for_instantiation(token, false);
            Some(callback)
        }
        _ => None,
    };

    let state = match call_protocol {
        FnKindCallProtocol::Action | FnKindCallProtocol::Callback => {
            let token = item_iterator
                .next()
                .expect("Action and callbacks must possess a State argument");
            let state_tmp = read_arguments_for_instantiation(token, true);
            Some(state_tmp)
        }
        _ => None,
    };

    let zk_state = if require_zk_state {
        let token = item_iterator
            .next()
            .expect("Hooks in Zk contracts must possess a ZkState argument");
        let state_tmp = read_arguments_for_instantiation(token, false);
        Some(state_tmp)
    } else {
        None
    };

    let result_types: Vec<Type> =
        extract_concrete_return_types_from_function_return(&item.sig.output);

    // Parse RPC params
    let rpc_params = item_iterator
        .map(|token| read_arguments_for_instantiation(token, false))
        .collect();
    TokenizedInvocation::new(
        ctx,
        callback_context,
        state,
        zk_state,
        rpc_params,
        result_types,
    )
}

/// Read the arguments from the given function AST.
///
/// * `item` - the parsed function
/// * `skip` - number of leading items to skip
fn get_arguments_names_and_types(item: &syn::ItemFn, skip: usize) -> ArgumentList {
    let mut arguments = ArgumentList::new();
    for token in item.sig.inputs.iter() {
        let pat = get_parameter_type_pattern(token);
        let identifier = pat.pat.to_token_stream();
        let ty = pat.ty.to_token_stream();
        arguments.push(identifier, ty);
    }

    arguments.split_off(skip)
}

/// Determines the parameter type for the given function argument.
fn get_parameter_type_pattern(token: &FnArg) -> &PatType {
    match token {
        FnArg::Receiver(_) => {
            panic!("Contract functions must be bare functions.")
        }
        FnArg::Typed(pat) => pat,
    }
}

fn read_arguments_for_instantiation(token: &FnArg, is_state: bool) -> InstantiableArgument {
    let pat = get_parameter_type_pattern(token);
    let var_name = match &*pat.pat {
        syn::Pat::Ident(x) => x.ident.to_string(),
        pat => panic!("Unsupported argument pattern: {}", pat.to_token_stream()),
    };

    let ty = *(pat.ty.clone());
    match ty {
        Type::Path(path) => {
            let expr = generate_read_from_path_expression(&path, is_state);
            InstantiableArgument::new(&var_name, &Type::Path(path), expr)
        }

        Type::Tuple(_) => {
            panic!("Unsupported tuple type");
        }

        Type::Array(array) => {
            let expr = generate_read_from_array_expression(&array, is_state);
            InstantiableArgument::new(&var_name, &Type::Array(array), expr)
        }

        Type::ImplTrait(_) => {
            panic!("Unsupported impl trait type");
        }

        Type::Reference(_) => {
            panic!("Unsupported reference type");
        }

        _ => {
            panic!("Unsupported argument type.")
        }
    }
}

/// Generate instantiating expressions for the given type.
///
/// This is a part of a macro and assumes that `input_buf` is in scope where the macro is called
/// and that said ident represents an instance of std::io::Read.
///
/// * `path` - the AST type to generate an instantiating expression for
/// * `is_state` - whether we are using [`pbc_traits::ReadWriteState`] or [`pbc_traits::ReadRPC`]
fn generate_read_from_path_expression(path: &TypePath, is_state: bool) -> TokenStream2 {
    let (trait_type, read_from) = if is_state {
        (quote!(pbc_traits::ReadWriteState), quote!(state_read_from))
    } else {
        (quote!(pbc_traits::ReadRPC), quote!(rpc_read_from))
    };
    let type_name = match path.path.get_ident() {
        Some(ident) => quote! {#ident},
        None => path.into_token_stream(),
    };
    quote! {<#type_name as #trait_type>::#read_from(&mut input_reader);}
}

/// Generate instantiating expressions for the given array.
///
/// This is a part of a macro and assumes that `reader_ident` is in scope where the macro is called
/// and that said ident represents an instance of std::io::Read.
///
/// * `reader_ident` - the reader variable/expression
/// * `path` - the AST type to generate an instantiating expression for
/// * `is_state` - whether we are using [`pbc_traits::ReadWriteState`] or [`pbc_traits::ReadRPC`]
fn generate_read_from_array_expression(array: &TypeArray, is_state: bool) -> TokenStream2 {
    let (trait_type, read_from) = if is_state {
        (quote!(pbc_traits::ReadWriteState), quote!(state_read_from))
    } else {
        (quote!(pbc_traits::ReadRPC), quote!(rpc_read_from))
    };

    let array_tokens = array.to_token_stream();
    quote! { <#array_tokens as #trait_type>::#read_from(&mut input_reader); }
}

fn check_valid_zk_contract(zk_argument: bool, error_message: String) -> TokenStream2 {
    quote! {
        const _ : () = assert!(#zk_argument == __PBC_IS_ZK_CONTRACT, #error_message);
    }
}

/// Parses the type of the secret variable used in `zk_on_secret_input` functions,
/// from `ZkInputDef  in the return type of the function signature.
///
/// This function relies heavily on the structure of the annotated [zk_on_secret_input](https://partisiablockchain.gitlab.io/language/contract-sdk/pbc_contract_codegen/attr.zk_on_secret_input.html) function.
///
/// Panics if any of the following conditions are not satisfied:
/// * The function return type is a populated tuple type.
/// * The last type of the tuple type is of type [`Type::Path`].
/// * The last segment of the [`Type::Path`] type is "ZkInputDef".
/// * The last segment must include [`PathArguments::AngleBracketed`] arguments.
/// * The last [`PathArguments::AngleBracketed`] argument must be a type.
///   If these conditions are satisfied, the last type argument is returned, e.g.
///   *T* in the expression `path::ZkInputDef<_, T>`
pub fn parse_secret_input_type_from_function_return(func_return: &ReturnType) -> SecretInput {
    let types = extract_concrete_return_types_from_function_return(func_return);

    let input_def_type = types.last().unwrap();
    let secret_type = match input_def_type {
        Type::Path(TypePath { path, .. }) => {
            let input_def_segment = path.segments.last().unwrap();
            assert_eq!(input_def_segment.ident.to_string(), "ZkInputDef");
            match &input_def_segment.arguments {
                PathArguments::AngleBracketed(syn::AngleBracketedGenericArguments {
                    args, ..
                }) => match args.last().unwrap() {
                    GenericArgument::Type(t) => t.clone(),
                    _ => panic!("Expected the last argument in angle brackets to be a type."),
                },
                _ => {
                    panic!("Expected AngleBracketed arguments in type.")
                }
            }
        }
        _ => {
            panic!("Expected Path type at ZkInputDef type location.")
        }
    };

    SecretInput::Some(secret_type)
}

/// Gets the secret_type attribute of and parses it into a `SecretInput` enum containing the
/// secret input type.
/// Panics if the attribute is not a string literal.
///
/// ### Parameters:
///
/// * `args`: [HashMap<String, Lit>] - parsed attributes of a macro.
///
/// ### Returns
/// Some of the secret input type if present in args. Default Sbi32 if not present.
pub fn parse_secret_input_type_from_attributes(args: HashMap<String, Lit>) -> SecretInput {
    match args.get("secret_type") {
        None => SecretInput::None,
        Some(Lit::Str(lit_str)) => SecretInput::Some(lit_str.parse().unwrap()),
        Some(err) => panic!(
            "Invalid type, expecting a string literal, but got: {}",
            err.to_token_stream()
        ),
    }
}
