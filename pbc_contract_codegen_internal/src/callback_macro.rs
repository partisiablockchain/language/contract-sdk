//! Defines logic for handling the `#[callback]` attribute.

use pbc_abi::abi_model::FunctionKind;
use proc_macro::TokenStream;

use crate::macro_abi::{make_hook_abi_fn, make_hook_abi_fn_delegator};
use pbc_contract_common::address::Shortname;

use crate::{
    determine_names, variables_for_inner_call, wrap_function_for_export, FnKindCallProtocol,
    SecretInput, TokenStream2, WrappedFunctionKind,
};

/// Defines logic for handling the `#[callback]` attribute.
pub fn handle_callback_macro(
    input: TokenStream,
    shortname_override: Option<Shortname>,
    zk_argument: bool,
) -> TokenStream {
    let fn_ast: syn::ItemFn = syn::parse(input.clone()).unwrap();

    let names = determine_names(shortname_override, &fn_ast, "callback", true);

    let docs = format!(
        "Serialization wrapper for contract callback `{}`.",
        names.fn_identifier
    );

    let kind =
        WrappedFunctionKind::public_contract_hook_kind(3, FunctionKind::Callback, zk_argument);

    let invocation = variables_for_inner_call(&fn_ast, FnKindCallProtocol::Callback, zk_argument);
    let mut result = wrap_function_for_export(
        &names.fn_identifier,
        &names.export_symbol,
        &docs,
        invocation,
        &kind,
        Some(zk_argument),
    );

    let kind_id_and_shortname = format!("{:?}_{}", kind.fn_kind as u8, names.function_shortname);
    let abi_fn_name = format_ident!(
        "__abi_fn_{}_{}",
        &kind_id_and_shortname,
        &names.fn_identifier
    );
    let shortname_bytes = &names.function_shortname.bytes();
    let abi_fn = {
        let rpc_pos = kind.system_arguments;
        let shortname_ident = quote! {Some(vec![#(#shortname_bytes,)*])};
        make_hook_abi_fn(
            &fn_ast,
            &abi_fn_name,
            kind.fn_kind,
            rpc_pos,
            shortname_ident,
            SecretInput::None,
        )
    };

    let shortname_fn: TokenStream2 = {
        let shortname = format_ident!(
            "SHORTNAME_{}",
            names.fn_identifier.to_string().to_uppercase()
        );
        let shortname_bytes_raw = names.function_shortname.bytes_raw();
        quote! {const #shortname: pbc_contract_common::address::ShortnameCallback = pbc_contract_common::address::ShortnameCallback::from_bytes_raw([#(#shortname_bytes_raw,)*]);}
    };

    result.extend(TokenStream2::from(input));
    result.extend(abi_fn);
    result.extend(make_hook_abi_fn_delegator(&abi_fn_name));
    result.extend(shortname_fn);
    result.into()
}
