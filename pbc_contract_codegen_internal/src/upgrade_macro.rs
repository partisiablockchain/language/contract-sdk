//! Defines the internal logic for handling the `#[upgrade_is_allowed]` and `#[upgrade]` attribute macros.
//!
//! This module only covers the internal codegens systems. See documentation in `pbc_contract_codegen` for how to use the macros.

use pbc_abi::abi_model::FunctionKind;
use proc_macro::TokenStream;

use crate::{
    determine_names, variables_for_inner_call, wrap_function_for_export, FnKindCallProtocol,
    TokenStream2, WrappedFunctionKind,
};

/// Handles the `#[upgrade_is_allowed]` macro, which is called when a user is attempting to upgrade
/// the contract to some new code. The Rust contract should perform permission checks, and then
/// return a [`bool`] to indicate whether to allow or disallow the upgrade.
///
/// Expects the following arguments from the binder:
///
/// - Context
/// - Contract state
/// - Hashes of the current byte-code.
/// - Hashes of the upgraded byte-code.
///
/// All of the above arguments are passed along to the Rust contract.
///
/// Macro-produced wrapper will return the following results:
///
/// - Upgrade allowed? Directly returned from the contract. Single [`bool`], indicating whether to allow the
///   upgrade or not. Will be packed into the `UPGRADE_IS_ALLOWED` section.
/// - State `TypeWitness`. This is not visible to the Rust contract. Packed into the `STATE_TYPE_WITNESS` section.
///
/// The Rust contract is allowed to panic, which is treated as a refusal to allow
/// the upgrade.
pub fn handle_upgrade_is_allowed(input: TokenStream) -> TokenStream {
    let zk_argument = false;

    let fn_ast: syn::ItemFn = syn::parse(input.clone()).unwrap();
    let names = determine_names(None, &fn_ast, "upgrade_is_allowed", false);

    let mut invocation = variables_for_inner_call(&fn_ast, FnKindCallProtocol::Action, zk_argument);

    if invocation.rpc_params.len() != 3 {
        panic!("upgrade_is_allowed must have five arguments: (context, state, current_hashes, upgrade_to_hashes, upgrade_rpc)");
    }

    // Set type of parsed invocation to enforce the correct type arguments.
    invocation.rpc_params[0].arg_type =
        syn::parse_str("pbc_contract_common::upgrade::ContractHashes").unwrap();
    invocation.rpc_params[1].arg_type =
        syn::parse_str("pbc_contract_common::upgrade::ContractHashes").unwrap();
    invocation.rpc_params[2].arg_type = syn::parse_str("std::vec::Vec<u8>").unwrap();

    let docs = format!(
        "Serialization wrapper for contract upgrade checker [`{}`].",
        names.fn_identifier
    );

    let additional_return_types = vec![(quote! { bool }, format_ident!("write_upgrade_section"))];

    let kind = WrappedFunctionKind {
        return_state_and_events: false,
        additional_return_types,
        min_num_return_types: 1,
        system_arguments: 5,
        fn_kind: FunctionKind::UpgradeIsAllowed,
        allow_rpc_arguments: false,
        return_state_type_witness: true,
        read_and_validate_type_witness: false,
    };

    let mut result = wrap_function_for_export(
        &names.fn_identifier,
        &names.export_symbol,
        &docs,
        invocation,
        &kind,
        None,
    );

    result.extend(TokenStream2::from(input));
    result.into()
}

/// Handles the `#[upgrade]` macro, which is called on an upgraded contract, to upgrade the
/// previous contract state to the new contract state. At this point it is too late to perform
/// permission checks; the contract should just map from the original state to the new state.
///
/// Expects the following arguments from the binder:
///
/// - Context
/// - Type witness of the previous contract state. **This is not visible to the Rust Contract**.
/// - The previous contract state
///
/// The context and the previous contract state is passed along to the Rust contract. The
/// `TypeWitness` that is pass along, will be validated compared to the `TypeWitness` of the
/// previous contract state as defined by the upgraded contract, to ensure that the upgraded
/// contract have a correct understanding of how to upgrade.
///
/// Macro-produced wrapper will return the following results:
///
/// - Updated state.
///
/// If the Rust contract panics, the entire upgrade will be rolled back.
pub fn handle_upgrade(input: TokenStream) -> TokenStream {
    let zk_argument = false;
    let fn_ast: syn::ItemFn = syn::parse(input.clone()).unwrap();
    let names = determine_names(None, &fn_ast, "upgrade", false);
    let invocation = variables_for_inner_call(&fn_ast, FnKindCallProtocol::Action, zk_argument);

    let docs = format!(
        "Serialization wrapper for contract upgrade [`{}`].",
        names.fn_identifier
    );

    if !invocation.rpc_params.is_empty() {
        panic!("upgrade does not support RPC arguments");
    }

    let kind = WrappedFunctionKind {
        return_state_and_events: true,
        additional_return_types: vec![],
        min_num_return_types: 1,
        system_arguments: 2,
        fn_kind: FunctionKind::UpgradeFromState,
        allow_rpc_arguments: false,
        return_state_type_witness: false,
        read_and_validate_type_witness: true,
    };

    let mut result = wrap_function_for_export(
        &names.fn_identifier,
        &names.export_symbol,
        &docs,
        invocation,
        &kind,
        None,
    );

    result.extend(TokenStream2::from(input));
    result.into()
}
