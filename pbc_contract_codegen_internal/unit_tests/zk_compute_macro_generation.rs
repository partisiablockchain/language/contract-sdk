//! Test macro expansion for the zk_compute macro.
use crate::zk_compute_macro::metadata_parameter_and_serialization;
use crate::{parse_secret_input_type_from_function_return, SecretInput};
use proc_macro2::TokenStream;
use syn::token::RArrow;
use syn::{ReturnType, Type};

#[test]
pub fn metadata_slice_parameter() {
    let metadata_type = quote! { T };
    let num_outputs = None; // expect slice
    let (metadata_parameter, metadata_serialization, metadata_generic) =
        metadata_parameter_and_serialization(&metadata_type, num_outputs);
    let unwrapped_serialization = metadata_serialization.unwrap();

    assert_eq!(
        format!("{metadata_parameter}"),
        "output_metadata : & [& T] ,"
    );
    assert_eq!(format!("{unwrapped_serialization}"), "output_metadata");
    assert_eq!(
        format!("{metadata_generic}"),
        "< T : pbc_traits :: ReadWriteState >"
    );
}

#[test]
fn secret_input_derives() {
    let s1 = SecretInput::None;
    assert_eq!(format!("{:?}", s1), format!("{:?}", s1));

    assert!(s1.eq(&s1));
}

#[test]
#[should_panic(expected = "Expected Path type at ZkInputDef type location.")]
fn parse_secret_return_not_path_type() {
    let ret_type = ReturnType::Type(
        RArrow::default(),
        Box::new(Type::Verbatim(TokenStream::new())),
    );

    parse_secret_input_type_from_function_return(&ret_type);
}

#[test]
#[should_panic(expected = "Expected AngleBracketed arguments in type.")]
fn parse_secret_return_no_angle_brackets() {
    let ret_type = syn::parse_str("-> ZkInputDef").unwrap();

    parse_secret_input_type_from_function_return(&ret_type);
}

#[test]
#[should_panic(expected = "Expected the last argument in angle brackets to be a type.")]
fn parse_secret_return_last_not_type() {
    let ret_type = syn::parse_str("-> ZkInputDef<'a>").unwrap();

    parse_secret_input_type_from_function_return(&ret_type);
}
