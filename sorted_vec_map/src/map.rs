//! Provides a sorted [`Vec`]-based map.
//!
//! Backed by a simple [`Vec<Entry<K,V>>`] for use in PBC smart contracts. [`SortedVecMap`]
//! provides constant time serialization/deserialization if the entries is serializable by copy
//! (Constant size determinable at compile time).
//!
//! [`SortedVecMap`] provides amortized `O(n)` insert and `O(log(n))` lookup.

use std::borrow::Borrow;
use std::fmt::Debug;
use std::ops::{Index, RangeBounds};

use crate::entry::Entry;

/// A [`Vec`]-based map.
/// [`SortedVecMap`] provides amortized `O(n)` insert and `O(log(n))` lookup.
///
/// Implements a simplfied version of the [entry API](Self::entry).
///
/// ### Fields:
/// * `entries`: [`Vec<Entry<K,V>>`], the entries in [`SortedVecMap`].
#[derive(PartialEq, Debug, Clone, Eq)]
pub struct SortedVecMap<K, V> {
    entries: Vec<Entry<K, V>>,
}

impl<K: Ord, V> SortedVecMap<K, V> {
    fn get_index_of<Q>(&self, key: &Q) -> Result<usize, usize>
    where
        K: Borrow<Q>,
        Q: Eq + Ord + ?Sized,
    {
        self.entries
            .binary_search_by_key(&key, |entry| entry.key.borrow())
    }

    fn get_entry<Q>(&self, key: &Q) -> Option<&Entry<K, V>>
    where
        K: Borrow<Q>,
        Q: Eq + Ord + ?Sized,
    {
        self.get_index_of(key)
            .ok()
            .map(|index| &self.entries[index])
    }

    fn get_entry_mut<Q>(&mut self, key: &Q) -> Option<&mut Entry<K, V>>
    where
        K: Borrow<Q>,
        Q: Eq + Ord + ?Sized,
    {
        self.get_index_of(key)
            .ok()
            .map(|index| &mut self.entries[index])
    }
}

impl<K, V> Default for SortedVecMap<K, V> {
    fn default() -> Self {
        Self::new()
    }
}

impl<K, V> SortedVecMap<K, V> {
    /// Constructor for [`SortedVecMap`].
    pub fn new() -> Self {
        SortedVecMap {
            entries: Vec::new(),
        }
    }

    /// Clears [`SortedVecMap`], removing all elements.
    pub fn clear(&mut self) {
        self.entries.clear()
    }

    /// Returns a reference to the value corresponding to the key.
    pub fn get<Q>(&self, key: &Q) -> Option<&V>
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.get_entry(key).map(|entry| &entry.value)
    }

    /// Returns the key-value pair corresponding to the supplied key.
    pub fn get_key_value<Q>(&self, k: &Q) -> Option<(&K, &V)>
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.get_entry(k).map(Entry::tuple)
    }

    /// Returns the first key-value pair in [`SortedVecMap`].
    /// The key in this pair is the minimum key in [`SortedVecMap`].
    pub fn first_key_value(&self) -> Option<(&K, &V)>
    where
        K: Ord,
    {
        self.entries.first().map(Entry::tuple)
    }

    /// Removes and returns the first element in [`SortedVecMap`].
    /// The key of this element is the minimum key that was in [`SortedVecMap`].
    pub fn pop_first(&mut self) -> Option<(K, V)>
    where
        K: Ord,
    {
        if !self.entries.is_empty() {
            Some(self.entries.remove(0).into_tuple())
        } else {
            None
        }
    }

    /// Returns the last key-value pair in [`SortedVecMap`].
    /// The key in this pair is the maximum key in [`SortedVecMap`].
    pub fn last_key_value(&self) -> Option<(&K, &V)>
    where
        K: Ord,
    {
        self.entries.last().map(Entry::tuple)
    }

    /// Removes and returns the last element in [`SortedVecMap`].
    /// The key of this element is the maximum key that was in [`SortedVecMap`].
    pub fn pop_last(&mut self) -> Option<(K, V)>
    where
        K: Ord,
    {
        self.entries.pop().map(Entry::into_tuple)
    }

    /// Returns `true` if [`SortedVecMap`] contains a value for the specified key.
    pub fn contains_key<Q>(&self, key: &Q) -> bool
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.get(key).is_some()
    }

    /// Returns a mutable reference to the value corresponding to the key.
    pub fn get_mut<Q>(&mut self, key: &Q) -> Option<&mut V>
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.get_entry_mut(key).map(|entry| &mut entry.value)
    }

    /// Inserts a key-value pair into [`SortedVecMap`].
    ///
    /// If [`SortedVecMap`] did not have this key present, None is returned.
    /// If [`SortedVecMap`] did have this key present, the value is updated, and the old value is returned.
    pub fn insert(&mut self, key: K, value: V) -> Option<V>
    where
        K: Ord,
    {
        match self.get_index_of(&key) {
            Ok(idx) => {
                self.entries.push(Entry { key, value });
                Some(self.entries.swap_remove(idx).value)
            }
            Err(idx) => {
                self.entries.insert(idx, Entry { key, value });
                None
            }
        }
    }

    /// Removes a key from [`SortedVecMap`], returning the value at the key if the key was previously in [`SortedVecMap`].
    pub fn remove<Q>(&mut self, key: &Q) -> Option<V>
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.remove_entry(key).map(|entry| entry.1)
    }

    /// Removes a key from [`SortedVecMap`], returning the stored key and value if the key was previously in [`SortedVecMap`].
    pub fn remove_entry<Q>(&mut self, key: &Q) -> Option<(K, V)>
    where
        K: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        match self.get_index_of(key) {
            Ok(idx) => {
                let entry = self.entries.remove(idx);
                Some(entry.into_tuple())
            }
            Err(_) => None,
        }
    }

    /// Retains only the elements specified by the predicate.
    ///
    /// In other words, remove all pairs `(k, v)` for which `f(&k, &mut v)` returns `false`.
    pub fn retain<F>(&mut self, mut f: F)
    where
        K: Ord,
        F: FnMut(&K, &mut V) -> bool,
    {
        self.entries
            .retain_mut(|entry| f(&entry.key, &mut entry.value));
    }

    /// Moves all elements from other into self, leaving other empty.
    ///
    /// If a key from other is already present in self, the respective value from self will be
    /// overwritten with the respective value from other.
    pub fn append(&mut self, other: &mut Self)
    where
        K: Ord,
    {
        self.entries.reserve(other.len());
        while let Some((key, value)) = other.pop_last() {
            self.insert(key, value);
        }
    }

    /// Constructs a double-ended iterator over a sub-range of keys in [`SortedVecMap`].
    ///
    /// The simplest way is to use the range syntax `min..max`, thus `range(min..max)` will
    /// yield elements from min (inclusive) to max (exclusive):
    ///
    /// ```
    /// # use sorted_vec_map::SortedVecMap;
    /// let set: SortedVecMap<i32, char> = [(1,'a'),(2, 'b'),(3, 'c'),(4, 'd'),(5, 'e')].into();
    /// let range_result: Vec<_> = set.range(2..4).collect();
    /// assert_eq!(range_result, vec![(&2, &'b'), (&3, &'c')]);
    /// ```
    ///
    /// The range may also be entered as using [`core::ops::Bound`]:
    /// `range((Excluded(4), Included(10)))` will yield a left-exclusive,
    /// right-inclusive range from 4 to 10
    ///
    /// ```
    /// # use core::ops::Bound;
    /// # use sorted_vec_map::SortedVecMap;
    /// let set: SortedVecMap<i32, char> = [(1,'a'),(2, 'b'),(3, 'c'),(4, 'd'),(5, 'e')].into();
    /// let range_result: Vec<_> = set.range((Bound::Excluded(2),Bound::Included(4))).collect();
    /// assert_eq!(range_result, vec![(&3, &'c'), (&4, &'d')]);
    /// ```
    ///
    /// Unlike [`BTreeMap::range`](std::collections::BTreeMap::range) this returns an empty iterator if range `start > end`
    /// or if range `start == end` and both bounds are `Excluded`:
    ///
    /// ```
    /// # use sorted_vec_map::SortedVecMap;
    /// let set: SortedVecMap<i32, char> = [(1,'a'),(2, 'b'),(3, 'c'),(4, 'd'),(5, 'e')].into();
    /// assert_eq!(set.range(10..4).count(), 0);
    /// ```
    pub fn range<T, R>(&self, range: R) -> impl DoubleEndedIterator<Item = (&'_ K, &'_ V)>
    where
        T: Ord,
        K: Borrow<T> + Ord,
        R: RangeBounds<T>,
    {
        self.entries
            .iter()
            .filter(move |entry| range.contains(entry.key.borrow()))
            .map(Entry::tuple)
    }

    /// Constructs a mutable double-ended iterator over a sub-range of keys in [`SortedVecMap`].
    ///
    /// See [`Self::range`] for usage.
    pub fn range_mut<T, R>(
        &mut self,
        range: R,
    ) -> impl DoubleEndedIterator<Item = (&'_ K, &'_ mut V)>
    where
        T: Ord,
        K: Borrow<T> + Ord,
        R: RangeBounds<T>,
    {
        self.entries
            .iter_mut()
            .filter(move |entry| range.contains(entry.key.borrow()))
            .map(|entry| (&entry.key, &mut entry.value))
    }

    /// Creates a consuming iterator visiting all the keys, in sorted order.
    pub fn into_keys(self) -> impl DoubleEndedIterator<Item = K> {
        self.entries.into_iter().map(|entry| entry.key)
    }

    /// Creates a consuming iterator visiting all the values, in order by key.
    pub fn into_values(self) -> impl DoubleEndedIterator<Item = V> {
        self.entries.into_iter().map(|entry| entry.value)
    }

    /// Creates a consuming iterator visiting all the key-value pairs, in order by key.
    pub fn into_tuple_iter(self) -> impl DoubleEndedIterator<Item = (K, V)> {
        self.entries.into_iter().map(Entry::into_tuple)
    }

    /// Gets an iterator over the entries of [`SortedVecMap`], sorted by key.
    pub fn iter(&self) -> impl DoubleEndedIterator<Item = (&'_ K, &'_ V)> {
        self.entries.iter().map(Entry::tuple)
    }

    /// Gets a mutable iterator over the entries of [`SortedVecMap`], sorted by key.
    pub fn iter_mut(&mut self) -> impl DoubleEndedIterator<Item = (&'_ mut K, &'_ mut V)> {
        self.entries
            .iter_mut()
            .map(|entry| (&mut entry.key, &mut entry.value))
    }

    /// Gets an iterator over the keys of [`SortedVecMap`], in sorted order.
    pub fn keys(&self) -> impl DoubleEndedIterator<Item = &'_ K> {
        self.entries.iter().map(|entry| &entry.key)
    }

    /// Gets an iterator over the values of [`SortedVecMap`], in order by key.
    pub fn values(&self) -> impl DoubleEndedIterator<Item = &'_ V> {
        self.entries.iter().map(|entry| &entry.value)
    }

    /// Gets a mutable iterator over the values of [`SortedVecMap`], in order by key.
    pub fn values_mut(&mut self) -> impl DoubleEndedIterator<Item = &'_ mut V> {
        self.entries.iter_mut().map(|entry| &mut entry.value)
    }

    /// Returns the number of elements in [`SortedVecMap`].
    pub fn len(&self) -> usize {
        self.entries.len()
    }

    /// Returns `true` if [`SortedVecMap`] contains no elements.
    pub fn is_empty(&self) -> bool {
        self.entries.is_empty()
    }

    /// Get a non-mutable reference to the underlying entries of the map.
    pub fn entries(&self) -> &Vec<Entry<K, V>> {
        &self.entries
    }

    /// Gets the given key’s corresponding entry in the map for in-place manipulation.
    pub fn entry(&mut self, key: K) -> EntryApi<'_, K, V> {
        EntryApi { map: self, key }
    }
}

/// A view into a single entry in a map, which may either be vacant or occupied.
///
/// This enum is constructed from the [`SortedVecMap::entry`] method.
pub struct EntryApi<'a, K, V>
where
    K: 'a,
    V: 'a,
{
    map: &'a mut SortedVecMap<K, V>,
    key: K,
}

impl<'a, K: Ord + Clone, V> EntryApi<'a, K, V> {
    /// Ensures a value is in the entry by inserting the default if empty, and returns a mutable reference to the value in the entry.
    pub fn or_insert(self, default: V) -> &'a mut V {
        if !self.map.contains_key(&self.key) {
            self.map.insert(self.key.clone(), default);
        }
        self.map.get_mut(&self.key).unwrap() // Safe
    }
}

impl<K, V, Q> Index<&Q> for SortedVecMap<K, V>
where
    K: Borrow<Q> + Ord,
    Q: Ord + ?Sized,
{
    type Output = V;

    /// Returns a reference to the value corresponding to the supplied key.
    ///
    /// # Panics
    ///
    /// Panics if the key is not present in the `SortedVecMap`.
    fn index(&self, key: &Q) -> &V {
        self.get(key).expect("no entry found for key")
    }
}

impl<K: Ord + Copy, V, const N: usize> From<[(K, V); N]> for SortedVecMap<K, V> {
    /// Converts a `[(K, V); N]` into a `SortedVecMap<K, V>`.
    fn from(arr: [(K, V); N]) -> Self {
        // use stable sort to preserve the insertion order.
        let entries: Vec<Entry<K, V>> = Vec::from(arr)
            .into_iter()
            .map(|(key, value)| Entry { key, value })
            .collect();
        Self::from(entries)
    }
}

impl<K: Ord, V> From<Vec<Entry<K, V>>> for SortedVecMap<K, V> {
    /// Converts a `[(K, V); N]` into a `SortedVecMap<K, V>`.
    fn from(mut entries: Vec<Entry<K, V>>) -> Self {
        entries.sort_by(|a, b| a.key.cmp(&b.key));
        entries.dedup_by(|a, b| a.key.eq(&b.key));
        SortedVecMap { entries }
    }
}
