//! Contains [`Entry`] struct for [`super::SortedVecMap`].

/// Container for a key-value pair.
///
/// ### Fields:
/// * `key`: `K`, the key of the entry.
///
/// * `value`: `V`, the value of the entry.
#[repr(C)]
#[derive(PartialEq, Debug, Clone, Eq)]
#[non_exhaustive]
pub struct Entry<K, V> {
    /// The key of the entry.
    pub key: K,
    /// The value of the entry.
    pub value: V,
}

impl<K, V> Entry<K, V> {
    /// Create new entry with the given key and value.
    pub fn new(key: K, value: V) -> Self {
        Entry { key, value }
    }

    /// Convert this entry to a tuple of the key and value.
    pub fn into_tuple(self) -> (K, V) {
        (self.key, self.value)
    }

    /// Get this entry as a tuple of the key and value.
    pub fn tuple(&self) -> (&K, &V) {
        (&self.key, &self.value)
    }
}
