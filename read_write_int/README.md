# Read Write int

Utility crate for working with streams of bytes.

- Provides `ReadInt` and `WriteInt` traits for reading and writing integers of different sizes and endianness.