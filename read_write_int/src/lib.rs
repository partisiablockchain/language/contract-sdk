#![doc = include_str!("../README.md")]

mod read_int;
mod write_int;

pub use read_int::ReadInt;
pub use write_int::WriteInt;
