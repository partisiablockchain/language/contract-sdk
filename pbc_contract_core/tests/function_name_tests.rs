//! Tests of [`FunctionKind`] and [`FunctionName`].

use pbc_contract_core::function_name::{FunctionKind, FunctionName};
use pbc_contract_core::shortname::Shortname;

fn interesting_shortname_values() -> Vec<(u32, Vec<u8>)> {
    vec![
        (0, vec![0x00]),
        (1, vec![0x01]),
        (127, vec![0x7F]),
        (128, vec![0x80, 0x01]),
        (256, vec![0x80, 0x02]),
        (1000, vec![0xe8, 0x07]),
        (586977299, vec![0x93, 0xA0, 0xF2, 0x97, 0x02]),
    ]
}

#[test]
fn function_kind_clone() {
    let kind = FunctionKind::Init;
    assert_eq!(kind, kind.clone());
}

#[test]
fn shortname_as_u32() {
    for (i, shortname_bytes) in interesting_shortname_values() {
        let shortname = Shortname::from_be_bytes(&shortname_bytes).unwrap();
        assert_eq!(Shortname::from_u32(i), shortname);
        assert_eq!(shortname.as_u32(), i);
    }
}

#[test]
fn u32_as_shortname_as_u32() {
    for (i, _) in interesting_shortname_values() {
        let shortname = Shortname::from_u32(i);
        assert_eq!(i, shortname.as_u32());
    }
}

#[test]
fn u32_as_shortname_bytes_as_u32() {
    for (shortname_value, shortname_bytes) in interesting_shortname_values() {
        let parsed = Shortname::from_be_bytes(&shortname_bytes).unwrap();
        assert_eq!(shortname_bytes, parsed.bytes());
        assert_eq!(shortname_value, parsed.as_u32());
    }
}

#[test]
fn invalid_shortnames() {
    let invalid_shortname_bytes = [
        vec![0x00, 0x00],
        vec![0x00, 0x01],
        vec![0x00, 0x7F],
        vec![0x00, 0x80, 0x01],
        vec![0x80, 0x02, 0x00],
        vec![0x80],
        vec![0x80, 0x00], // Technically valid LEB128, but not normalized
        vec![0x80, 0x80, 0x80, 0x80, 0x32], // Too large for u32
        vec![0x93, 0xA0, 0xF2, 0x97, 0x32], // Too large for u32
    ];
    for bytes in invalid_shortname_bytes {
        let result = Shortname::from_be_bytes(&bytes);
        assert!(result.is_err());
    }
}

#[test]
fn function_name_new_shortname() {
    let func_name = FunctionName::new("on_erc721_received".to_string(), None);
    let expected_bytes: Vec<u8> = vec![0x83, 0x81, 0xAE, 0xCB, 0x0D];
    assert_eq!(func_name.shortname().bytes(), expected_bytes);
    let other_shortname = Shortname::from_u32(0);
    let func_name2 = FunctionName::new("on_erc721_received".to_string(), Some(other_shortname));
    let expected_shortname = Shortname::from_u32(0);
    assert_eq!(*func_name2.shortname(), expected_shortname);
}
