//! Tests of [`FnAbi`].
use create_type_spec_derive::CreateTypeSpec;
use pbc_contract_core::abi::FnAbi;
use pbc_contract_core::function_name::FunctionKind;
use pbc_traits::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_zk::SecretBinary;

#[derive(CreateTypeSpec, SecretBinary)]
struct MyStruct {}

/// Test that function must be be an zk input invocation in order to define an secret argument.
#[test]
pub fn secret_argument_action_kind() {
    let mut fnabi = FnAbi::new("my_action".to_string(), None, FunctionKind::Action);
    let res = std::panic::catch_unwind(move || {
        fnabi.secret_argument::<MyStruct>(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new())
    });

    assert!(res.is_err());
}
