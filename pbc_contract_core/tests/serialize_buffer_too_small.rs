//! Coverage tests of [`FnAbi`]
use pbc_contract_core::abi::FnAbi;
use pbc_contract_core::function_name::FunctionKind;
use test_utility::fail_write_abi;

/// [`FnAbi`] serialization fails with an correct error.
#[test]
pub fn buffer_too_small_for_kind() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(0, fnabi);
}

/// [`FnAbi`] serialization fails with an correct error.
#[test]
pub fn buffer_too_small_for_name() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(5, fnabi);
}

/// [`FnAbi`] serialization fails with an correct error.
#[test]
pub fn buffer_too_small_for_args() {
    let fnabi = FnAbi::new("my_func".to_string(), None, FunctionKind::Action);
    fail_write_abi(20, fnabi);
}
