#[cfg(feature = "abi")]
use std::io::Write;

use pbc_traits::{
    create_type_spec::{NamedTypeLookup, NamedTypeSpecs},
    CreateTypeSpec, WriteInt, WriteRPC,
};
use read_write_rpc_derive::ReadWriteRPC;

/// A struct representing any (name, type) entity.
/// In this case it is function arguments and struct fields.
///
/// Serialized with the ABI format.
#[derive(ReadWriteRPC)]
pub struct NamedEntityAbi {
    /// The name of the field or argument.
    pub name: String,
    /// The raw type spec for the type of the argument.
    pub type_spec: Vec<u8>,
}

impl NamedEntityAbi {
    /// Instantiate a `NamedEntityAbi` with  the specified name.
    ///
    /// * `name` - the name of the type.
    /// * `type_spec` - the type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn new<T: CreateTypeSpec>(
        name: String,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) -> Self {
        let type_spec = T::__ty_generate_spec(named_types, named_type_specs);
        NamedEntityAbi { name, type_spec }
    }
}

#[cfg(feature = "abi")]
impl super::AbiSerialize for NamedEntityAbi {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.name.rpc_write_to(writer)?;
        for ord in self.type_spec.iter() {
            writer.write_u8(*ord)?;
        }

        Ok(())
    }
}

#[cfg(feature = "abi")]
#[cfg(test)]
mod test {
    use crate::abi::AbiSerialize;
    use pbc_traits::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};

    use super::NamedEntityAbi;

    #[test]
    fn read_write_rpc_smoke_test() {
        let abi = NamedEntityAbi::new::<String>(
            "my_name".to_string(),
            &mut NamedTypeLookup::new(),
            &mut NamedTypeSpecs::new(),
        );

        let mut output: Vec<u8> = Vec::new();
        abi.serialize_abi(&mut output).unwrap();

        assert_eq!(
            output,
            vec![0, 0, 0, 7, 109, 121, 95, 110, 97, 109, 101, 11]
        )
    }
}
