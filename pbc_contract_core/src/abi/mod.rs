//! Module containing ABI traits and glue code.

use std::io::Write;

pub use contract::ContractAbi;
pub use func::FnAbi;
pub use pbc_traits::abi::{EnumVariant, KindInfo, NamedEntityAbi, NamedTypeSpec};
use pbc_traits::{WriteInt, WriteRPC};

mod contract;
mod func;
/// ABI generation goes through this module.
pub mod generate;

/// Serialize this struct according to the ABI specification.
///
/// * `slice` - the slice of T's to write to the stream
/// * `writer` - the writer
///
pub fn abi_serialize_slice<T: AbiSerialize, W: Write>(
    slice: &[T],
    writer: &mut W,
) -> std::io::Result<()> {
    writer.write_u32_be(slice.len() as u32)?;
    for t in slice.iter() {
        AbiSerialize::serialize_abi(t, writer)?;
    }
    Ok(())
}

/// A trait for serializing ABI objects.
/// This is different from `ReadWriteState` and `WriteRPC` since it is intended
/// for serializing across the FFI layer between contracts and `pbc-abigen`.
/// It does not serialize struct fields directly, but according to the ABI specification.
pub trait AbiSerialize {
    /// Serialize the ABI to the given writer.
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()>;
}

impl AbiSerialize for EnumVariant {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.discriminant.rpc_write_to(writer)?;
        for ord in self.type_spec.iter() {
            writer.write_u8(*ord)?;
        }

        Ok(())
    }
}
impl AbiSerialize for NamedEntityAbi {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.name.rpc_write_to(writer)?;
        for ord in self.type_spec.iter() {
            writer.write_u8(*ord)?;
        }

        Ok(())
    }
}

impl AbiSerialize for NamedTypeSpec {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        match &self.kind_information {
            KindInfo::Struct { fields } => {
                writer.write_u8(1)?;
                self.name.rpc_write_to(writer)?;
                abi_serialize_slice(fields, writer)
            }
            KindInfo::Enum { variants } => {
                writer.write_u8(2)?;
                self.name.rpc_write_to(writer)?;
                abi_serialize_slice(variants, writer)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::abi::AbiSerialize;
    use pbc_traits::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};

    use super::NamedEntityAbi;

    #[test]
    fn read_write_rpc_smoke_test() {
        let abi = NamedEntityAbi::new::<String>(
            "my_name".to_string(),
            &mut NamedTypeLookup::new(),
            &mut NamedTypeSpecs::new(),
        );

        let mut output: Vec<u8> = Vec::new();
        abi.serialize_abi(&mut output).unwrap();

        assert_eq!(
            output,
            vec![0, 0, 0, 7, 109, 121, 95, 110, 97, 109, 101, 11]
        )
    }
}
