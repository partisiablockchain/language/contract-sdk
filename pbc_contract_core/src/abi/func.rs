use pbc_traits::{
    abi::NamedEntityAbi,
    create_type_spec::{NamedTypeLookup, NamedTypeSpecs},
    CreateTypeSpec,
};
use std::io::Write;

use pbc_zk_core::SecretBinary;

use super::{abi_serialize_slice, AbiSerialize};
use crate::function_name::{FunctionKind, FunctionName};
use crate::shortname::Shortname;

/// A struct representing a function in the ABI.
///
/// Serialized with the ABI format.
pub struct FnAbi {
    name: FunctionName,
    fn_kind: FunctionKind,
    args: Vec<NamedEntityAbi>,
    secret_arg: Option<NamedEntityAbi>,
}

impl FnAbi {
    /// Create a function abi with the supplied name.
    pub fn new(name: String, shortname: Option<Shortname>, fn_kind: FunctionKind) -> Self {
        Self::from_name(FunctionName::new(name, shortname), fn_kind)
    }

    /// Create a function abi with the given function name
    pub fn from_name(name: FunctionName, fn_kind: FunctionKind) -> Self {
        FnAbi {
            name,
            fn_kind,
            args: Vec::new(),
            secret_arg: None,
        }
    }

    /// Add an argument to this instance. Types are inferred.
    ///
    /// * `name` - the name of the type.
    /// * `named_types` - the lookup table of named types to index for the ABI generation. See `pbc-abigen` for details.
    /// * `named_type_specs` - the lookup table of type index to type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn argument<T: CreateTypeSpec>(
        &mut self,
        name: String,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) {
        self.args.push(NamedEntityAbi::new::<T>(
            name,
            named_types,
            named_type_specs,
        ));
    }

    /// Add a secret argument to this instance. Argument must implement [`SecretBinary`].
    /// Name is "secret_input"
    ///
    /// * `named_types` - the lookup table of named types to index for the ABI generation. See `pbc-abigen` for details.
    /// * `named_type_specs` - the lookup table of type index to type spec for the ABI generation. See `pbc-abigen` for details.
    pub fn secret_argument<T: CreateTypeSpec + SecretBinary>(
        &mut self,
        named_types: &mut NamedTypeLookup,
        named_type_specs: &mut NamedTypeSpecs,
    ) {
        assert_eq!(
            self.fn_kind,
            FunctionKind::ZkSecretInputWithExplicitType,
            "Only function with kind ZkSecretInputWithExplicitType can take a secret argument"
        );
        self.secret_arg = Some(NamedEntityAbi::new::<T>(
            "secret_input".to_string(),
            named_types,
            named_type_specs,
        ));
    }

    /// Gets the shortname of the function.
    pub fn shortname(&self) -> &Shortname {
        self.name.shortname()
    }
}

impl AbiSerialize for FnAbi {
    fn serialize_abi<T: Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.fn_kind.serialize_abi(writer)?;
        self.name.serialize_abi(writer)?;
        abi_serialize_slice(&self.args, writer)?;
        match self.secret_arg {
            None => Ok(()),
            Some(ref arg) => arg.serialize_abi::<T>(writer),
        }
    }
}
