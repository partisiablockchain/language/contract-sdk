//! Crate containing contract data types, notably ABI generation.

pub mod function_name;
pub mod shortname;

pub mod abi;

mod leb128;
