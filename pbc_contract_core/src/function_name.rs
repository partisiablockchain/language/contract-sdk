//! Function name utility.

use sha2::{Digest, Sha256};

use pbc_traits::WriteRPC;

use crate::abi::AbiSerialize;

use crate::shortname::Shortname;

/// A small struct that automatically calculates the shortname of a function.
///
/// Serialized with the ABI format.
pub struct FunctionName {
    #[allow(unused)]
    name: String,
    shortname: Shortname,
}

impl FunctionName {
    /// Create a new instance with the specified name. The shortname is calculated if None is
    /// supplied.
    pub fn new(name: String, shortname: Option<Shortname>) -> FunctionName {
        FunctionName::create_from_str(&name, shortname)
    }

    /// Create a new instance with the specified name as a str. The shortname is calculated eagerly.
    pub fn create_from_str(name: &str, shortname_override: Option<Shortname>) -> FunctionName {
        let shortname = if let Some(value) = shortname_override {
            value
        } else {
            name_to_shortname(name)
        };

        FunctionName {
            name: name.to_string(),
            shortname,
        }
    }

    /// Gets the Shortname
    pub fn shortname(&self) -> &Shortname {
        &self.shortname
    }
}

/// Denotes the kind of the ABI function hook.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
#[repr(u8)]
pub enum FunctionKind {
    /// Kind for `init` hook.
    Init = 0x01,
    /// Kind for `action` hook.
    Action = 0x02,
    /// Kind for `callback` hook.
    Callback = 0x03,
    /// Kind for `zk_on_secret_input` hook.
    #[deprecated(note = "Use ZkSecretInputWithExplicitType instead")]
    ZkSecretInput = 0x10,
    /// Kind for `zk_on_variable_inputted` hook.
    ZkVarInputted = 0x11,
    /// Kind for `zk_on_variable_rejected` hook.
    ZkVarRejected = 0x12,
    /// Kind for `zk_on_compute_complete` hook.
    ZkComputeComplete = 0x13,
    /// Kind for `zk_on_variable_opened` hook.
    ZkVarOpened = 0x14,
    /// Kind for `zk_on_user_variable_opened` hook.
    #[deprecated(note = "Use ZkVarOpened instead")]
    ZkUserVarOpened = 0x15,
    /// Kind for `zk_on_attestation_complete` hook.
    ZkAttestationComplete = 0x16,
    /// Kind for `zk_on_secret_input` hook.
    ZkSecretInputWithExplicitType = 0x17,
    /// Kind for `zk_on_external_event` hook.
    ZkExternalEvent = 0x18,
    /// Kind for `upgrade_is_allowed` hook.
    UpgradeIsAllowed = 0x20,
    /// Kind for `upgrade` hook.
    UpgradeFromState = 0x21,
}

impl AbiSerialize for FunctionName {
    fn serialize_abi<T: std::io::Write>(&self, writer: &mut T) -> std::io::Result<()> {
        self.name.rpc_write_to(writer)?;
        self.shortname.rpc_write_to(writer)
    }
}

impl AbiSerialize for FunctionKind {
    fn serialize_abi<T: std::io::Write>(&self, writer: &mut T) -> std::io::Result<()> {
        let discriminant = *self as u8;
        discriminant.rpc_write_to(writer)
    }
}

/// Create a shortname from the given function name.
/// The shortname consists of the first 4 bytes of the SHA256 hash of the name.
fn name_to_shortname(raw_name: &str) -> Shortname {
    let mut digest = Sha256::new();
    Digest::update(&mut digest, raw_name.as_bytes());
    let output = digest.finalize();
    let first_four = output.chunks(4).next().unwrap();
    let shortname_u32 = u32::from_be_bytes(first_four.try_into().unwrap());
    Shortname::from_u32(shortname_u32)
}
