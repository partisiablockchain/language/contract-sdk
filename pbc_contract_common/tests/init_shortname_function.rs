//! Tests of [`Shortname`].
use pbc_contract_common::fn_init_shortname;
use pbc_data_types::shortname::Shortname;

const FN_INIT_SHORTNAME: u32 = 0xFFFFFFFF;

#[test]
fn init_shortname_should_return_init_shortname() {
    assert_eq!(fn_init_shortname(), Shortname::from_u32(FN_INIT_SHORTNAME));
}
