//! Tests of [`EvmEventFilter`].
use pbc_contract_common::zk::evm_event::{EvmAddress, EvmEventFilter};
use pbc_contract_common::U256;
use pbc_traits::WriteRPC;

const DEFAULT_EVM_ADDRESS: EvmAddress = [
    0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 1, 5, 16, 64, 234, 14, 17, 192,
];

#[test]
fn builder_update_filter() {
    let b = EvmEventFilter::builder(DEFAULT_EVM_ADDRESS);
    let filter = b
        .filter_from_block(U256 {
            bytes: [
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0,
            ],
        })
        .filter_from_block(U256 {
            bytes: [
                2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0,
            ],
        })
        .build();

    let mut gotten_bytes = Vec::new();
    filter.rpc_write_to(&mut gotten_bytes).unwrap();
    let expected_bytes = [
        0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 1, 5, 16, 64, 234, 14, 17, 192, // Address
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, // block
        0, 0, 0, 0, // no topics
    ];

    assert_eq!(gotten_bytes, expected_bytes);
}

#[test]
fn builder_add_matches() {
    let b = EvmEventFilter::builder(DEFAULT_EVM_ADDRESS);
    let filter = b
        .any_match()
        .exact_match([
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
            5, 5, 5,
        ])
        .one_of_match(vec![
            [
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8,
            ],
            [
                3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                3, 3, 3, 3,
            ],
        ])
        .any_match()
        .build();

    let mut gotten_bytes = Vec::new();
    filter.rpc_write_to(&mut gotten_bytes).unwrap();
    let expected_bytes = [
        0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 1, 5, 16, 64, 234, 14, 17, 192, // Address
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, // default block
        0, 0, 0, 4, // 4 topics
        0, 0, 0, 0, // First topic is empty (any)
        0, 0, 0, 1, // Second topic has 1 entry
        5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, // Second topic
        0, 0, 0, 2, // Next topic (third) has 2 entries
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, // Third topic
        3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
        3, 3, 0, 0, 0, 0, // Last topic is empty, like the first
    ];

    assert_eq!(gotten_bytes, expected_bytes);
}

#[test]
#[should_panic(expected = "Attempted to build EvmEventFilter with more than four topics")]
fn builder_too_many_topics() {
    let b = EvmEventFilter::builder(DEFAULT_EVM_ADDRESS);
    b.any_match()
        .any_match()
        .any_match()
        .any_match()
        .any_match()
        .build();
}
