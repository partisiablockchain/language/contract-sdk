//! Serialization regression tests, to detect possible changes to the serialization, which would
//! invalidate a lot of tooling.
use std::fmt::Debug;

use pbc_contract_common::{test_examples, zk, U256};
use pbc_data_types::address::AddressType;
use pbc_traits::{ReadRPC, ReadWriteState, WriteRPC};

fn canary_rpc_noeq<T: ReadRPC + WriteRPC>(example: &T) -> Vec<u8> {
    // Check serializable
    let mut buf_rpc_1 = Vec::with_capacity(21);
    example
        .rpc_write_to(&mut buf_rpc_1)
        .expect("Write entire value");

    // Check deserializable
    let example_from_rpc = T::rpc_read_from(&mut buf_rpc_1.as_slice());

    // Check that serialization of deserialized is identical to original
    let mut buf_rpc_2 = Vec::with_capacity(21);
    example_from_rpc
        .rpc_write_to(&mut buf_rpc_2)
        .expect("Write entire value");
    assert_eq!(buf_rpc_1, buf_rpc_2);

    // Result!
    buf_rpc_2
}

fn canary_rpc<T: ReadRPC + WriteRPC + PartialEq + Debug>(example: &T) -> Vec<u8> {
    // Check serializable
    let buf_rpc = canary_rpc_noeq(example);

    // Check deserialize identically
    let example_from_rpc = T::rpc_read_from(&mut buf_rpc.as_slice());

    assert_eq!(example, &example_from_rpc);
    buf_rpc
}

/// Serializing and then deserializing a value does not produce the same value, but
/// a reserialization produces the same bytes.
fn write_read_rewrite_state_is_stable<T: ReadWriteState>(example: &T) -> Vec<u8> {
    // Check serializable
    let mut buf_state_1 = Vec::with_capacity(21);
    example
        .state_write_to(&mut buf_state_1)
        .expect("Write entire value");

    // Check deserializable
    let example_from_state = T::state_read_from(&mut buf_state_1.as_slice());

    // Check that serialization of deserialized is identical to original
    let mut buf_state_2 = Vec::with_capacity(21);
    example_from_state
        .state_write_to(&mut buf_state_2)
        .expect("Write entire value");
    assert_eq!(buf_state_1, buf_state_2);

    // Result!
    buf_state_2
}

/// Serializing and then deserializing a value should produce an equal value.
fn write_read_state_produce_equal_value<T: ReadWriteState + PartialEq + Debug>(
    example: &T,
) -> Vec<u8> {
    // Check serializable identically
    let mut buf_state = Vec::with_capacity(21);
    example
        .state_write_to(&mut buf_state)
        .expect("Write entire value");

    // Check deserialize identically
    let example_from_state = T::state_read_from(&mut buf_state.as_slice());

    assert_eq!(example, &example_from_state);
    buf_state
}

fn canary_rpc_state_eq<T: ReadWriteState + ReadRPC + WriteRPC + PartialEq + Debug>(example: &T) {
    // Check serializable identically
    let buf_rpc = canary_rpc(example);
    let buf_state = write_read_state_produce_equal_value(example);
    assert_eq!(buf_rpc, buf_state);
}

#[test]
fn canary_address_type() {
    let examples = vec![
        AddressType::Account,
        AddressType::SystemContract,
        AddressType::PublicContract,
        AddressType::ZkContract,
    ];
    for example in examples {
        canary_rpc_state_eq(&example);
    }
}

#[test]
fn canary_address() {
    canary_rpc_state_eq(&test_examples::EXAMPLE_ADDRESS_1);
    canary_rpc_state_eq(&test_examples::EXAMPLE_ADDRESS_2);
}

#[test]
fn canary_u256() {
    // Canary rpc
    let example_u256 = test_examples::EXAMPLE_U256;
    let mut buf = Vec::with_capacity(example_u256.bytes.len());
    example_u256
        .rpc_write_to(&mut buf)
        .expect("Write entire value");
    let read_example_u256 = U256::rpc_read_from(&mut buf.as_slice());
    assert_eq!(read_example_u256, example_u256);
    write_read_state_produce_equal_value(&test_examples::EXAMPLE_U256);
}

#[test]
fn canary_hash() {
    canary_rpc_state_eq(&test_examples::EXAMPLE_HASH_1);
    canary_rpc_state_eq(&test_examples::EXAMPLE_HASH_2);
}

#[test]
fn canary_public_key() {
    canary_rpc_state_eq(&test_examples::EXAMPLE_PUBLIC_KEY);
}

#[test]
fn canary_bls_public_key() {
    canary_rpc_state_eq(&test_examples::EXAMPLE_BLS_PUBLIC_KEY);
}

#[test]
fn canary_bls_signature() {
    canary_rpc_state_eq(&test_examples::EXAMPLE_BLS_SIGNATURE);
}

#[test]
fn canary_signature() {
    canary_rpc_state_eq(&test_examples::example_signature());
}

/// [`SortedVecMap`] is serializable and deserializable.
#[test]
fn canary_sorted_vec_map() {
    let buffer = write_read_state_produce_equal_value(&test_examples::example_vec_map());
    let expected: Vec<u8> = vec![
        2, 0, 0, 0, // length of the map
        1, // key
        3, 0, 0, 0, // Length of vec
        2, 0, 0, 0, // String length
        b'm', b'y', // "my"
        4, 0, 0, 0, // String length
        b'n', b'a', b'm', b'e', // "name"
        2, 0, 0, 0, // String length
        b'i', b's', // "is"
        2,    // key
        1, 0, 0, 0, // Length of vec
        4, 0, 0, 0, // String length
        b'w', b'h', b'a', b't', // "what"
    ];

    assert_eq!(buffer, expected);
}

#[test]
fn canary_context() {
    canary_rpc(&test_examples::EXAMPLE_CONTEXT);
}

#[test]
fn canary_callback_context() {
    canary_rpc_noeq(&test_examples::example_callback_context());
}

#[test]
fn canary_secret_var() {
    let examples = vec![
        test_examples::SECRET_VAR_ID_4,
        test_examples::SECRET_VAR_ID_30,
        test_examples::SECRET_VAR_ID_31,
    ];
    for example in examples {
        let buf_rpc = canary_rpc(&example);
        let buf_state = write_read_state_produce_equal_value(&example);
        assert_ne!(buf_rpc, buf_state);
    }
}

#[test]
fn canary_zk_closed() {
    write_read_rewrite_state_is_stable(&test_examples::ZK_CLOSED_1);
    write_read_rewrite_state_is_stable(&test_examples::ZK_CLOSED_2);
    write_read_rewrite_state_is_stable(&test_examples::zk_closed_open());
}

#[test]
fn canary_attestation() {
    write_read_rewrite_state_is_stable(&test_examples::example_data_attestation());
}

#[test]
fn canary_event_subscription() {
    write_read_rewrite_state_is_stable(&test_examples::example_subscription());
}

#[test]
fn canary_external_event() {
    write_read_rewrite_state_is_stable(&test_examples::example_external_event());
}

#[test]
fn canary_ids() {
    write_read_state_produce_equal_value(&test_examples::example_secret_var_id());
    canary_rpc(&test_examples::example_secret_var_id());
    write_read_state_produce_equal_value(&test_examples::example_attestation_id());
    canary_rpc(&test_examples::example_attestation_id());
    write_read_state_produce_equal_value(&test_examples::example_event_subscription_id());
    canary_rpc(&test_examples::example_event_subscription_id());
    write_read_state_produce_equal_value(&test_examples::example_external_event_id());
    canary_rpc(&test_examples::example_external_event_id());
}

#[test]
fn read_zk_state() {
    let input: Vec<u8> = test_examples::example_zk_state_bytes();
    let state = zk::ZkState::<u32>::rpc_read_from(&mut input.as_slice());
    assert_eq!(state.calculation_state, zk::CalculationStatus::Waiting);
}
