//! Tests of [`ReadWriteState`].
use pbc_contract_common::address::{Address, AddressType};
use pbc_contract_common::test_examples::{
    example_event_subscription_id, example_external_event_id,
};
use pbc_contract_common::zk::evm_event::EvmEventFilter;
use pbc_contract_common::zk::{SecretVarId, ZkInputDef, ZkStateChange};
use pbc_data_types::shortname::ShortnameZkComputation;
use test_utility::fail_write_rpc;

#[test]
fn start_computation_buffer_too_small() {
    let change = ZkStateChange::start_computation::<u8>(
        ShortnameZkComputation::from_u32(0x32),
        vec![5],
        None,
    );
    let clon1 = change.clone();
    let clon2 = change.clone();

    fail_write_rpc(0, change);
    fail_write_rpc(1, clon1);
    fail_write_rpc(6, clon2);
}

#[test]
fn delete_pending_input_buffer_too_small() {
    let change = ZkStateChange::DeletePendingInput {
        variable: SecretVarId::new(9),
    };

    fail_write_rpc(0, change);
}

#[test]
fn transfer_variable_buffer_too_small() {
    let change = ZkStateChange::TransferVariable {
        variable: SecretVarId::new(2),
        new_owner: Address {
            address_type: AddressType::Account,
            identifier: [
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            ],
        },
    };
    let clon1 = change.clone();

    fail_write_rpc(0, change);
    fail_write_rpc(1, clon1);
}

#[test]
fn delete_variable_buffer_too_small() {
    let change = ZkStateChange::DeleteVariable {
        variable: SecretVarId::new(3),
    };

    fail_write_rpc(0, change);
}

#[test]
fn delete_variables_buffer_too_small() {
    let change = ZkStateChange::DeleteVariables {
        variables_to_delete: vec![SecretVarId::new(4), SecretVarId::new(55)],
    };

    fail_write_rpc(0, change);
}

#[test]
fn open_variables_buffer_too_small() {
    let change = ZkStateChange::OpenVariables {
        variables: vec![SecretVarId::new(5), SecretVarId::new(33)],
    };

    fail_write_rpc(0, change);
}

#[test]
#[allow(deprecated)]
fn output_complete_buffer_too_small() {
    let change = ZkStateChange::OutputComplete {};

    fail_write_rpc(0, change);
}

#[test]
fn attest_complete_buffer_too_small() {
    let change = ZkStateChange::Attest {
        data_to_attest: vec![8, 5],
    };

    fail_write_rpc(0, change);
}

#[test]
fn subscribe_evm_buffer_too_small() {
    let b = EvmEventFilter::builder([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5]);
    let change = ZkStateChange::SubscribeToEvmEvents {
        chain_id: "cid".to_string(),
        filter: b.build(),
    };
    let clon1 = change.clone();

    fail_write_rpc(0, change);
    fail_write_rpc(5, clon1);
}

#[test]
fn unsubscribe_evm_buffer_too_small() {
    let change = ZkStateChange::UnsubscribeFromEvmEvents {
        subscription_id: example_event_subscription_id(),
    };

    fail_write_rpc(0, change);
}

#[test]
fn delete_evm_event_buffer_too_small() {
    let change = ZkStateChange::DeleteEvmEvent {
        event_id: example_external_event_id(),
    };
    let clon1 = change.clone();

    fail_write_rpc(0, change);
    fail_write_rpc(1, clon1);
}

#[test]
fn delete_evm_events_buffer_too_small() {
    let change = ZkStateChange::DeleteEvmEvents {
        events_to_delete: vec![example_external_event_id(), example_external_event_id()],
    };

    fail_write_rpc(0, change);
}

#[test]
fn input_def_buffer_too_small_for_secret_t() {
    let input_def: ZkInputDef<Vec<i32>, i32> = ZkInputDef::with_metadata(None, vec![1, 2, 3, 4]);
    fail_write_rpc(0, input_def);
}

#[test]
fn input_def_buffer_too_small_for_seal() {
    let input_def: ZkInputDef<Vec<i32>, i32> = ZkInputDef::with_metadata(None, vec![1, 2, 3, 4]);
    fail_write_rpc(5, input_def);
}
