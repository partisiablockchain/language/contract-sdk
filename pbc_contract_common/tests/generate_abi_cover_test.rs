//! Test ABI generation for test state.
use create_type_spec_derive::CreateTypeSpec;
use pbc_abi::abi_model::{FnAbi, FunctionKind};
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_contract_common::abi::generate::{generate_abi, LookupTable};

fn dummy_lut_fn_abi(
    _named_types: &mut NamedTypeLookup,
    _named_type_specs: &mut NamedTypeSpecs,
) -> FnAbi {
    FnAbi::new("my_func".to_string(), Some(vec![42]), FunctionKind::Action)
}

#[derive(CreateTypeSpec)]
#[allow(unused)]
struct MyState {
    field: u64,
}

/// ABI can be generated for a struct with the @CreateTypeSpec macro.
#[test]
pub fn generate_dummy_abi() {
    let dummy_fn_ptr: LookupTable<FnAbi> = dummy_lut_fn_abi;
    let fn_list = Box::new(dummy_fn_ptr);
    let fn_list_ptr = Box::into_raw(fn_list);

    let res =
        unsafe { generate_abi::<MyState>([10, 2, 0], [5, 4, 0], 1, fn_list_ptr as *const usize) };

    let expected = [
        80, 66, 67, 65, 66, 73, // "PCBABI" header
        10, 2, 0, // Binder version
        5, 4, 0, // Client version
        0, 0, 0, 1, // Type length
        1, // Struct type
        0, 0, 0, 7, // Name length
        77, 121, 83, 116, 97, 116, 101, // "MyState"
        0, 0, 0, 1, // No of fields
        0, 0, 0, 5, // Field name length
        102, 105, 101, 108, 100, // "field"
        4,   // Type spec of argument (u64 = 0x4)
        0, 0, 0, 1, // Number of functions
        2, // Action kind
        0, 0, 0, 7, // Function name length
        109, 121, 95, 102, 117, 110, 99, // "my_func"
        42, // Shortname = 42
        0, 0, 0, 0, // No args
        0, 0, // Pointer to state struct
    ];

    assert_eq!(res, expected);
}
