//! Tests for [`ReadWriteState`] macro.
use pbc_abi::abi_model::TypeSpec::NamedTypeRef;
use pbc_abi::CreateTypeSpec;
use read_write_state_derive::ReadWriteState;

use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
#[cfg(feature = "abi")]
use pbc_contract_common::test_examples::{
    example_attestation_id, example_data_attestation, SECRET_VAR_ID_4, ZK_CLOSED_1,
};
use pbc_contract_common::test_examples::{
    example_event_subscription_id, example_external_event, example_external_event_id,
    example_subscription,
};
use pbc_contract_common::zk::evm_event::{
    EventSubscriptionId, EvmAddress, EvmEventFilter, ExternalEventId,
};
use pbc_contract_common::zk::{CalculationStatus, SecretVarId, ZkInputDef};
use pbc_traits::{ReadRPC, ReadWriteState, WriteRPC};
use std::cmp::Ordering;

const DEFAULT_EVM_ADDRESS: EvmAddress = [
    0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 1, 5, 16, 64, 234, 14, 17, 192,
];

/// [`SecretVarId`] is a fully featured type.
#[test]
#[allow(clippy::clone_on_copy)]
pub fn secret_var_id_derives() {
    let clone = SECRET_VAR_ID_4.clone();

    assert_eq!(SECRET_VAR_ID_4, clone);
    assert_eq!(format!("{:?}", clone), "SecretVarId { raw_id: 4 }");

    assert_eq!(SECRET_VAR_ID_4.cmp(&SECRET_VAR_ID_4), Ordering::Equal); // Ord
    assert_eq!(
        SECRET_VAR_ID_4.partial_cmp(&SECRET_VAR_ID_4),
        Some(Ordering::Equal)
    ); // PartialOrd

    assert_eq!(SecretVarId::__ty_name(), "SecretVarId");
    let identifier = SecretVarId::__ty_identifier();
    assert_eq!(SecretVarId::__ty_identifier(), identifier);

    let w =
        SecretVarId::__ty_generate_spec(&mut NamedTypeLookup::new(), &mut NamedTypeSpecs::new());

    assert_eq!(w, NamedTypeRef { index: 0 })
}

/// [`AttestationId`] is a fully featured type.
#[test]
#[allow(clippy::clone_on_copy)]
pub fn attestation_id_derives() {
    let attest_id = example_attestation_id();
    let clone = attest_id.clone();

    assert_eq!(attest_id, clone);
    assert_eq!(format!("{:?}", clone), "AttestationId { raw_id: 1 }");
}

/// [`AttestationId`] can be formatted using debug.
#[test]
pub fn data_attestation_derives() {
    let data_attest_id = example_data_attestation();

    assert_eq!(format!("{:?}", data_attest_id), "DataAttestation { attestation_id: AttestationId { raw_id: 1 }, signatures: [Some(Signature { recovery_id: 17, value_r: [118, 119, 228, 77, 2, 19, 80, 73, 78, 111, 124, 5, 90, 139, 104, 129, 38, 103, 20, 189, 178, 3, 128, 185, 254, 95, 172, 117, 10, 123, 152, 241], value_s: [214, 87, 68, 45, 98, 243, 176, 41, 174, 79, 220, 229, 186, 107, 200, 97, 134, 71, 116, 157, 18, 227, 224, 153, 94, 63, 12, 85, 106, 91, 248, 209] }), Some(Signature { recovery_id: 54, value_r: [55, 164, 13, 194, 211, 16, 9, 14, 47, 60, 197, 26, 75, 40, 65, 230, 39, 212, 125, 114, 195, 64, 121, 190, 31, 108, 53, 202, 59, 88, 177, 150], value_s: [23, 4, 237, 34, 179, 112, 233, 110, 15, 156, 165, 122, 43, 136, 33, 70, 7, 52, 93, 210, 163, 160, 89, 30, 255, 204, 21, 42, 27, 184, 145, 246] }), Some(Signature { recovery_id: 247, value_r: [100, 205, 130, 147, 208, 201, 206, 239, 252, 133, 218, 11, 232, 1, 166, 231, 148, 61, 50, 131, 0, 57, 126, 223, 44, 245, 138, 251, 24, 113, 86, 215], value_s: [196, 173, 226, 115, 48, 169, 46, 207, 92, 101, 58, 235, 72, 225, 6, 199, 244, 29, 146, 99, 96, 25, 222, 191, 140, 213, 234, 219, 120, 81, 182, 183] }), Some(Signature { recovery_id: 36, value_r: [141, 66, 83, 144, 137, 142, 175, 188, 69, 154, 203, 168, 193, 102, 167, 84, 253, 242, 67, 192, 249, 62, 159, 236, 181, 74, 187, 216, 49, 22, 151, 132], value_s: [109, 162, 51, 240, 105, 238, 143, 28, 37, 250, 171, 8, 161, 198, 135, 180, 221, 82, 35, 32, 217, 158, 127, 76, 149, 170, 155, 56, 17, 118, 119, 228] })], data: [1, 2, 3, 4, 5, 6, 7, 8] }");
}

/// [`CalculationStatus`] is a fully featured type.
#[test]
#[allow(deprecated)]
pub fn calculation_status_derives() {
    let wait = CalculationStatus::Waiting;
    let cal = CalculationStatus::Calculating;
    let out = CalculationStatus::Output;
    let mal = CalculationStatus::MaliciousBehaviour;
    let done = CalculationStatus::Done;

    assert_eq!(format!("{:?}", wait), "Waiting");
    assert_eq!(format!("{:?}", cal), "Calculating");
    assert_eq!(format!("{:?}", out), "Output");
    assert_eq!(format!("{:?}", mal), "MaliciousBehaviour");
    assert_eq!(format!("{:?}", done), "Done");

    let mut buf = Vec::new();
    wait.rpc_write_to(&mut buf).unwrap();
    cal.rpc_write_to(&mut buf).unwrap();
    out.rpc_write_to(&mut buf).unwrap();
    mal.rpc_write_to(&mut buf).unwrap();
    done.rpc_write_to(&mut buf).unwrap();

    assert_eq!(buf, [0, 1, 2, 3, 4])
}

/// [`ZkClosed`] can be formatted using debug.
#[test]
pub fn zk_closed_derives() {
    let closed = ZK_CLOSED_1;

    assert_eq!(format!("{:?}", closed), "ZkClosed { variable_id: SecretVarId { raw_id: 31 }, owner: Address { address_type: PublicContract, identifier: [2, 32, 3, 2, 2, 3, 2, 3, 2, 32, 32, 3, 23, 2, 3, 23, 2, 3, 23, 2] }, is_sealed: false, metadata: 255, data: None }");
}

/// [`ZkInputDef`] can be formatted using debug.
#[test]
pub fn zk_input_def_derives() {
    let input_def: ZkInputDef<i32, i32> = ZkInputDef::with_metadata(None, 4);

    assert_eq!(
        format!("{:?}", input_def),
        "ZkInputDef { seal: false, shortname: None, metadata: 4, secret_type: PhantomData<i32>, secret_size: 32 }"
    );
}

/// [`EvmEventFilter`] is a fully featured type.
#[test]
fn evm_filter_derives() {
    let filter = EvmEventFilter::builder(DEFAULT_EVM_ADDRESS).build();

    assert!(filter.eq(&filter)); // PartialEq
    assert_eq!(format!("{:?}", filter), format!("{:?}", filter)); // Debug
    assert_eq!(filter.cmp(&filter), Ordering::Equal); // Ord
    assert_eq!(filter.partial_cmp(&filter), Some(Ordering::Equal)); // PartialOrd

    // Read and write RPC
    let mut rpc_buf = [0u8; 56];
    filter.rpc_write_to(&mut rpc_buf.as_mut_slice()).unwrap();
    let de_rpc_filter = EvmEventFilter::rpc_read_from(&mut rpc_buf.as_slice());
    assert_eq!(de_rpc_filter, filter);

    // Read and write state
    let mut state_buf = [0u8; 56];
    filter
        .state_write_to(&mut state_buf.as_mut_slice())
        .unwrap();
    let de_state_filter = EvmEventFilter::state_read_from(&mut state_buf.as_slice());
    assert_eq!(de_state_filter, filter);
}

/// [`EvmEvent`] is a fully featured type.
#[test]
#[allow(clippy::clone_on_copy)]
fn evm_event_subscription_id() {
    let subscription_id = example_event_subscription_id();
    let clon = subscription_id.clone();

    assert_eq!(
        format!("{:?}", subscription_id),
        format!("{:?}", subscription_id)
    ); // Debug
    assert_eq!(subscription_id, clon); // Clone

    // CreateTypeSpec derive
    assert_eq!(EventSubscriptionId::__ty_name(), "EventSubscriptionId");
    let identifier = EventSubscriptionId::__ty_identifier();
    assert_eq!(EventSubscriptionId::__ty_identifier(), identifier);

    let w = EventSubscriptionId::__ty_generate_spec(
        &mut NamedTypeLookup::new(),
        &mut NamedTypeSpecs::new(),
    );

    assert_eq!(w, NamedTypeRef { index: 0 })
}

/// [`EventSubscription`] is a fully featured type.
#[test]
fn evm_event_subscription_derives() {
    let event_subscription = example_subscription();

    assert_eq!(
        format!("{:?}", event_subscription),
        format!("{:?}", event_subscription)
    ); // Debug
}

/// [`ExternalEventId`] is a fully featured type.
#[test]
fn evm_external_event_id_derives() {
    let external_id = example_external_event_id();

    assert_eq!(format!("{:?}", external_id), format!("{:?}", external_id)); // Debug

    // CreateTypeSpec derive
    assert_eq!(ExternalEventId::__ty_name(), "ExternalEventId");
    let identifier = ExternalEventId::__ty_identifier();
    assert_eq!(ExternalEventId::__ty_identifier(), identifier);

    let w = ExternalEventId::__ty_generate_spec(
        &mut NamedTypeLookup::new(),
        &mut NamedTypeSpecs::new(),
    );

    assert_eq!(w, NamedTypeRef { index: 0 })
}

/// [`ExternalEvent`] is a fully featured type.
#[test]
fn evm_external_event_derives() {
    let external_event = example_external_event();

    assert_eq!(
        format!("{:?}", external_event),
        format!("{:?}", external_event)
    ); // Debug
}

/// [`SecretVarId`] must be usable in metadata
#[test]
pub fn secret_var_id_can_be_used_in_metadata() {
    let var_id = SecretVarId::new(4);
    ZkInputDef::<SecretVarId, i8>::with_metadata(None, var_id);
}

#[derive(Clone, ReadWriteState)]
struct MyMetadata {
    id: SecretVarId,
}

/// [`SecretVarId`] must be usable in a struct that is then used in metadata
#[test]
pub fn secret_var_id_can_be_used_in_metadata_struct() {
    let id = SecretVarId::new(4);
    let metadata = MyMetadata { id };
    ZkInputDef::<MyMetadata, i8>::with_metadata(None, metadata);
}
