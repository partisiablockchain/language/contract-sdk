//! Tests for [`ZkStateChange`].
use pbc_contract_common::test_examples::{
    example_event_subscription_id, example_external_event_id,
};
use pbc_contract_common::zk::evm_event::EvmEventFilter;
use pbc_contract_common::zk::{SecretVarId, ZkStateChange};
use pbc_contract_common::ContractResultBuffer;
use pbc_data_types::address::{Address, AddressType};
use pbc_data_types::shortname::{ShortnameZkComputation, ShortnameZkComputeComplete};

fn assert_written(actual: Vec<ZkStateChange>, expected: Vec<u8>) {
    let mut gotten_buffer = ContractResultBuffer::new();
    gotten_buffer.write_zk_state_change(actual);
    assert_eq!(gotten_buffer.data, expected);
}

const EXAMPLE_METADATA: u8 = 5;
const EXAMPLE_INPUTS: [u8; 2] = [1, 2];

const SHORTNAME_COMPUTE: ShortnameZkComputation = ShortnameZkComputation::from_u32(0x35);

#[test]
fn start_computation() {
    let state_changes = vec![ZkStateChange::start_computation::<u8>(
        SHORTNAME_COMPUTE,
        vec![EXAMPLE_METADATA],
        None,
    )];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 23, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        9, // Discriminant: Start computation with shortname
        0, 0, 0, 0x35, // Shortname
        0, 0, 0, 1, // Number of pieces of metadata
        0, 0, 0, 1, // Size of metadata
        5, // Metadata
        0, 0, 0, 0, // Number of input arguments
        0, // No compute complete shortname
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn start_computation_with_inputs() {
    let state_changes = vec![ZkStateChange::start_computation_with_inputs::<u8, u8>(
        SHORTNAME_COMPUTE,
        vec![EXAMPLE_METADATA],
        EXAMPLE_INPUTS.to_vec(),
        None,
    )];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 33, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        9, // Discriminant: Start computation with shortname
        0, 0, 0, 0x35, // Shortname
        0, 0, 0, 1, // Number of pieces of metadata
        0, 0, 0, 1, // Size of metadata
        5, // Metadata
        0, 0, 0, 2, // Number of input arguments
        0, 0, 0, 1, // Size of input 1
        1, // Input 1
        0, 0, 0, 1, // Size of input 2
        2, // Input 1
        0, // No compute complete shortname
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn start_computation_with_shortname() {
    let state_changes = vec![ZkStateChange::start_computation::<u8>(
        SHORTNAME_COMPUTE,
        vec![EXAMPLE_METADATA],
        Some(ShortnameZkComputeComplete::from_u32(7)),
    )];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 24, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        9, // Discriminant: Start computation with shortname
        0, 0, 0, 0x35, // Shortname
        0, 0, 0, 1, // Number of pieces of metadata
        0, 0, 0, 1, // Size of metadata
        5, // Metadata
        0, 0, 0, 0, // Number of input arguments
        1, 7, // Compute complete shortname
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
#[allow(deprecated)]
fn using_output_complete_panics() {
    let state_changes = vec![ZkStateChange::OutputComplete {}];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 9, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        3, // Discriminant: Delete variables
        0, 0, 0, 0, // Number to delete
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn delete_pending_input() {
    let state_changes = vec![ZkStateChange::DeletePendingInput {
        variable: SecretVarId::new(9),
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 9, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        1, // Discriminant: Delete pending inputs
        0, 0, 0, 9, // Number to delete
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn transfer_variable() {
    let state_changes = vec![ZkStateChange::TransferVariable {
        variable: SecretVarId::new(2),
        new_owner: Address {
            address_type: AddressType::Account,
            identifier: [
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            ],
        },
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 30, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        2, // Discriminant: Transfer variables
        0, 0, 0, 2, 0, // Address type: Account
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, // Address
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn delete_variable() {
    let state_changes = vec![ZkStateChange::DeleteVariable {
        variable: SecretVarId::new(3),
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 13, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        3, // Discriminant: Delete variables
        0, 0, 0, 1, // Number to delete
        0, 0, 0, 3, // Id to delete
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn delete_variables() {
    let state_changes = vec![ZkStateChange::DeleteVariables {
        variables_to_delete: vec![SecretVarId::new(4), SecretVarId::new(55)],
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 17, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        3, // Discriminant: Delete variables
        0, 0, 0, 2, // Number to delete
        0, 0, 0, 4, // First id
        0, 0, 0, 55, // Second id
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn open_variables() {
    let state_changes = vec![ZkStateChange::OpenVariables {
        variables: vec![SecretVarId::new(5), SecretVarId::new(33)],
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 17, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        4, // Discriminant: Open variables
        0, 0, 0, 2, // Number to open
        0, 0, 0, 5, // Id of first open
        0, 0, 0, 33, // id of second open
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn contract_done() {
    let state_changes = vec![ZkStateChange::ContractDone];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 5, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        6, // Discriminant: Contract done
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn attest() {
    let state_changes = vec![ZkStateChange::Attest {
        data_to_attest: vec![8, 5],
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 11, // Zk state section length
        0, 0, 0, 1, // Number of state changes
        7, // Discriminant: Attest
        0, 0, 0, 2, // Length of data
        8, 5, // data
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn subscribe_to_evm_events() {
    let b = EvmEventFilter::builder([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5]);
    let state_changes = vec![ZkStateChange::SubscribeToEvmEvents {
        chain_id: "cid".to_string(),
        filter: b.build(),
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 68, // Zk state section length
        0, 0, 0, 1,  // Number of state changes
        10, // Discriminant: subscribe to evm events
        0, 0, 0, 3, // Length of chain id
        99, 105, 100, // chain id
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, // address
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, // Otherwise empty filter.
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn unsubscribe_to_evm_events() {
    let state_changes = vec![ZkStateChange::UnsubscribeFromEvmEvents {
        subscription_id: example_event_subscription_id(),
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 9, // Zk state section length
        0, 0, 0, 1,  // Number of state changes
        11, // Discriminant: unsubscribe evm
        0, 0, 0, 1, // event id
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn delete_evm_event() {
    let state_changes = vec![ZkStateChange::DeleteEvmEvent {
        event_id: example_external_event_id(),
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 13, // Zk state section length
        0, 0, 0, 1,  // Number of state changes
        12, // Discriminant: delete evm event
        0, 0, 0, 1, // Length of data
        0, 0, 0, 1, // Id to delete
    ];
    assert_written(state_changes, expected_bytes);
}

#[test]
fn delete_evm_events() {
    let state_changes = vec![ZkStateChange::DeleteEvmEvents {
        events_to_delete: vec![example_external_event_id(), example_external_event_id()],
    }];
    let expected_bytes = vec![
        0, 0, 0, 0,  // Empty length
        17, // Zk state change section id
        0, 0, 0, 17, // Zk state section length
        0, 0, 0, 1,  // Number of state changes
        12, // Discriminant: delete evm event
        0, 0, 0, 2, // Length of data
        0, 0, 0, 1, // Id to delete (first)
        0, 0, 0, 1, // Id to delete (second)
    ];
    assert_written(state_changes, expected_bytes);
}
