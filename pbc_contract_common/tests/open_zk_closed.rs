//! Tests for opening ZkClosed values.
#[cfg(feature = "abi")]
use pbc_contract_common::test_examples::{zk_closed_open, ZK_CLOSED_1};

/// Opened ZkClosed values can be inspected.
#[test]
fn get_opened_some() {
    let open = zk_closed_open();

    let val: Option<Vec<u8>> = open.open_value();
    assert!(val.is_some());
    assert_eq!(val.unwrap(), vec![1u8, 2, 3]);
}

/// Unopened ZkClosed values cannot be inspected.
#[test]
fn get_closed_empty() {
    let closed = ZK_CLOSED_1;

    let val: Option<Vec<u8>> = closed.open_value();
    assert!(val.is_none());
}
