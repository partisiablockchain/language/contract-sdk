//! Test of [`AvlTreeSet`]; a wrapper for [`AvlTreeMap`], used through the WASM Binder.

use pbc_contract_common::avl_tree_map::AvlTreeSet;

/// [`AvlTreeSet`] can be printed using the [`Debug`] trait.
#[test]
fn avl_tree_set_debug_printing() {
    let set: AvlTreeSet<i32> = AvlTreeSet::new();
    assert_eq!(
        format!("{set:?}"),
        "AvlTreeSet { inner_map: AvlTreeMap { key_type: PhantomData<i32>, value_type: PhantomData<pbc_data_types::avl_tree_map::set::Unit>, tree_id: 0 } }"
    );
}

/// [`AvlTreeSet`] can insert and remove items.
#[test]
fn avl_tree_set_can_insert_remove() {
    let mut set: AvlTreeSet<i32> = AvlTreeSet::new();

    assert!(set.is_empty());

    set.insert(42);
    assert_eq!(set.len(), 1);
    assert!(set.contains(&42));
    assert!(!set.contains(&420));

    set.insert(420);
    assert_eq!(set.len(), 2);
    assert!(set.contains(&420));

    set.remove(&42);
    assert_eq!(set.len(), 1);
    assert!(set.contains(&420));
    assert!(!set.contains(&42));

    set.remove(&420);
    assert_eq!(set.len(), 0)
}

/// Adding existing item to [`AvlTreeSet`] does nothing.
#[test]
fn avl_tree_set_insert_same_item_twice_does_nothing() {
    let mut set: AvlTreeSet<i32> = AvlTreeSet::new();
    assert_eq!(set.len(), 0);

    set.insert(1);
    assert_eq!(set.len(), 1);
    assert!(set.contains(&1));

    set.insert(1);
    assert_eq!(set.len(), 1);
    assert!(set.contains(&1));
}

/// [`AvlTreeSet`] can be iterated through.
#[test]
fn avl_tree_set_can_iterate_over() {
    let bytes: Vec<u32> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    let mut set: AvlTreeSet<u32> = AvlTreeSet::new();
    for el in &bytes {
        set.insert(*el);
        assert!(set.contains(el));
    }
    for (i, num) in set.iter().enumerate() {
        assert_eq!(i as u32, num)
    }
}

/// [`AvlTreeSet`] can iterate too much and get None.
#[test]
fn avl_tree_set_iter_next() {
    let mut set: AvlTreeSet<u16> = AvlTreeSet::new();
    set.insert(10);
    set.insert(20);
    set.insert(30);

    let mut set_iter = set.iter();
    assert_eq!(set_iter.next().unwrap(), 10);
    assert_eq!(set_iter.next().unwrap(), 20);
    assert_eq!(set_iter.next().unwrap(), 30);
    assert_eq!(set_iter.next(), None);
}
