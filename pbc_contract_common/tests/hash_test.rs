//! Tests of the [`Hash`] object.
use pbc_contract_common::Hash;

fn example_hash() -> Hash {
    Hash::digest(&[1, 2, 3])
}

/// Hashes can be formatted as uppercase hex strings.
#[test]
pub fn upper_hex() {
    assert_eq!(
        format!("{:X}", example_hash()),
        "039058C6F2C0CB492C533B0A4D14EF77CC0F78ABCCCED5287D84A1A2011CFB81"
    );
}

/// Hashes can be formatted as lowercase hex strings.
#[test]
pub fn lower_hex() {
    assert_eq!(
        format!("{:x}", example_hash()),
        "039058c6f2c0cb492c533b0a4d14ef77cc0f78abccced5287d84a1a2011cfb81"
    );
}

/// Hashes are displayed as uppercase hex strings.
#[test]
pub fn display() {
    assert_eq!(
        format!("{}", example_hash()),
        format!("{:X}", example_hash())
    );
}

/// Hashes are debug displayed as uppercase hex strings.
#[test]
pub fn debug() {
    assert_eq!(
        format!("{:?}", example_hash()),
        "039058C6F2C0CB492C533B0A4D14EF77CC0F78ABCCCED5287D84A1A2011CFB81"
    );
}
