//! Tests of [`RawPtr`].
#[cfg(feature = "abi")]
use pbc_contract_common::RawPtr;
use std::io::Read;
use std::io::Write;

/// Can read bytes from [`RawPtr`] created from a vector.
#[test]
pub fn two_reads_from_raw() {
    let mut mem = vec![42, 55];
    let mut rp = RawPtr::new(mem.as_mut_ptr());

    assert_eq!(rp.read(), 42);
    assert_eq!(rp.read(), 55);
}

/// Can read buffers from [`RawPtr`] created from a vector.
#[test]
pub fn read_into_buffer() {
    let mut mem = vec![42, 55];
    let mut buf = vec![0, 0];
    let mut rp = RawPtr::new(mem.as_mut_ptr());

    assert!(Read::read(&mut rp, &mut buf).is_ok());

    assert_eq!(buf, vec![42, 55])
}

/// Can flush from [`RawPtr`] created from a vector.
#[test]
pub fn flush_ok() {
    let mut mem = vec![];
    let mut rp = RawPtr::new(mem.as_mut_ptr());
    assert!(rp.flush().is_ok());
}
