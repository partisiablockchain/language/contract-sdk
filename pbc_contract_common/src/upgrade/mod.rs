//! Definitions used for upgrading Rust/WASM smart contracts.

use create_type_spec_derive::CreateTypeSpecInternal;
use pbc_abi::abi_model::{AbiSerialize, TypeSpec};
use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_abi::CreateTypeSpec;
use pbc_data_types::Hash;
use read_write_rpc_derive::ReadRPC;
use read_write_rpc_derive::WriteRPC;
use read_write_state_derive::ReadWriteState;

/// Contract hashes are used to identify the running contract code.
#[derive(
    PartialEq, Eq, ReadRPC, WriteRPC, ReadWriteState, Debug, Clone, CreateTypeSpecInternal,
)]
#[non_exhaustive]
pub struct ContractHashes {
    /// Hash of the contract bytecode.
    pub contract: Hash,
    /// Hash of the contract binder bytecode.
    pub binder: Hash,
    /// Hash of the ABI.
    pub abi: Hash,
}

/// This is a identifier that roughly uniquely identifies a Rust type as it appears in the contract
/// ABI.
///
/// [`TypeWitness`]es are used to validate that upgraded contracts have a correct understanding of
/// the layout of the previous version of the contract.
///
/// Use [`TypeWitness::of`] to get the [`TypeWitness`] of any [`CreateTypeSpec`]-implementing type.
///
/// ```
/// # use pbc_contract_common::upgrade::TypeWitness;
/// # use create_type_spec_derive::CreateTypeSpec;
/// #
/// #[derive(CreateTypeSpec)]
/// struct Point { x: u8, y: u8 };
///
/// let point_witness: TypeWitness = TypeWitness::of::<Point>();
/// ```
///
/// ## Details
///
/// Two structs with the same field (and field names) will generate the same [`TypeWitness`],
/// even if they are named differently. Two identical structs except for their field order
/// or their field names will not produce the same [`TypeWitness`]:
///
/// ```
/// # use pbc_contract_common::upgrade::TypeWitness;
/// # use create_type_spec_derive::CreateTypeSpec;
/// #
/// #[derive(CreateTypeSpec)]
/// struct Point { x: u8, y: u8 };
///
/// #[derive(CreateTypeSpec)]
/// struct PointByAnyOtherName { x: u8, y: u8 };
///
/// #[derive(CreateTypeSpec)]
/// struct PointSwap { y: u8, x: u8 };
///
/// #[derive(CreateTypeSpec)]
/// struct PointOtherTypes { x: u16, y: u16 };
///
/// assert_eq!(TypeWitness::of::<Point>(), TypeWitness::of::<PointByAnyOtherName>());
/// assert_ne!(TypeWitness::of::<Point>(), TypeWitness::of::<PointSwap>());
/// assert_ne!(TypeWitness::of::<Point>(), TypeWitness::of::<PointOtherTypes>());
/// ```
///
/// Here `Point` and `PointByAnyOtherName` have the same [`TypeWitness`], but `PointSwap` and
/// `PointOtherTypes` have different [`TypeWitness`] from the others.
///
/// ## Deriving type witnesses (Version 1)
///
/// Type witnesses are constructed by:
///
/// 1. Generating the recursive type spec using [`CreateTypeSpec::__ty_generate_spec`].
/// 2. Creating a new output buffer with layout:
///   1. Direct type spec for bytes.
///   2. Appending each type in the produced recursive type specs as: First the type spec
///      index, then the type spec with the name replaced with an empty string.
/// 3. Hashing the result with SHA256.
///
/// The layout generated in step 2 will look roughtly like the following, depending upon the
/// type: `TYPE_SPEC 0 TYPE_0_SPEC 1 TYPE_1_SPEC ...`
#[derive(PartialEq, Eq, ReadRPC, WriteRPC, Debug, Clone, CreateTypeSpecInternal)]
#[non_exhaustive]
pub struct TypeWitness {
    /// Version field of the type witness. Version 1 is the only one defined so far.
    pub version: u8,
    /// [`TypeWitness`] hash.
    pub hash: Hash,
}

impl TypeWitness {
    /// Generate the [`TypeWitness`] of the given type.
    ///
    /// Note: This function must be more or less permanently stable, as a change in the value might
    /// result in contracts that cannot be upgraded.
    pub fn of<T: CreateTypeSpec>() -> Self {
        // Generate ABI as normal
        let mut named_type_index_lookup = NamedTypeLookup::new();
        let mut named_type_specs = NamedTypeSpecs::new();

        // Abi bytes contains the reference for the current type.
        // If T is a struct, then abi_bytes = [0,0].
        let abi_struct: TypeSpec =
            T::__ty_generate_spec(&mut named_type_index_lookup, &mut named_type_specs);

        // Emit a completed ABI
        let mut serialization = vec![];
        abi_struct.serialize_abi(&mut serialization).unwrap();
        for (k, mut v) in named_type_specs.into_tuple_iter() {
            // Replace name with empty string
            v.name = "".to_string();

            // Write type spec
            serialization.push(k);
            v.serialize_abi(&mut serialization).unwrap();
        }

        Self {
            version: 1,
            hash: Hash::digest(&serialization),
        }
    }
}
