/// ABI generation goes through this module.
pub mod generate;
#[cfg(feature = "abi")]
mod raw_ptr;
pub use pbc_abi::{abi_model, create_type_spec, CreateTypeSpec};

/// Write an ABI object to the given pointer.
/// This method uses unsafe code.
#[allow(clippy::not_unsafe_ptr_arg_deref)]
#[cfg(feature = "abi")]
pub fn abi_to_ptr<T: pbc_traits::ReadWriteState>(abi: T, pointer: *mut u8) -> u32 {
    let mut raw = raw_ptr::RawPtr::new(pointer);
    abi.state_write_to(&mut raw).unwrap();
    raw.get_offset()
}
