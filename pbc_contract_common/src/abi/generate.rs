use pbc_abi::abi_model::{AbiSerialize, ContractAbi, FnAbi};

use pbc_abi::create_type_spec::{NamedTypeLookup, NamedTypeSpecs};
use pbc_abi::CreateTypeSpec;
use std::io::Write;

/// Serialized with the ABI format.
pub type LookupTable<T> = unsafe fn(&mut NamedTypeLookup, &mut NamedTypeSpecs) -> T;

/// Cast a raw function pointer to a: `fn(&NamedTypeLookup) -> T`, for any T.
unsafe fn cast_pointer_unconditionally<T>(ptr: *const ()) -> LookupTable<T> {
    std::mem::transmute::<*const (), LookupTable<T>>(ptr)
}

/// Read a raw C-type array of usize from memory interpreting all items as a function pointer
/// using `cast_pointer_unconditionally`.
unsafe fn read_fn_pointer_array<T>(len: u32, ptr: *const usize) -> Vec<LookupTable<T>> {
    let mut result = Vec::with_capacity(len as usize);
    for i in 0..len {
        let location = ptr.add(i as usize);
        let fn_ptr = std::ptr::read(location) as *const ();
        result.push(cast_pointer_unconditionally(fn_ptr));
    }
    result
}

/// Generates and serializes a FileAbi to raw bytes.
///
/// # Safety
///
/// This should only be run by the ABI generation tool.
pub unsafe fn generate_abi<StateT: CreateTypeSpec>(
    version_binder: [u8; 3],
    version_client: [u8; 3],
    fn_len: u32,
    fn_list_ptr: *const usize,
) -> Vec<u8> {
    let fn_types_lookup: Vec<LookupTable<FnAbi>> =
        read_fn_pointer_array::<FnAbi>(fn_len, fn_list_ptr);
    let contract = generate_contract_abi::<StateT>(fn_types_lookup);

    let abi_header_buffer = abi_header_bytes(version_binder, version_client);

    let mut output: Vec<u8> = Vec::new();
    output.write_all(&abi_header_buffer).unwrap();
    contract.serialize_abi(&mut output).unwrap();
    output
}

/// Generates and serializes a [`ContractAbi`] to raw bytes.
///
/// Given the following prerequisites this function is always guaranteed to generate the same ABI.
///
/// * All types correctly implements [`CreateTypeSpec::__ty_generate_spec`] as documented,
/// * The order of the functions in the provided list is unchanged,
/// * Function names are unchanged,
/// * Function argument names are unchanged,
/// * The order of function arguments are unchanged,
/// * Type names, field names, and variant names are unchanged, and
/// * Field and variant order is unchanged.
///
/// This means that the random type identifier [`CreateTypeSpec::__ty_identifier`] may be different
/// between calls.
///
/// If we allow for changing the names of týpes, fields and variants the types in the generated ABI
/// is still guaranteed to be in the same order.
///
/// # Safety
///
/// This should only be run by the ABI generation tool.
pub unsafe fn generate_contract_abi<StateT: CreateTypeSpec>(
    fn_abi_lookup: Vec<LookupTable<FnAbi>>,
) -> ContractAbi {
    let mut named_types = NamedTypeLookup::new();
    let mut named_type_specs = NamedTypeSpecs::new();
    let state_type_spec = StateT::__ty_generate_spec(&mut named_types, &mut named_type_specs);
    let actions: Vec<FnAbi> = fn_abi_lookup
        .iter()
        .map(|fn_abi_closure| fn_abi_closure(&mut named_types, &mut named_type_specs))
        .collect();

    let mut contract = ContractAbi::new(state_type_spec);
    contract.types(named_type_specs.into_values().collect());
    contract.actions(actions);
    contract
}

/// Create a header for the given version
unsafe fn abi_header_bytes(version_binder: [u8; 3], version_client: [u8; 3]) -> [u8; 12] {
    let mut bytes = [0u8; 12];
    for (i, byte) in "PBCABI".as_bytes().iter().enumerate() {
        bytes[i] = *byte;
    }

    bytes[6] = version_binder[0];
    bytes[7] = version_binder[1];
    bytes[8] = version_binder[2];

    bytes[9] = version_client[0];
    bytes[10] = version_client[1];
    bytes[11] = version_client[2];

    bytes
}
