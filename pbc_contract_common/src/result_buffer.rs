use crate::events::EventGroup;
use pbc_abi::CreateTypeSpec;
use pbc_traits::WriteInt;
use pbc_traits::{ReadWriteState, WriteRPC};

use crate::upgrade::TypeWitness;
use crate::zk;
use pbc_zk_core::SecretBinary;

fn write_u32_be_at_idx(buffer: &mut [u8], idx: usize, value: u32) -> std::io::Result<()> {
    buffer[idx..(4 + idx)].clone_from_slice(&value.to_be_bytes());
    Ok(())
}

/// Container module for sections identifiers.
mod result_section_type_id {
    /// Identifier for the state section
    pub const STATE: u8 = 0x01;
    /// Identifier for the events section
    pub const EVENTS: u8 = 0x02;
    /// Identifier for the zk-state-change section
    pub const ZK_STATE_CHANGE: u8 = 0x11;
    /// Identifier for the zk-input-def section
    pub const ZK_INPUT_DEF: u8 = 0x12;
    /// Identifier for the upgrade-is-allowed section
    pub const UPGRADE_IS_ALLOWED: u8 = 0x20;
    /// Identifier for the state-type-witness section
    pub const STATE_TYPE_WITNESS: u8 = 0x21;
}

/// PBC internal object for serializing results to buffer, in a format understood by the blockchain
/// binder. Wraps buffer data, providing easy section serialization methods.
///
/// **Contracts should not use this struct directly.**
///
/// Usage protocol:
/// - Initialize with [`new`](Self::new)
/// - Write sections, in order: (Calls are allowed to be absent.)
///   * [`write_events`](Self::write_events)
///   * [`write_state`](Self::write_state)
/// - Finalize with [`finalize_result_buffer`](Self::finalize_result_buffer)
#[non_exhaustive]
pub struct ContractResultBuffer {
    /// Stores the actual buffer data
    pub data: Vec<u8>,

    /// Stores section id of the next allowed section
    pub next_allowed_section_id: u8,
}

#[allow(clippy::new_without_default)]
impl ContractResultBuffer {
    /// Allocates a vector and writes the result tuple according to what the blockchain binder expects.
    ///
    /// This will only write the buffer itself, it will not forget it.
    ///
    /// Should be used in conjunction with [`Self::finalize_result_buffer`], which will place the buffer as
    /// expected by the blockchain binder, and produce some output to locate it.
    pub fn new() -> Self {
        let mut data = Vec::with_capacity(10240);

        // Reserve 4 bytes for the length of the rest of the result.
        data.write_u32_be(0).unwrap();

        Self {
            data,
            next_allowed_section_id: 0,
        }
    }

    /// Writes single section by using the given `section_data_writer`.
    ///
    /// Section will be serialized with [the section format](https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#section-format):
    ///
    /// ```ignore
    /// | id: u8 | len: u32 | data: $len bytes |
    /// ```
    ///
    /// The section data is determined from the given `section_data_writer`, but the length of that
    /// data can only be determined after all of the data has been written. A placeholder length of
    /// zero is initially set, but this value is overwritten after all data is written.
    #[inline]
    fn write_section<F: FnOnce(&mut Vec<u8>) -> std::io::Result<()>>(
        &mut self,
        section_id: u8,
        section_data_writer: F,
    ) -> std::io::Result<()> {
        // Check that this section id is allowed to be written
        assert!(self.next_allowed_section_id <= section_id, "Duplicated or incorrectly ordered sections. Tried to write section with id 0x{:02x}, but expected section id of at least 0x{:02x}", section_id, self.next_allowed_section_id );
        self.next_allowed_section_id = section_id + 1;

        // Write id
        self.data.write_u8(section_id).unwrap();

        // Write placeholder length, and keep track of where we wrote it
        let buf_length_idx = self.data.len();
        self.data.write_u32_be(0).unwrap();

        // Write section data, using the supplied function
        section_data_writer(&mut self.data).unwrap();

        // Determine actual length of data, and replace the placeholder.
        let data_length = (self.data.len() - buf_length_idx - 4) as u32;
        write_u32_be_at_idx(&mut self.data, buf_length_idx, data_length)
    }

    /// Writes the state to the output buffer
    ///
    /// See [`Self`] documentation for order of operations.
    pub fn write_state<S: ReadWriteState>(&mut self, state: S) {
        if std::mem::size_of::<S>() == 0 {
            return;
        }
        self.write_section(result_section_type_id::STATE, |buf| {
            state.state_write_to(buf)
        })
        .unwrap();
    }

    /// Writes a vector of events to the output buffer.
    ///
    /// See [`Self`] documentation for order of operations.
    pub fn write_events(&mut self, events: Vec<EventGroup>) {
        if events.is_empty() {
            return;
        }
        self.write_section(result_section_type_id::EVENTS, |buf| {
            events.rpc_write_to(buf)
        })
        .unwrap();
    }

    /// Finalizes the [`ContractResultBuffer`] such that the data can be read from the WASM
    /// binders.
    ///
    /// The result value is the pointer to the buffer, which will be used by the WASM binder to
    /// locate the buffer.
    ///
    /// See [`ContractResultBuffer`] documentation for order of operations.
    ///
    /// # Safety
    ///
    /// The method writes the result and [`std::mem::forget`]s [`ContractResultBuffer`], which
    /// ensures that the buffer is not garbage collected and overwritten before being read by the
    /// WASM binder. As this is a [memory leak](https://en.wikipedia.org/wiki/Memory_leak) this
    /// function should only be called as the last part of the transaction.
    pub unsafe fn finalize_result_buffer(self) -> u64 {
        let mut buf = self.data;

        // Write length of buffer to the first 4 bytes of the buffer
        // These bytes were reserved for the buffer length.
        let payload_len = (buf.len() - std::mem::size_of::<u32>()) as u32;
        let len_bytes = payload_len.to_be_bytes();
        for (i, byte) in len_bytes.iter().enumerate() {
            buf[i] = *byte;
        }

        let ptr = buf.as_ptr();

        // Disable garbage collection for the buffer, such that it's not overwritten before it's
        // read by the binder.
        std::mem::forget(buf);

        ptr as u64
    }

    /// Writes an instance of [`zk::ZkInputDef`] to the output buffer.
    pub fn write_zk_input_def_result<MetadataT: ReadWriteState, SecretT: SecretBinary>(
        &mut self,
        declaration: zk::ZkInputDef<MetadataT, SecretT>,
    ) {
        self.write_section(result_section_type_id::ZK_INPUT_DEF, |buf| {
            declaration.rpc_write_to(buf)
        })
        .unwrap();
    }

    /// Writes a vector of [`zk::ZkStateChange`] to the output buffer.
    pub fn write_zk_state_change(&mut self, changes: Vec<zk::ZkStateChange>) {
        self.write_section(result_section_type_id::ZK_STATE_CHANGE, |buf| {
            changes.rpc_write_to(buf)
        })
        .unwrap();
    }

    /// Writes the upgrade section, which indicates to the binder whether the contract allows the
    /// attempted upgrade or not.
    pub fn write_upgrade_section(&mut self, can_be_upgraded: bool) {
        self.write_section(result_section_type_id::UPGRADE_IS_ALLOWED, |buf| {
            can_be_upgraded.rpc_write_to(buf)
        })
        .unwrap();
    }

    /// Writes the state witness section, which describes the layout of the given contract state.
    pub fn write_state_witness<S: CreateTypeSpec>(&mut self) {
        let type_witness = TypeWitness::of::<S>();
        self.write_section(result_section_type_id::STATE_TYPE_WITNESS, |buf| {
            type_witness.rpc_write_to(buf)
        })
        .unwrap();
    }
}
