//! Partisia Blockchain SDK Common Crate
//!
//! Defines common types and methods used in PBC smart contracts.

pub use pbc_data_types::address;
pub use pbc_data_types::avl_tree_map;
pub use pbc_data_types::shortname;
pub use pbc_data_types::signature;
pub use sorted_vec_map;
pub mod upgrade;
pub use pbc_data_types::{BlsPublicKey, BlsSignature, Hash, PublicKey, U256};
pub use result_buffer::ContractResultBuffer;

/// ABI generation goes through this module.
pub mod abi;
pub mod context;
pub mod events;
mod result_buffer;
#[cfg(any(test, doc, feature = "test_examples"))]
pub mod test_examples;
pub mod zk;
/// The shortname for the init method of a contract.
const FN_INIT_SHORTNAME: u32 = 0xFFFFFFFF;

/// The shortname for the init method of a contract.
pub fn fn_init_shortname() -> shortname::Shortname {
    shortname::Shortname::from_u32(FN_INIT_SHORTNAME)
}
