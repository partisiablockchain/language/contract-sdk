//! Definitions specific for external EVM events that Zero-Knowledge contracts can subscribe to.
//!
//! [`EventSubscription`] cannot be manually created, but are handled by the system when adding a
//! new subscription using an [`EvmEventFilter`].
//! [`EvmEventFilter`] are created by using [`EvmEventFilterBuilder`].
//! [`ExternalEvent`] cannot be manually created, but are added by the system.

pub use pbc_data_types::zk::{
    EventSubscriptionId, EvmAddress, EvmChainId, EvmEventTopic, ExternalEventId,
};
use pbc_data_types::U256;
use read_write_rpc_derive::{ReadRPC, WriteRPC};
use read_write_state_derive::ReadWriteState;

/// Filter to apply to a subscription on EVM events.
#[derive(Eq, PartialEq, Debug, Clone, Ord, PartialOrd, ReadRPC, WriteRPC, ReadWriteState)]
pub struct EvmEventFilter {
    /// Address of contract that emits events.
    address: EvmAddress,
    /// Block number of the earliest block to receive events from.
    from_block: U256,
    /// Event topics, i.e. event signature and indexed parameters. The list is order dependent.
    topics: Vec<EvmEventTopicFilter>,
}

impl EvmEventFilter {
    /// Create a new filter builder.
    pub fn builder(address: EvmAddress) -> EvmEventFilterBuilder {
        EvmEventFilterBuilder {
            address,
            from_block: None,
            topics: vec![],
        }
    }
}

/// A filter on an EVM event topic is a list of possible matches for the topic.
type EvmEventTopicFilter = Vec<EvmEventTopic>;

/// Event filter builder
pub struct EvmEventFilterBuilder {
    address: EvmAddress,
    from_block: Option<U256>,
    topics: Vec<EvmEventTopicFilter>,
}

impl EvmEventFilterBuilder {
    /// Set earliest block to receive events from.
    pub fn filter_from_block(mut self, block_number: U256) -> Self {
        self.from_block = Some(block_number);
        self
    }

    /// Allow next topic to match on any value.
    pub fn any_match(mut self) -> Self {
        self.topics.push(vec![]);
        self
    }

    /// Next topic should have an exact match on the supplied value.
    pub fn exact_match(mut self, topic: EvmEventTopic) -> Self {
        self.topics.push(vec![topic]);
        self
    }

    /// Next topic can match on any one of the supplied values.
    pub fn one_of_match(mut self, topics: Vec<EvmEventTopic>) -> Self {
        self.topics.push(topics);
        self
    }

    /// Build the filter.
    pub fn build(self) -> EvmEventFilter {
        if self.topics.len() > 4 {
            panic!("Attempted to build EvmEventFilter with more than four topics")
        }
        EvmEventFilter {
            address: self.address,
            from_block: self.from_block.clone().unwrap_or(U256 { bytes: [0; 32] }),
            topics: self.topics,
        }
    }
}

/// A subscription to external EVM events.
///
/// # Invariants
///
/// Cannot be manually created; must be retrieved from state.
#[derive(Debug, ReadWriteState)]
#[non_exhaustive]
pub struct EventSubscription {
    /// The id of this event subscription.
    pub subscription_id: EventSubscriptionId,
    /// Whether the subscription is active or not.
    pub is_active: bool,
    /// Identifier of the chain that is subscribed to.
    pub chain_id: EvmChainId,
    /// Address of the contract that is subscribed to.
    pub contract_address: EvmAddress,
    /// Block number of the first block the subscription applies to.
    pub from_block: U256,
    /// Topics of the subscription.
    pub topics: Vec<EvmEventTopicFilter>,
}

/// An external EVM event.
///
/// # Invariants
///
/// Cannot be manually created; must be retrieved from state.
#[derive(Debug, ReadWriteState)]
#[non_exhaustive]
pub struct ExternalEvent {
    /// The id of the event subscription.
    pub subscription_id: EventSubscriptionId,
    /// The id of this event.
    pub event_id: ExternalEventId,
    /// Data included in the log.
    pub data: Vec<u8>,
    /// Topics included in the log,
    pub topics: Vec<EvmEventTopic>,
}
